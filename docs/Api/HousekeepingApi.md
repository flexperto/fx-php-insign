# Insign\HousekeepingApi

All URIs are relative to *http://is2-cloud.test.flexperto.com/is2-horn-8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cleanUpDBUsingDELETE1**](HousekeepingApi.md#cleanUpDBUsingDELETE1) | **DELETE** /configure/cleanupdb | Cleanup database
[**cleanUpDBUsingGET1**](HousekeepingApi.md#cleanUpDBUsingGET1) | **GET** /configure/cleanupdb | Cleanup database
[**cleanUpDBUsingPOST1**](HousekeepingApi.md#cleanUpDBUsingPOST1) | **POST** /configure/cleanupdb | Cleanup database
[**deleteAllSessionsUsingDELETE**](HousekeepingApi.md#deleteAllSessionsUsingDELETE) | **DELETE** /persistence/deleteall | Mark all sessions of the sessions owner as deleted
[**deleteAllSessionsUsingGET**](HousekeepingApi.md#deleteAllSessionsUsingGET) | **GET** /persistence/deleteall | Mark all sessions of the sessions owner as deleted
[**deleteAllSessionsUsingPOST**](HousekeepingApi.md#deleteAllSessionsUsingPOST) | **POST** /persistence/deleteall | Mark all sessions of the sessions owner as deleted
[**deleteMarkedSessionsUsingDELETE**](HousekeepingApi.md#deleteMarkedSessionsUsingDELETE) | **DELETE** /persistence/deletemarked | Delete all session marked as deleted of the sessions owner permanently
[**deleteMarkedSessionsUsingGET**](HousekeepingApi.md#deleteMarkedSessionsUsingGET) | **GET** /persistence/deletemarked | Delete all session marked as deleted of the sessions owner permanently
[**deleteMarkedSessionsUsingPOST**](HousekeepingApi.md#deleteMarkedSessionsUsingPOST) | **POST** /persistence/deletemarked | Delete all session marked as deleted of the sessions owner permanently
[**deleteUserAndSessionsUsingDELETE**](HousekeepingApi.md#deleteUserAndSessionsUsingDELETE) | **DELETE** /configure/deleteuser | Remove a user
[**deleteUserAndSessionsUsingGET**](HousekeepingApi.md#deleteUserAndSessionsUsingGET) | **GET** /configure/deleteuser | Remove a user
[**deleteUserAndSessionsUsingPOST**](HousekeepingApi.md#deleteUserAndSessionsUsingPOST) | **POST** /configure/deleteuser | Remove a user


# **cleanUpDBUsingDELETE1**
> cleanUpDBUsingDELETE1()

Cleanup database

Delete all session that have previously been marked as deleted from the database now. Usually no need to call this as it is usually trigger by an internal nightly cron job.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $apiInstance->cleanUpDBUsingDELETE1();
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->cleanUpDBUsingDELETE1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cleanUpDBUsingGET1**
> cleanUpDBUsingGET1()

Cleanup database

Delete all session that have previously been marked as deleted from the database now. Usually no need to call this as it is usually trigger by an internal nightly cron job.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $apiInstance->cleanUpDBUsingGET1();
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->cleanUpDBUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cleanUpDBUsingPOST1**
> cleanUpDBUsingPOST1()

Cleanup database

Delete all session that have previously been marked as deleted from the database now. Usually no need to call this as it is usually trigger by an internal nightly cron job.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $apiInstance->cleanUpDBUsingPOST1();
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->cleanUpDBUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAllSessionsUsingDELETE**
> \Insign\Model\BasicResult deleteAllSessionsUsingDELETE($sessionid)

Mark all sessions of the sessions owner as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteAllSessionsUsingDELETE($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->deleteAllSessionsUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAllSessionsUsingGET**
> \Insign\Model\BasicResult deleteAllSessionsUsingGET($sessionid)

Mark all sessions of the sessions owner as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteAllSessionsUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->deleteAllSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAllSessionsUsingPOST**
> \Insign\Model\BasicResult deleteAllSessionsUsingPOST($sessionid)

Mark all sessions of the sessions owner as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteAllSessionsUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->deleteAllSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteMarkedSessionsUsingDELETE**
> \Insign\Model\BasicResult deleteMarkedSessionsUsingDELETE($sessionid)

Delete all session marked as deleted of the sessions owner permanently

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteMarkedSessionsUsingDELETE($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->deleteMarkedSessionsUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteMarkedSessionsUsingGET**
> \Insign\Model\BasicResult deleteMarkedSessionsUsingGET($sessionid)

Delete all session marked as deleted of the sessions owner permanently

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteMarkedSessionsUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->deleteMarkedSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteMarkedSessionsUsingPOST**
> \Insign\Model\BasicResult deleteMarkedSessionsUsingPOST($sessionid)

Delete all session marked as deleted of the sessions owner permanently

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteMarkedSessionsUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->deleteMarkedSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingDELETE**
> deleteUserAndSessionsUsingDELETE($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingDELETE($user);
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->deleteUserAndSessionsUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingGET**
> deleteUserAndSessionsUsingGET($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingGET($user);
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->deleteUserAndSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingPOST**
> deleteUserAndSessionsUsingPOST($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\HousekeepingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingPOST($user);
} catch (Exception $e) {
    echo 'Exception when calling HousekeepingApi->deleteUserAndSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

