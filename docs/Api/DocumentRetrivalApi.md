# Insign\DocumentRetrivalApi

All URIs are relative to *http://is2-cloud.test.flexperto.com/is2-horn-8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**downloadDocumentsMultiUsingGET1**](DocumentRetrivalApi.md#downloadDocumentsMultiUsingGET1) | **GET** /get/documents/downloadmulti | Download several session documents as zips in zip file
[**downloadDocumentsMultiUsingPOST1**](DocumentRetrivalApi.md#downloadDocumentsMultiUsingPOST1) | **POST** /get/documents/downloadmulti | Download several session documents as zips in zip file
[**downloadDocumentsProtectedUsingGET**](DocumentRetrivalApi.md#downloadDocumentsProtectedUsingGET) | **GET** /mail/documents/download | Retrieve all sessions documents as single zip file
[**downloadDocumentsProtectedUsingPOST**](DocumentRetrivalApi.md#downloadDocumentsProtectedUsingPOST) | **POST** /mail/documents/download | Retrieve all sessions documents as single zip file
[**downloadDocumentsUsingGET1**](DocumentRetrivalApi.md#downloadDocumentsUsingGET1) | **GET** /get/audit/download | Download the audit report for a process
[**downloadDocumentsUsingGET2**](DocumentRetrivalApi.md#downloadDocumentsUsingGET2) | **GET** /get/documents/download | Download all sessions documents as single zip file
[**downloadDocumentsUsingPOST1**](DocumentRetrivalApi.md#downloadDocumentsUsingPOST1) | **POST** /get/audit/download | Download the audit report for a process
[**downloadDocumentsUsingPOST2**](DocumentRetrivalApi.md#downloadDocumentsUsingPOST2) | **POST** /get/documents/download | Download all sessions documents as single zip file
[**getDocumentUsingGET**](DocumentRetrivalApi.md#getDocumentUsingGET) | **GET** /get/document | Download single document containing the current signatures and modifications
[**getDocumentUsingPOST**](DocumentRetrivalApi.md#getDocumentUsingPOST) | **POST** /get/document | Download single document containing the current signatures and modifications
[**getEnvelopedDocumentsUsingGET1**](DocumentRetrivalApi.md#getEnvelopedDocumentsUsingGET1) | **GET** /get/documents/enveloped | Attach sessions current documents to the provided container pdf-file
[**getEnvelopedDocumentsUsingPOST1**](DocumentRetrivalApi.md#getEnvelopedDocumentsUsingPOST1) | **POST** /get/documents/enveloped | Attach sessions current documents to the provided container pdf-file
[**getEnvelopedDocumentsUsingPUT1**](DocumentRetrivalApi.md#getEnvelopedDocumentsUsingPUT1) | **PUT** /get/documents/enveloped | Attach sessions current documents to the provided container pdf-file
[**recoverdeleteSessionsUsingDELETE**](DocumentRetrivalApi.md#recoverdeleteSessionsUsingDELETE) | **DELETE** /persistence/recoverdeleted | Retrieve all sessions documents as single zip file
[**recoverdeleteSessionsUsingGET**](DocumentRetrivalApi.md#recoverdeleteSessionsUsingGET) | **GET** /persistence/recoverdeleted | Retrieve all sessions documents as single zip file
[**recoverdeleteSessionsUsingPOST**](DocumentRetrivalApi.md#recoverdeleteSessionsUsingPOST) | **POST** /persistence/recoverdeleted | Retrieve all sessions documents as single zip file


# **downloadDocumentsMultiUsingGET1**
> downloadDocumentsMultiUsingGET1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download several session documents as zips in zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = array("sessionid_example"); // string[] | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsMultiUsingGET1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->downloadDocumentsMultiUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | [**string[]**](../Model/string.md)| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsMultiUsingPOST1**
> downloadDocumentsMultiUsingPOST1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download several session documents as zips in zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = array("sessionid_example"); // string[] | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsMultiUsingPOST1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->downloadDocumentsMultiUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | [**string[]**](../Model/string.md)| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsProtectedUsingGET**
> downloadDocumentsProtectedUsingGET($sessionid, $bestaetigentoken, $mustberead)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$bestaetigentoken = "bestaetigentoken_example"; // string | bestaetigentoken
$mustberead = true; // bool | mustberead

try {
    $apiInstance->downloadDocumentsProtectedUsingGET($sessionid, $bestaetigentoken, $mustberead);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->downloadDocumentsProtectedUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **bestaetigentoken** | **string**| bestaetigentoken | [optional]
 **mustberead** | **bool**| mustberead | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsProtectedUsingPOST**
> downloadDocumentsProtectedUsingPOST($sessionid, $bestaetigentoken, $mustberead)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$bestaetigentoken = "bestaetigentoken_example"; // string | bestaetigentoken
$mustberead = true; // bool | mustberead

try {
    $apiInstance->downloadDocumentsProtectedUsingPOST($sessionid, $bestaetigentoken, $mustberead);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->downloadDocumentsProtectedUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **bestaetigentoken** | **string**| bestaetigentoken | [optional]
 **mustberead** | **bool**| mustberead | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsUsingGET1**
> downloadDocumentsUsingGET1($sessionid)

Download the audit report for a process

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $apiInstance->downloadDocumentsUsingGET1($sessionid);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->downloadDocumentsUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsUsingGET2**
> downloadDocumentsUsingGET2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsUsingGET2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->downloadDocumentsUsingGET2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsUsingPOST1**
> downloadDocumentsUsingPOST1($sessionid)

Download the audit report for a process

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $apiInstance->downloadDocumentsUsingPOST1($sessionid);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->downloadDocumentsUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsUsingPOST2**
> downloadDocumentsUsingPOST2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsUsingPOST2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->downloadDocumentsUsingPOST2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDocumentUsingGET**
> getDocumentUsingGET($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type)

Download single document containing the current signatures and modifications

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid
$externtoken = "externtoken_example"; // string | Token for external access if access by SSO User. Only used internally
$includebiodata = true; // bool | Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat).
$originalfile = false; // bool | Get the original file that was initially uploaded? Default: false.
$type = "type_example"; // string | Type of download. Internal use only. type='form' to keep form field in the document and skip signatures.

try {
    $apiInstance->getDocumentUsingGET($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->getDocumentUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |
 **externtoken** | **string**| Token for external access if access by SSO User. Only used internally | [optional]
 **includebiodata** | **bool**| Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat). | [optional] [default to true]
 **originalfile** | **bool**| Get the original file that was initially uploaded? Default: false. | [optional] [default to false]
 **type** | **string**| Type of download. Internal use only. type&#x3D;&#39;form&#39; to keep form field in the document and skip signatures. | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDocumentUsingPOST**
> getDocumentUsingPOST($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type)

Download single document containing the current signatures and modifications

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid
$externtoken = "externtoken_example"; // string | Token for external access if access by SSO User. Only used internally
$includebiodata = true; // bool | Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat).
$originalfile = false; // bool | Get the original file that was initially uploaded? Default: false.
$type = "type_example"; // string | Type of download. Internal use only. type='form' to keep form field in the document and skip signatures.

try {
    $apiInstance->getDocumentUsingPOST($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->getDocumentUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |
 **externtoken** | **string**| Token for external access if access by SSO User. Only used internally | [optional]
 **includebiodata** | **bool**| Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat). | [optional] [default to true]
 **originalfile** | **bool**| Get the original file that was initially uploaded? Default: false. | [optional] [default to false]
 **type** | **string**| Type of download. Internal use only. type&#x3D;&#39;form&#39; to keep form field in the document and skip signatures. | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEnvelopedDocumentsUsingGET1**
> getEnvelopedDocumentsUsingGET1($sessionid, $embed_bio_data, $file)

Attach sessions current documents to the provided container pdf-file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$embed_bio_data = true; // bool | embedBioData
$file = "/path/to/file.txt"; // \SplFileObject | file

try {
    $apiInstance->getEnvelopedDocumentsUsingGET1($sessionid, $embed_bio_data, $file);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->getEnvelopedDocumentsUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **embed_bio_data** | **bool**| embedBioData | [optional] [default to true]
 **file** | **\SplFileObject**| file | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/pdf

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEnvelopedDocumentsUsingPOST1**
> getEnvelopedDocumentsUsingPOST1($sessionid, $embed_bio_data, $file)

Attach sessions current documents to the provided container pdf-file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = new \stdClass; // object | sessionid
$embed_bio_data = new \stdClass; // object | embedBioData
$file = "/path/to/file.txt"; // \SplFileObject | file

try {
    $apiInstance->getEnvelopedDocumentsUsingPOST1($sessionid, $embed_bio_data, $file);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->getEnvelopedDocumentsUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | [**object**](../Model/.md)| sessionid |
 **embed_bio_data** | [**object**](../Model/.md)| embedBioData | [optional]
 **file** | **\SplFileObject**| file | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/pdf

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEnvelopedDocumentsUsingPUT1**
> getEnvelopedDocumentsUsingPUT1($sessionid, $embed_bio_data, $file)

Attach sessions current documents to the provided container pdf-file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$embed_bio_data = true; // bool | embedBioData
$file = "/path/to/file.txt"; // \SplFileObject | file

try {
    $apiInstance->getEnvelopedDocumentsUsingPUT1($sessionid, $embed_bio_data, $file);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->getEnvelopedDocumentsUsingPUT1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **embed_bio_data** | **bool**| embedBioData | [optional] [default to true]
 **file** | **\SplFileObject**| file | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/pdf

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recoverdeleteSessionsUsingDELETE**
> \Insign\Model\BasicResult recoverdeleteSessionsUsingDELETE($sessionid)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->recoverdeleteSessionsUsingDELETE($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->recoverdeleteSessionsUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recoverdeleteSessionsUsingGET**
> \Insign\Model\BasicResult recoverdeleteSessionsUsingGET($sessionid)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->recoverdeleteSessionsUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->recoverdeleteSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recoverdeleteSessionsUsingPOST**
> \Insign\Model\BasicResult recoverdeleteSessionsUsingPOST($sessionid)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\DocumentRetrivalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->recoverdeleteSessionsUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DocumentRetrivalApi->recoverdeleteSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

