# Insign\ProcessHandlingApi

All URIs are relative to *http://is2-cloud.test.flexperto.com/is2-horn-8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delegateUsingPOST**](ProcessHandlingApi.md#delegateUsingPOST) | **POST** /extern/delegate | Redirect extern process to another user
[**delegateUsingPUT**](ProcessHandlingApi.md#delegateUsingPUT) | **PUT** /extern/delegate | Redirect extern process to another user
[**deleteSessionUsingDELETE**](ProcessHandlingApi.md#deleteSessionUsingDELETE) | **DELETE** /persistence/delete | Mark session as deleted
[**deleteSessionUsingGET**](ProcessHandlingApi.md#deleteSessionUsingGET) | **GET** /persistence/delete | Mark session as deleted
[**deleteSessionUsingPOST**](ProcessHandlingApi.md#deleteSessionUsingPOST) | **POST** /persistence/delete | Mark session as deleted
[**exportSessionUsingGET**](ProcessHandlingApi.md#exportSessionUsingGET) | **GET** /exportSession | Export a session as binary data.
[**exportSessionUsingPOST**](ProcessHandlingApi.md#exportSessionUsingPOST) | **POST** /exportSession | Export a session as binary data.
[**getExternInfosUsingGET**](ProcessHandlingApi.md#getExternInfosUsingGET) | **GET** /get/externInfos | Additional status information about session that is in external state
[**getExternInfosUsingPOST**](ProcessHandlingApi.md#getExternInfosUsingPOST) | **POST** /get/externInfos | Additional status information about session that is in external state
[**getSessionInfoUsingGET**](ProcessHandlingApi.md#getSessionInfoUsingGET) | **GET** /getSessionInfoForExport | Get session metadata for export
[**getSessionInfoUsingPOST**](ProcessHandlingApi.md#getSessionInfoUsingPOST) | **POST** /getSessionInfoForExport | Get session metadata for export
[**getSessionsUsingGET3**](ProcessHandlingApi.md#getSessionsUsingGET3) | **GET** /get/usersessions | List of session that belong to a single user. You can also pass a user list separated by &#39;separator&#39; for multiple users
[**getSessionsUsingPOST1**](ProcessHandlingApi.md#getSessionsUsingPOST1) | **POST** /get/querysessions | Retrieve information about a bunch of sessions
[**getSessionsUsingPOST3**](ProcessHandlingApi.md#getSessionsUsingPOST3) | **POST** /get/usersessions | List of session that belong to a single user. You can also pass a user list separated by &#39;separator&#39; for multiple users
[**importSessionUsingGET**](ProcessHandlingApi.md#importSessionUsingGET) | **GET** /importSession | Import binary session data
[**importSessionUsingPOST**](ProcessHandlingApi.md#importSessionUsingPOST) | **POST** /importSession | Import binary session data
[**loadSessionUsingPOST**](ProcessHandlingApi.md#loadSessionUsingPOST) | **POST** /persistence/loadsession | Load session from database
[**newTokenUsingGET**](ProcessHandlingApi.md#newTokenUsingGET) | **GET** /extern/newtoken | Reaktivate external user token and create new password for user if token was renewed
[**newTokenUsingPOST**](ProcessHandlingApi.md#newTokenUsingPOST) | **POST** /extern/newtoken | Reaktivate external user token and create new password for user if token was renewed
[**newTokenUsingPUT**](ProcessHandlingApi.md#newTokenUsingPUT) | **PUT** /extern/newtoken | Reaktivate external user token and create new password for user if token was renewed
[**purgeSessionUsingDELETE**](ProcessHandlingApi.md#purgeSessionUsingDELETE) | **DELETE** /persistence/purge | Remove session by TAN oder sessionid. Removes session from memory.
[**purgeSessionUsingGET**](ProcessHandlingApi.md#purgeSessionUsingGET) | **GET** /persistence/purge | Remove session by TAN oder sessionid. Removes session from memory.
[**purgeSessionUsingPOST**](ProcessHandlingApi.md#purgeSessionUsingPOST) | **POST** /persistence/purge | Remove session by TAN oder sessionid. Removes session from memory.
[**reconfigureSessionUsingPOST**](ProcessHandlingApi.md#reconfigureSessionUsingPOST) | **POST** /reconfigureSession | Reconfigure a session
[**resetSignatureUsingPOST**](ProcessHandlingApi.md#resetSignatureUsingPOST) | **POST** /extern/resetlastSignature | Redirect extern process to another user
[**resetSignatureUsingPUT**](ProcessHandlingApi.md#resetSignatureUsingPUT) | **PUT** /extern/resetlastSignature | Redirect extern process to another user
[**resetSignaturesUsingGET**](ProcessHandlingApi.md#resetSignaturesUsingGET) | **GET** /configure/resetsignatures | Reset sessions signatures
[**resetSignaturesUsingPOST**](ProcessHandlingApi.md#resetSignaturesUsingPOST) | **POST** /configure/resetsignatures | Reset sessions signatures
[**restartsessionUsingGET**](ProcessHandlingApi.md#restartsessionUsingGET) | **GET** /configure/restartsession | Reset sessions information
[**restartsessionUsingPOST**](ProcessHandlingApi.md#restartsessionUsingPOST) | **POST** /configure/restartsession | Reset sessions information
[**sessionloeschenUsingGET**](ProcessHandlingApi.md#sessionloeschenUsingGET) | **GET** /configure/ablehnen | Abort and delete session
[**sessionloeschenUsingPOST**](ProcessHandlingApi.md#sessionloeschenUsingPOST) | **POST** /configure/ablehnen | Abort and delete session
[**setExternalUsingPOST**](ProcessHandlingApi.md#setExternalUsingPOST) | **POST** /extern/begin | Start extern mode. Uses /beginmulti instead.
[**setExternalUsingPOST1**](ProcessHandlingApi.md#setExternalUsingPOST1) | **POST** /extern/beginmulti | Start extern mode and creates users for this mode.
[**setExternalUsingPUT**](ProcessHandlingApi.md#setExternalUsingPUT) | **PUT** /extern/begin | Start extern mode. Uses /beginmulti instead.
[**setExternalUsingPUT1**](ProcessHandlingApi.md#setExternalUsingPUT1) | **PUT** /extern/beginmulti | Start extern mode and creates users for this mode.
[**setcallbackurlUsingGET**](ProcessHandlingApi.md#setcallbackurlUsingGET) | **GET** /configure/setcallbackurl | Modify sessions callbackURL
[**setcallbackurlUsingPOST**](ProcessHandlingApi.md#setcallbackurlUsingPOST) | **POST** /configure/setcallbackurl | Modify sessions callbackURL
[**updateExternUserUsingPOST**](ProcessHandlingApi.md#updateExternUserUsingPOST) | **POST** /extern/updateUser | Update external user information
[**updateExternUserUsingPUT**](ProcessHandlingApi.md#updateExternUserUsingPUT) | **PUT** /extern/updateUser | Update external user information
[**updateTokenUsingPOST**](ProcessHandlingApi.md#updateTokenUsingPOST) | **POST** /extern/updatetoken | Reaktivate external user token and create new password for user
[**updateTokenUsingPUT**](ProcessHandlingApi.md#updateTokenUsingPUT) | **PUT** /extern/updatetoken | Reaktivate external user token and create new password for user
[**zurueckholenUsingPOST**](ProcessHandlingApi.md#zurueckholenUsingPOST) | **POST** /extern/abort | Return session from external state
[**zurueckholenUsingPUT**](ProcessHandlingApi.md#zurueckholenUsingPUT) | **PUT** /extern/abort | Return session from external state


# **delegateUsingPOST**
> \Insign\Model\BasicResult delegateUsingPOST($input, $sessionid)

Redirect extern process to another user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ExternDelegate(); // \Insign\Model\ExternDelegate | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->delegateUsingPOST($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->delegateUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ExternDelegate**](../Model/ExternDelegate.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **delegateUsingPUT**
> \Insign\Model\BasicResult delegateUsingPUT($input, $sessionid)

Redirect extern process to another user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ExternDelegate(); // \Insign\Model\ExternDelegate | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->delegateUsingPUT($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->delegateUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ExternDelegate**](../Model/ExternDelegate.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSessionUsingDELETE**
> \Insign\Model\BasicResult deleteSessionUsingDELETE($sessionid, $sessionidtodelete)

Mark session as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->deleteSessionUsingDELETE($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->deleteSessionUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSessionUsingGET**
> \Insign\Model\BasicResult deleteSessionUsingGET($sessionid, $sessionidtodelete)

Mark session as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->deleteSessionUsingGET($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->deleteSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSessionUsingPOST**
> \Insign\Model\BasicResult deleteSessionUsingPOST($sessionid, $sessionidtodelete)

Mark session as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->deleteSessionUsingPOST($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->deleteSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **exportSessionUsingGET**
> exportSessionUsingGET($compression_format, $compression_level, $session_id)

Export a session as binary data.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$compression_format = "compression_format_example"; // string | compression format may use 'gzip'
$compression_level = 56; // int | 1 to 9
$session_id = "session_id_example"; // string | session id

try {
    $apiInstance->exportSessionUsingGET($compression_format, $compression_level, $session_id);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->exportSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **compression_format** | **string**| compression format may use &#39;gzip&#39; | [optional]
 **compression_level** | **int**| 1 to 9 | [optional]
 **session_id** | **string**| session id | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **exportSessionUsingPOST**
> exportSessionUsingPOST($compression_format, $compression_level, $session_id)

Export a session as binary data.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$compression_format = "compression_format_example"; // string | compression format may use 'gzip'
$compression_level = 56; // int | 1 to 9
$session_id = "session_id_example"; // string | session id

try {
    $apiInstance->exportSessionUsingPOST($compression_format, $compression_level, $session_id);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->exportSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **compression_format** | **string**| compression format may use &#39;gzip&#39; | [optional]
 **compression_level** | **int**| 1 to 9 | [optional]
 **session_id** | **string**| session id | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExternInfosUsingGET**
> \Insign\Model\ExternUserInfo[] getExternInfosUsingGET($sessionid, $id_vorgangsverwaltung)

Additional status information about session that is in external state

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getExternInfosUsingGET($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->getExternInfosUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternUserInfo[]**](../Model/ExternUserInfo.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExternInfosUsingPOST**
> \Insign\Model\ExternUserInfo[] getExternInfosUsingPOST($sessionid, $id_vorgangsverwaltung)

Additional status information about session that is in external state

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getExternInfosUsingPOST($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->getExternInfosUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternUserInfo[]**](../Model/ExternUserInfo.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionInfoUsingGET**
> \Insign\Model\InformationAboutASession getSessionInfoUsingGET($session_id)

Get session metadata for export

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$session_id = "session_id_example"; // string | sessionId

try {
    $result = $apiInstance->getSessionInfoUsingGET($session_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->getSessionInfoUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **string**| sessionId |

### Return type

[**\Insign\Model\InformationAboutASession**](../Model/InformationAboutASession.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionInfoUsingPOST**
> \Insign\Model\InformationAboutASession getSessionInfoUsingPOST($session_id)

Get session metadata for export

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$session_id = "session_id_example"; // string | sessionId

try {
    $result = $apiInstance->getSessionInfoUsingPOST($session_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->getSessionInfoUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **string**| sessionId |

### Return type

[**\Insign\Model\InformationAboutASession**](../Model/InformationAboutASession.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingGET3**
> \Insign\Model\SessionsResult getSessionsUsingGET3($separator, $user)

List of session that belong to a single user. You can also pass a user list separated by 'separator' for multiple users

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$separator = "separator_example"; // string | Separator for users (only required using list)
$user = "user_example"; // string | User ID(s)

try {
    $result = $apiInstance->getSessionsUsingGET3($separator, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->getSessionsUsingGET3: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **separator** | **string**| Separator for users (only required using list) | [optional]
 **user** | **string**| User ID(s) | [optional]

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingPOST1**
> \Insign\Model\SessionsResult getSessionsUsingPOST1($iso3_country, $iso3_language, $country, $display_country, $display_language, $display_name, $display_script, $display_variant, $language, $script, $sessionids, $unicode_locale_attributes, $unicode_locale_keys, $variant)

Retrieve information about a bunch of sessions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$iso3_country = "iso3_country_example"; // string | 
$iso3_language = "iso3_language_example"; // string | 
$country = "country_example"; // string | 
$display_country = "display_country_example"; // string | 
$display_language = "display_language_example"; // string | 
$display_name = "display_name_example"; // string | 
$display_script = "display_script_example"; // string | 
$display_variant = "display_variant_example"; // string | 
$language = "language_example"; // string | 
$script = "script_example"; // string | 
$sessionids = new \Insign\Model\ListOfSessionIDs(); // \Insign\Model\ListOfSessionIDs | list of session to retrieve information about
$unicode_locale_attributes = array("unicode_locale_attributes_example"); // string[] | 
$unicode_locale_keys = array("unicode_locale_keys_example"); // string[] | 
$variant = "variant_example"; // string | 

try {
    $result = $apiInstance->getSessionsUsingPOST1($iso3_country, $iso3_language, $country, $display_country, $display_language, $display_name, $display_script, $display_variant, $language, $script, $sessionids, $unicode_locale_attributes, $unicode_locale_keys, $variant);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->getSessionsUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iso3_country** | **string**|  | [optional]
 **iso3_language** | **string**|  | [optional]
 **country** | **string**|  | [optional]
 **display_country** | **string**|  | [optional]
 **display_language** | **string**|  | [optional]
 **display_name** | **string**|  | [optional]
 **display_script** | **string**|  | [optional]
 **display_variant** | **string**|  | [optional]
 **language** | **string**|  | [optional]
 **script** | **string**|  | [optional]
 **sessionids** | [**\Insign\Model\ListOfSessionIDs**](../Model/ListOfSessionIDs.md)| list of session to retrieve information about | [optional]
 **unicode_locale_attributes** | [**string[]**](../Model/string.md)|  | [optional]
 **unicode_locale_keys** | [**string[]**](../Model/string.md)|  | [optional]
 **variant** | **string**|  | [optional]

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingPOST3**
> \Insign\Model\SessionsResult getSessionsUsingPOST3($separator, $user)

List of session that belong to a single user. You can also pass a user list separated by 'separator' for multiple users

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$separator = "separator_example"; // string | Separator for users (only required using list)
$user = "user_example"; // string | User ID(s)

try {
    $result = $apiInstance->getSessionsUsingPOST3($separator, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->getSessionsUsingPOST3: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **separator** | **string**| Separator for users (only required using list) | [optional]
 **user** | **string**| User ID(s) | [optional]

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **importSessionUsingGET**
> \Insign\Model\SessionResult importSessionUsingGET()

Import binary session data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->importSessionUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->importSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Insign\Model\SessionResult**](../Model/SessionResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/octet-stream
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **importSessionUsingPOST**
> \Insign\Model\SessionResult importSessionUsingPOST()

Import binary session data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->importSessionUsingPOST();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->importSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Insign\Model\SessionResult**](../Model/SessionResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/octet-stream
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **loadSessionUsingPOST**
> \Insign\Model\SessionResult loadSessionUsingPOST($input, $sessionid, $sessionidtoload)

Load session from database

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ConfigurationInformationForLoadingAProcess(); // \Insign\Model\ConfigurationInformationForLoadingAProcess | input
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtoload = "sessionidtoload_example"; // string | sessionidtoload

try {
    $result = $apiInstance->loadSessionUsingPOST($input, $sessionid, $sessionidtoload);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->loadSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ConfigurationInformationForLoadingAProcess**](../Model/ConfigurationInformationForLoadingAProcess.md)| input | [optional]
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtoload** | **string**| sessionidtoload | [optional]

### Return type

[**\Insign\Model\SessionResult**](../Model/SessionResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **newTokenUsingGET**
> \Insign\Model\ExternUserResult newTokenUsingGET($sessionid, $token)

Reaktivate external user token and create new password for user if token was renewed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->newTokenUsingGET($sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->newTokenUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **newTokenUsingPOST**
> \Insign\Model\ExternUserResult newTokenUsingPOST($sessionid, $token)

Reaktivate external user token and create new password for user if token was renewed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->newTokenUsingPOST($sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->newTokenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **newTokenUsingPUT**
> \Insign\Model\ExternUserResult newTokenUsingPUT($sessionid, $token)

Reaktivate external user token and create new password for user if token was renewed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->newTokenUsingPUT($sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->newTokenUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purgeSessionUsingDELETE**
> \Insign\Model\BasicResult purgeSessionUsingDELETE($sessionid, $sessionidtodelete)

Remove session by TAN oder sessionid. Removes session from memory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->purgeSessionUsingDELETE($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->purgeSessionUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purgeSessionUsingGET**
> \Insign\Model\BasicResult purgeSessionUsingGET($sessionid, $sessionidtodelete)

Remove session by TAN oder sessionid. Removes session from memory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->purgeSessionUsingGET($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->purgeSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purgeSessionUsingPOST**
> \Insign\Model\BasicResult purgeSessionUsingPOST($sessionid, $sessionidtodelete)

Remove session by TAN oder sessionid. Removes session from memory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->purgeSessionUsingPOST($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->purgeSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **reconfigureSessionUsingPOST**
> \Insign\Model\Result reconfigureSessionUsingPOST($configure, $session_id)

Reconfigure a session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$configure = new \Insign\Model\ConfigureSession(); // \Insign\Model\ConfigureSession | configure
$session_id = "session_id_example"; // string | sessionId

try {
    $result = $apiInstance->reconfigureSessionUsingPOST($configure, $session_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->reconfigureSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **configure** | [**\Insign\Model\ConfigureSession**](../Model/ConfigureSession.md)| configure |
 **session_id** | **string**| sessionId |

### Return type

[**\Insign\Model\Result**](../Model/Result.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetSignatureUsingPOST**
> \Insign\Model\RestSingleSignature resetSignatureUsingPOST($data, $sessionid)

Redirect extern process to another user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$data = new \Insign\Model\DeleteSignatureExtern(); // \Insign\Model\DeleteSignatureExtern | data
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->resetSignatureUsingPOST($data, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->resetSignatureUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Insign\Model\DeleteSignatureExtern**](../Model/DeleteSignatureExtern.md)| data |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RestSingleSignature**](../Model/RestSingleSignature.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetSignatureUsingPUT**
> \Insign\Model\RestSingleSignature resetSignatureUsingPUT($data, $sessionid)

Redirect extern process to another user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$data = new \Insign\Model\DeleteSignatureExtern(); // \Insign\Model\DeleteSignatureExtern | data
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->resetSignatureUsingPUT($data, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->resetSignatureUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Insign\Model\DeleteSignatureExtern**](../Model/DeleteSignatureExtern.md)| data |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RestSingleSignature**](../Model/RestSingleSignature.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetSignaturesUsingGET**
> \Insign\Model\Result resetSignaturesUsingGET($sessionid, $docid)

Reset sessions signatures

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$docid = "docid_example"; // string | docid

try {
    $result = $apiInstance->resetSignaturesUsingGET($sessionid, $docid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->resetSignaturesUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **docid** | **string**| docid | [optional]

### Return type

[**\Insign\Model\Result**](../Model/Result.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetSignaturesUsingPOST**
> \Insign\Model\Result resetSignaturesUsingPOST($sessionid, $docid)

Reset sessions signatures

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$docid = "docid_example"; // string | docid

try {
    $result = $apiInstance->resetSignaturesUsingPOST($sessionid, $docid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->resetSignaturesUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **docid** | **string**| docid | [optional]

### Return type

[**\Insign\Model\Result**](../Model/Result.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restartsessionUsingGET**
> \Insign\Model\RedirectView restartsessionUsingGET($sessionid)

Reset sessions information

Reset documents to inital state after upload. Clear all signatures and input fields. Generate new TAN number.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->restartsessionUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->restartsessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restartsessionUsingPOST**
> \Insign\Model\RedirectView restartsessionUsingPOST($sessionid)

Reset sessions information

Reset documents to inital state after upload. Clear all signatures and input fields. Generate new TAN number.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->restartsessionUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->restartsessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sessionloeschenUsingGET**
> object sessionloeschenUsingGET($sessionid, $gdpr_declined)

Abort and delete session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$gdpr_declined = true; // bool | gdprDeclined

try {
    $result = $apiInstance->sessionloeschenUsingGET($sessionid, $gdpr_declined);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->sessionloeschenUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **gdpr_declined** | **bool**| gdprDeclined | [optional]

### Return type

**object**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sessionloeschenUsingPOST**
> object sessionloeschenUsingPOST($sessionid, $gdpr_declined)

Abort and delete session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$gdpr_declined = true; // bool | gdprDeclined

try {
    $result = $apiInstance->sessionloeschenUsingPOST($sessionid, $gdpr_declined);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->sessionloeschenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **gdpr_declined** | **bool**| gdprDeclined | [optional]

### Return type

**object**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setExternalUsingPOST**
> \Insign\Model\ExternUserResult setExternalUsingPOST($input, $sessionid, $id_vorgangsverwaltung)

Start extern mode. Uses /beginmulti instead.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\StartExtern(); // \Insign\Model\StartExtern | input
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->setExternalUsingPOST($input, $sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->setExternalUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\StartExtern**](../Model/StartExtern.md)| input |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setExternalUsingPOST1**
> \Insign\Model\ExternMultiuserResult setExternalUsingPOST1($input, $sessionid, $id_vorgangsverwaltung)

Start extern mode and creates users for this mode.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ContainerForMultipleExternalUsers(); // \Insign\Model\ContainerForMultipleExternalUsers | input
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->setExternalUsingPOST1($input, $sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->setExternalUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ContainerForMultipleExternalUsers**](../Model/ContainerForMultipleExternalUsers.md)| input |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternMultiuserResult**](../Model/ExternMultiuserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setExternalUsingPUT**
> \Insign\Model\ExternUserResult setExternalUsingPUT($input, $sessionid, $id_vorgangsverwaltung)

Start extern mode. Uses /beginmulti instead.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\StartExtern(); // \Insign\Model\StartExtern | input
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->setExternalUsingPUT($input, $sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->setExternalUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\StartExtern**](../Model/StartExtern.md)| input |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setExternalUsingPUT1**
> \Insign\Model\ExternMultiuserResult setExternalUsingPUT1($input, $sessionid, $id_vorgangsverwaltung)

Start extern mode and creates users for this mode.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ContainerForMultipleExternalUsers(); // \Insign\Model\ContainerForMultipleExternalUsers | input
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->setExternalUsingPUT1($input, $sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->setExternalUsingPUT1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ContainerForMultipleExternalUsers**](../Model/ContainerForMultipleExternalUsers.md)| input |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternMultiuserResult**](../Model/ExternMultiuserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setcallbackurlUsingGET**
> \Insign\Model\SessionResult setcallbackurlUsingGET($sessionid, $callbackurl, $callbackurlabschliessen)

Modify sessions callbackURL

If you want to modify the callbackURL property of an existing session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$callbackurl = "callbackurl_example"; // string | callbackurl
$callbackurlabschliessen = "callbackurlabschliessen_example"; // string | callbackurlabschliessen

try {
    $result = $apiInstance->setcallbackurlUsingGET($sessionid, $callbackurl, $callbackurlabschliessen);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->setcallbackurlUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **callbackurl** | **string**| callbackurl | [optional]
 **callbackurlabschliessen** | **string**| callbackurlabschliessen | [optional]

### Return type

[**\Insign\Model\SessionResult**](../Model/SessionResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setcallbackurlUsingPOST**
> \Insign\Model\SessionResult setcallbackurlUsingPOST($sessionid, $callbackurl, $callbackurlabschliessen)

Modify sessions callbackURL

If you want to modify the callbackURL property of an existing session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$callbackurl = "callbackurl_example"; // string | callbackurl
$callbackurlabschliessen = "callbackurlabschliessen_example"; // string | callbackurlabschliessen

try {
    $result = $apiInstance->setcallbackurlUsingPOST($sessionid, $callbackurl, $callbackurlabschliessen);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->setcallbackurlUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **callbackurl** | **string**| callbackurl | [optional]
 **callbackurlabschliessen** | **string**| callbackurlabschliessen | [optional]

### Return type

[**\Insign\Model\SessionResult**](../Model/SessionResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExternUserUsingPOST**
> \Insign\Model\ExternUserResult updateExternUserUsingPOST($input, $sessionid, $token)

Update external user information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\StartExtern(); // \Insign\Model\StartExtern | input
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->updateExternUserUsingPOST($input, $sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->updateExternUserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\StartExtern**](../Model/StartExtern.md)| input |
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExternUserUsingPUT**
> \Insign\Model\ExternUserResult updateExternUserUsingPUT($input, $sessionid, $token)

Update external user information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\StartExtern(); // \Insign\Model\StartExtern | input
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->updateExternUserUsingPUT($input, $sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->updateExternUserUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\StartExtern**](../Model/StartExtern.md)| input |
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTokenUsingPOST**
> \Insign\Model\ExternUserResult updateTokenUsingPOST($input, $sessionid)

Reaktivate external user token and create new password for user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ExternUserResult(); // \Insign\Model\ExternUserResult | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->updateTokenUsingPOST($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->updateTokenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTokenUsingPUT**
> \Insign\Model\ExternUserResult updateTokenUsingPUT($input, $sessionid)

Reaktivate external user token and create new password for user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ExternUserResult(); // \Insign\Model\ExternUserResult | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->updateTokenUsingPUT($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->updateTokenUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **zurueckholenUsingPOST**
> \Insign\Model\BasicResult zurueckholenUsingPOST($client_info, $oldsessionid, $sessionid)

Return session from external state

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$client_info = new \Insign\Model\ClientInfo(); // \Insign\Model\ClientInfo | clientInfo
$oldsessionid = "oldsessionid_example"; // string | oldsessionid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->zurueckholenUsingPOST($client_info, $oldsessionid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->zurueckholenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_info** | [**\Insign\Model\ClientInfo**](../Model/ClientInfo.md)| clientInfo |
 **oldsessionid** | **string**| oldsessionid |
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **zurueckholenUsingPUT**
> \Insign\Model\BasicResult zurueckholenUsingPUT($client_info, $oldsessionid, $sessionid)

Return session from external state

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\ProcessHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$client_info = new \Insign\Model\ClientInfo(); // \Insign\Model\ClientInfo | clientInfo
$oldsessionid = "oldsessionid_example"; // string | oldsessionid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->zurueckholenUsingPUT($client_info, $oldsessionid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProcessHandlingApi->zurueckholenUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_info** | [**\Insign\Model\ClientInfo**](../Model/ClientInfo.md)| clientInfo |
 **oldsessionid** | **string**| oldsessionid |
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

