# Insign\SessionCreationApi

All URIs are relative to *http://is2-cloud.test.flexperto.com/is2-horn-8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**configuredocumentsUsingPOST**](SessionCreationApi.md#configuredocumentsUsingPOST) | **POST** /configure/session | Creates a new session with the provided metadata for the documents
[**createVorgangsverwaltungSessionUsingGET**](SessionCreationApi.md#createVorgangsverwaltungSessionUsingGET) | **GET** /configure/sessionVorgangsverwaltung | Creates a new session for process management
[**createVorgangsverwaltungSessionUsingPOST**](SessionCreationApi.md#createVorgangsverwaltungSessionUsingPOST) | **POST** /configure/sessionVorgangsverwaltung | Creates a new session for process management
[**createconfigtemplateUsingPOST**](SessionCreationApi.md#createconfigtemplateUsingPOST) | **POST** /configure/createconfigtemplate | Add additional session configuration per mandant. If a configuration already exists, it will be updated.
[**uploaddocumentstreammultipartUsingPOST**](SessionCreationApi.md#uploaddocumentstreammultipartUsingPOST) | **POST** /configure/uploaddocument | Upload a document into a previously configured session
[**uploaddocumentstreammultipartieUsingPOST**](SessionCreationApi.md#uploaddocumentstreammultipartieUsingPOST) | **POST** /configure/uploaddocumentie | Upload a document into a previously configured session
[**uploadsummaryUsingPOST**](SessionCreationApi.md#uploadsummaryUsingPOST) | **POST** /configure/uploadsummary | Add summary page to a previously configured session


# **configuredocumentsUsingPOST**
> \Insign\Model\ConfigureDocumentsResult configuredocumentsUsingPOST($input, $configid)

Creates a new session with the provided metadata for the documents

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionCreationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ConfigureSession(); // \Insign\Model\ConfigureSession | input
$configid = "configid_example"; // string | configid

try {
    $result = $apiInstance->configuredocumentsUsingPOST($input, $configid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionCreationApi->configuredocumentsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ConfigureSession**](../Model/ConfigureSession.md)| input |
 **configid** | **string**| configid | [optional]

### Return type

[**\Insign\Model\ConfigureDocumentsResult**](../Model/ConfigureDocumentsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createVorgangsverwaltungSessionUsingGET**
> string createVorgangsverwaltungSessionUsingGET($sessionid, $from_editor)

Creates a new session for process management

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionCreationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$from_editor = true; // bool | fromEditor

try {
    $result = $apiInstance->createVorgangsverwaltungSessionUsingGET($sessionid, $from_editor);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionCreationApi->createVorgangsverwaltungSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **from_editor** | **bool**| fromEditor | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createVorgangsverwaltungSessionUsingPOST**
> string createVorgangsverwaltungSessionUsingPOST($sessionid, $from_editor)

Creates a new session for process management

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionCreationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$from_editor = true; // bool | fromEditor

try {
    $result = $apiInstance->createVorgangsverwaltungSessionUsingPOST($sessionid, $from_editor);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionCreationApi->createVorgangsverwaltungSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **from_editor** | **bool**| fromEditor | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createconfigtemplateUsingPOST**
> \Insign\Model\BasicResult createconfigtemplateUsingPOST($configid, $input)

Add additional session configuration per mandant. If a configuration already exists, it will be updated.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionCreationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$configid = "configid_example"; // string | configid
$input = new \Insign\Model\ConfigureSession(); // \Insign\Model\ConfigureSession | input

try {
    $result = $apiInstance->createconfigtemplateUsingPOST($configid, $input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionCreationApi->createconfigtemplateUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **configid** | **string**| configid |
 **input** | [**\Insign\Model\ConfigureSession**](../Model/ConfigureSession.md)| input |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploaddocumentstreammultipartUsingPOST**
> string uploaddocumentstreammultipartUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user)

Upload a document into a previously configured session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionCreationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filename = new \stdClass; // object | filename
$sessionid = new \stdClass; // object | sessionid
$docid = new \stdClass; // object | docid
$file = "/path/to/file.txt"; // \SplFileObject | file
$filesize = new \stdClass; // object | filesize
$pairing_token = new \stdClass; // object | pairingToken
$user = new \stdClass; // object | user

try {
    $result = $apiInstance->uploaddocumentstreammultipartUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionCreationApi->uploaddocumentstreammultipartUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filename** | [**object**](../Model/.md)| filename |
 **sessionid** | [**object**](../Model/.md)| sessionid |
 **docid** | [**object**](../Model/.md)| docid | [optional]
 **file** | **\SplFileObject**| file | [optional]
 **filesize** | [**object**](../Model/.md)| filesize | [optional]
 **pairing_token** | [**object**](../Model/.md)| pairingToken | [optional]
 **user** | [**object**](../Model/.md)| user | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploaddocumentstreammultipartieUsingPOST**
> string uploaddocumentstreammultipartieUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user)

Upload a document into a previously configured session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionCreationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filename = new \stdClass; // object | filename
$sessionid = new \stdClass; // object | sessionid
$docid = new \stdClass; // object | docid
$file = "/path/to/file.txt"; // \SplFileObject | file
$filesize = new \stdClass; // object | filesize
$pairing_token = new \stdClass; // object | pairingToken
$user = new \stdClass; // object | user

try {
    $result = $apiInstance->uploaddocumentstreammultipartieUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionCreationApi->uploaddocumentstreammultipartieUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filename** | [**object**](../Model/.md)| filename |
 **sessionid** | [**object**](../Model/.md)| sessionid |
 **docid** | [**object**](../Model/.md)| docid | [optional]
 **file** | **\SplFileObject**| file | [optional]
 **filesize** | [**object**](../Model/.md)| filesize | [optional]
 **pairing_token** | [**object**](../Model/.md)| pairingToken | [optional]
 **user** | [**object**](../Model/.md)| user | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploadsummaryUsingPOST**
> uploadsummaryUsingPOST($file, $sessionid, $foruser)

Add summary page to a previously configured session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionCreationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$file = "/path/to/file.txt"; // \SplFileObject | file
$sessionid = new \stdClass; // object | sessionid
$foruser = new \stdClass; // object | foruser

try {
    $apiInstance->uploadsummaryUsingPOST($file, $sessionid, $foruser);
} catch (Exception $e) {
    echo 'Exception when calling SessionCreationApi->uploadsummaryUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **\SplFileObject**| file |
 **sessionid** | [**object**](../Model/.md)| sessionid |
 **foruser** | [**object**](../Model/.md)| foruser | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

