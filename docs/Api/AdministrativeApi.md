# Insign\AdministrativeApi

All URIs are relative to *http://is2-cloud.test.flexperto.com/is2-horn-8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAuswertungJSONUsingGET**](AdministrativeApi.md#getAuswertungJSONUsingGET) | **GET** /auswertung/getjson | Get the accounting data for one or all tenants
[**systemCheckAutoJSONUsingGET**](AdministrativeApi.md#systemCheckAutoJSONUsingGET) | **GET** /admin/systemcheckauto | System health check
[**systemCheckAutoJSONUsingPOST**](AdministrativeApi.md#systemCheckAutoJSONUsingPOST) | **POST** /admin/systemcheckauto | System health check


# **getAuswertungJSONUsingGET**
> object getAuswertungJSONUsingGET($end, $start, $mandantenid)

Get the accounting data for one or all tenants

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AdministrativeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$end = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | end
$start = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | start
$mandantenid = "mandantenid_example"; // string | mandantenid

try {
    $result = $apiInstance->getAuswertungJSONUsingGET($end, $start, $mandantenid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdministrativeApi->getAuswertungJSONUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **end** | **\DateTime**| end |
 **start** | **\DateTime**| start |
 **mandantenid** | **string**| mandantenid | [optional]

### Return type

**object**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **systemCheckAutoJSONUsingGET**
> string systemCheckAutoJSONUsingGET($dbschemacheck)

System health check

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AdministrativeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dbschemacheck = false; // bool | dbschemacheck

try {
    $result = $apiInstance->systemCheckAutoJSONUsingGET($dbschemacheck);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdministrativeApi->systemCheckAutoJSONUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbschemacheck** | **bool**| dbschemacheck | [optional] [default to false]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain;charset=UTF-8, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **systemCheckAutoJSONUsingPOST**
> string systemCheckAutoJSONUsingPOST($dbschemacheck)

System health check

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AdministrativeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dbschemacheck = false; // bool | dbschemacheck

try {
    $result = $apiInstance->systemCheckAutoJSONUsingPOST($dbschemacheck);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdministrativeApi->systemCheckAutoJSONUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbschemacheck** | **bool**| dbschemacheck | [optional] [default to false]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain;charset=UTF-8, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

