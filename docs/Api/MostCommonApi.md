# Insign\MostCommonApi

All URIs are relative to *http://is2-cloud.test.flexperto.com/is2-horn-8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**configuredocumentsUsingPOST**](MostCommonApi.md#configuredocumentsUsingPOST) | **POST** /configure/session | Creates a new session with the provided metadata for the documents
[**createVorgangsverwaltungSessionUsingGET**](MostCommonApi.md#createVorgangsverwaltungSessionUsingGET) | **GET** /configure/sessionVorgangsverwaltung | Creates a new session for process management
[**createVorgangsverwaltungSessionUsingPOST**](MostCommonApi.md#createVorgangsverwaltungSessionUsingPOST) | **POST** /configure/sessionVorgangsverwaltung | Creates a new session for process management
[**deleteSessionUsingDELETE**](MostCommonApi.md#deleteSessionUsingDELETE) | **DELETE** /persistence/delete | Mark session as deleted
[**deleteSessionUsingGET**](MostCommonApi.md#deleteSessionUsingGET) | **GET** /persistence/delete | Mark session as deleted
[**deleteSessionUsingPOST**](MostCommonApi.md#deleteSessionUsingPOST) | **POST** /persistence/delete | Mark session as deleted
[**deleteUserAndSessionsUsingDELETE**](MostCommonApi.md#deleteUserAndSessionsUsingDELETE) | **DELETE** /configure/deleteuser | Remove a user
[**deleteUserAndSessionsUsingGET**](MostCommonApi.md#deleteUserAndSessionsUsingGET) | **GET** /configure/deleteuser | Remove a user
[**deleteUserAndSessionsUsingPOST**](MostCommonApi.md#deleteUserAndSessionsUsingPOST) | **POST** /configure/deleteuser | Remove a user
[**deletedocumentUsingDELETE**](MostCommonApi.md#deletedocumentUsingDELETE) | **DELETE** /configure/deletedocument | Remove a document from a session
[**deletedocumentUsingGET**](MostCommonApi.md#deletedocumentUsingGET) | **GET** /configure/deletedocument | Remove a document from a session
[**deletedocumentUsingPOST**](MostCommonApi.md#deletedocumentUsingPOST) | **POST** /configure/deletedocument | Remove a document from a session
[**downloadDocumentsMultiUsingGET1**](MostCommonApi.md#downloadDocumentsMultiUsingGET1) | **GET** /get/documents/downloadmulti | Download several session documents as zips in zip file
[**downloadDocumentsMultiUsingPOST1**](MostCommonApi.md#downloadDocumentsMultiUsingPOST1) | **POST** /get/documents/downloadmulti | Download several session documents as zips in zip file
[**downloadDocumentsProtectedUsingGET**](MostCommonApi.md#downloadDocumentsProtectedUsingGET) | **GET** /mail/documents/download | Retrieve all sessions documents as single zip file
[**downloadDocumentsProtectedUsingPOST**](MostCommonApi.md#downloadDocumentsProtectedUsingPOST) | **POST** /mail/documents/download | Retrieve all sessions documents as single zip file
[**downloadDocumentsUsingGET2**](MostCommonApi.md#downloadDocumentsUsingGET2) | **GET** /get/documents/download | Download all sessions documents as single zip file
[**downloadDocumentsUsingPOST2**](MostCommonApi.md#downloadDocumentsUsingPOST2) | **POST** /get/documents/download | Download all sessions documents as single zip file
[**getBlobDataUsingGET**](MostCommonApi.md#getBlobDataUsingGET) | **GET** /get/blobdata | Get session custom blob data
[**getBlobDataUsingPOST**](MostCommonApi.md#getBlobDataUsingPOST) | **POST** /get/blobdata | Get session custom blob data
[**getCheckStatusUsingGET**](MostCommonApi.md#getCheckStatusUsingGET) | **GET** /get/checkstatus | Get session status information
[**getCheckStatusUsingPOST**](MostCommonApi.md#getCheckStatusUsingPOST) | **POST** /get/checkstatus | Get session status information
[**getDocumentUsingGET**](MostCommonApi.md#getDocumentUsingGET) | **GET** /get/document | Download single document containing the current signatures and modifications
[**getDocumentUsingPOST**](MostCommonApi.md#getDocumentUsingPOST) | **POST** /get/document | Download single document containing the current signatures and modifications
[**getStatusLoadableUsingGET**](MostCommonApi.md#getStatusLoadableUsingGET) | **GET** /get/statuswithload | Get session status information and load session into memory if not already loaded
[**getStatusLoadableUsingPOST**](MostCommonApi.md#getStatusLoadableUsingPOST) | **POST** /get/statuswithload | Get session status information and load session into memory if not already loaded
[**getStatusUsingGET1**](MostCommonApi.md#getStatusUsingGET1) | **GET** /get/status | Get session status information
[**getStatusUsingPOST**](MostCommonApi.md#getStatusUsingPOST) | **POST** /get/status | Get session status information
[**getTransaktionsnummerUsingGET**](MostCommonApi.md#getTransaktionsnummerUsingGET) | **GET** /get/vorgangsnummer | Get session Transaction number (TAN)
[**getTransaktionsnummerUsingPOST**](MostCommonApi.md#getTransaktionsnummerUsingPOST) | **POST** /get/vorgangsnummer | Get session Transaction number (TAN)
[**getVersionUsingGET**](MostCommonApi.md#getVersionUsingGET) | **GET** /configure/get/version | Get inSign version
[**getVersionUsingPOST**](MostCommonApi.md#getVersionUsingPOST) | **POST** /configure/get/version | Get inSign version
[**loadSessionUsingPOST**](MostCommonApi.md#loadSessionUsingPOST) | **POST** /persistence/loadsession | Load session from database
[**purgeSessionUsingDELETE**](MostCommonApi.md#purgeSessionUsingDELETE) | **DELETE** /persistence/purge | Remove session by TAN oder sessionid. Removes session from memory.
[**purgeSessionUsingGET**](MostCommonApi.md#purgeSessionUsingGET) | **GET** /persistence/purge | Remove session by TAN oder sessionid. Removes session from memory.
[**purgeSessionUsingPOST**](MostCommonApi.md#purgeSessionUsingPOST) | **POST** /persistence/purge | Remove session by TAN oder sessionid. Removes session from memory.
[**recoverdeleteSessionsUsingDELETE**](MostCommonApi.md#recoverdeleteSessionsUsingDELETE) | **DELETE** /persistence/recoverdeleted | Retrieve all sessions documents as single zip file
[**recoverdeleteSessionsUsingGET**](MostCommonApi.md#recoverdeleteSessionsUsingGET) | **GET** /persistence/recoverdeleted | Retrieve all sessions documents as single zip file
[**recoverdeleteSessionsUsingPOST**](MostCommonApi.md#recoverdeleteSessionsUsingPOST) | **POST** /persistence/recoverdeleted | Retrieve all sessions documents as single zip file
[**uploaddocumentstreammultipartUsingPOST**](MostCommonApi.md#uploaddocumentstreammultipartUsingPOST) | **POST** /configure/uploaddocument | Upload a document into a previously configured session
[**uploaddocumentstreammultipartieUsingPOST**](MostCommonApi.md#uploaddocumentstreammultipartieUsingPOST) | **POST** /configure/uploaddocumentie | Upload a document into a previously configured session


# **configuredocumentsUsingPOST**
> \Insign\Model\ConfigureDocumentsResult configuredocumentsUsingPOST($input, $configid)

Creates a new session with the provided metadata for the documents

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ConfigureSession(); // \Insign\Model\ConfigureSession | input
$configid = "configid_example"; // string | configid

try {
    $result = $apiInstance->configuredocumentsUsingPOST($input, $configid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->configuredocumentsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ConfigureSession**](../Model/ConfigureSession.md)| input |
 **configid** | **string**| configid | [optional]

### Return type

[**\Insign\Model\ConfigureDocumentsResult**](../Model/ConfigureDocumentsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createVorgangsverwaltungSessionUsingGET**
> string createVorgangsverwaltungSessionUsingGET($sessionid, $from_editor)

Creates a new session for process management

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$from_editor = true; // bool | fromEditor

try {
    $result = $apiInstance->createVorgangsverwaltungSessionUsingGET($sessionid, $from_editor);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->createVorgangsverwaltungSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **from_editor** | **bool**| fromEditor | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createVorgangsverwaltungSessionUsingPOST**
> string createVorgangsverwaltungSessionUsingPOST($sessionid, $from_editor)

Creates a new session for process management

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$from_editor = true; // bool | fromEditor

try {
    $result = $apiInstance->createVorgangsverwaltungSessionUsingPOST($sessionid, $from_editor);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->createVorgangsverwaltungSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **from_editor** | **bool**| fromEditor | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSessionUsingDELETE**
> \Insign\Model\BasicResult deleteSessionUsingDELETE($sessionid, $sessionidtodelete)

Mark session as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->deleteSessionUsingDELETE($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->deleteSessionUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSessionUsingGET**
> \Insign\Model\BasicResult deleteSessionUsingGET($sessionid, $sessionidtodelete)

Mark session as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->deleteSessionUsingGET($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->deleteSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSessionUsingPOST**
> \Insign\Model\BasicResult deleteSessionUsingPOST($sessionid, $sessionidtodelete)

Mark session as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->deleteSessionUsingPOST($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->deleteSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingDELETE**
> deleteUserAndSessionsUsingDELETE($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingDELETE($user);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->deleteUserAndSessionsUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingGET**
> deleteUserAndSessionsUsingGET($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingGET($user);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->deleteUserAndSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingPOST**
> deleteUserAndSessionsUsingPOST($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingPOST($user);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->deleteUserAndSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletedocumentUsingDELETE**
> \Insign\Model\BasicResult deletedocumentUsingDELETE($docid, $sessionid)

Remove a document from a session

Document is removed if possible (i.e. document is marked as deleteable).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deletedocumentUsingDELETE($docid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->deletedocumentUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletedocumentUsingGET**
> \Insign\Model\BasicResult deletedocumentUsingGET($docid, $sessionid)

Remove a document from a session

Document is removed if possible (i.e. document is marked as deleteable).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deletedocumentUsingGET($docid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->deletedocumentUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletedocumentUsingPOST**
> \Insign\Model\BasicResult deletedocumentUsingPOST($docid, $sessionid)

Remove a document from a session

Document is removed if possible (i.e. document is marked as deleteable).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deletedocumentUsingPOST($docid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->deletedocumentUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsMultiUsingGET1**
> downloadDocumentsMultiUsingGET1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download several session documents as zips in zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = array("sessionid_example"); // string[] | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsMultiUsingGET1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->downloadDocumentsMultiUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | [**string[]**](../Model/string.md)| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsMultiUsingPOST1**
> downloadDocumentsMultiUsingPOST1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download several session documents as zips in zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = array("sessionid_example"); // string[] | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsMultiUsingPOST1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->downloadDocumentsMultiUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | [**string[]**](../Model/string.md)| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsProtectedUsingGET**
> downloadDocumentsProtectedUsingGET($sessionid, $bestaetigentoken, $mustberead)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$bestaetigentoken = "bestaetigentoken_example"; // string | bestaetigentoken
$mustberead = true; // bool | mustberead

try {
    $apiInstance->downloadDocumentsProtectedUsingGET($sessionid, $bestaetigentoken, $mustberead);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->downloadDocumentsProtectedUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **bestaetigentoken** | **string**| bestaetigentoken | [optional]
 **mustberead** | **bool**| mustberead | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsProtectedUsingPOST**
> downloadDocumentsProtectedUsingPOST($sessionid, $bestaetigentoken, $mustberead)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$bestaetigentoken = "bestaetigentoken_example"; // string | bestaetigentoken
$mustberead = true; // bool | mustberead

try {
    $apiInstance->downloadDocumentsProtectedUsingPOST($sessionid, $bestaetigentoken, $mustberead);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->downloadDocumentsProtectedUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **bestaetigentoken** | **string**| bestaetigentoken | [optional]
 **mustberead** | **bool**| mustberead | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsUsingGET2**
> downloadDocumentsUsingGET2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsUsingGET2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->downloadDocumentsUsingGET2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsUsingPOST2**
> downloadDocumentsUsingPOST2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsUsingPOST2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->downloadDocumentsUsingPOST2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBlobDataUsingGET**
> string getBlobDataUsingGET($sessionid)

Get session custom blob data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getBlobDataUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getBlobDataUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBlobDataUsingPOST**
> string getBlobDataUsingPOST($sessionid)

Get session custom blob data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getBlobDataUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getBlobDataUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCheckStatusUsingGET**
> \Insign\Model\CheckStatusResult getCheckStatusUsingGET($sessionid, $id_vorgangsverwaltung)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getCheckStatusUsingGET($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getCheckStatusUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\CheckStatusResult**](../Model/CheckStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCheckStatusUsingPOST**
> \Insign\Model\CheckStatusResult getCheckStatusUsingPOST($sessionid, $id_vorgangsverwaltung)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getCheckStatusUsingPOST($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getCheckStatusUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\CheckStatusResult**](../Model/CheckStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDocumentUsingGET**
> getDocumentUsingGET($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type)

Download single document containing the current signatures and modifications

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid
$externtoken = "externtoken_example"; // string | Token for external access if access by SSO User. Only used internally
$includebiodata = true; // bool | Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat).
$originalfile = false; // bool | Get the original file that was initially uploaded? Default: false.
$type = "type_example"; // string | Type of download. Internal use only. type='form' to keep form field in the document and skip signatures.

try {
    $apiInstance->getDocumentUsingGET($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getDocumentUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |
 **externtoken** | **string**| Token for external access if access by SSO User. Only used internally | [optional]
 **includebiodata** | **bool**| Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat). | [optional] [default to true]
 **originalfile** | **bool**| Get the original file that was initially uploaded? Default: false. | [optional] [default to false]
 **type** | **string**| Type of download. Internal use only. type&#x3D;&#39;form&#39; to keep form field in the document and skip signatures. | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDocumentUsingPOST**
> getDocumentUsingPOST($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type)

Download single document containing the current signatures and modifications

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid
$externtoken = "externtoken_example"; // string | Token for external access if access by SSO User. Only used internally
$includebiodata = true; // bool | Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat).
$originalfile = false; // bool | Get the original file that was initially uploaded? Default: false.
$type = "type_example"; // string | Type of download. Internal use only. type='form' to keep form field in the document and skip signatures.

try {
    $apiInstance->getDocumentUsingPOST($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getDocumentUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |
 **externtoken** | **string**| Token for external access if access by SSO User. Only used internally | [optional]
 **includebiodata** | **bool**| Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat). | [optional] [default to true]
 **originalfile** | **bool**| Get the original file that was initially uploaded? Default: false. | [optional] [default to false]
 **type** | **string**| Type of download. Internal use only. type&#x3D;&#39;form&#39; to keep form field in the document and skip signatures. | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusLoadableUsingGET**
> \Insign\Model\SessionStatusResult getStatusLoadableUsingGET($sessionid, $id_vorgangsverwaltung)

Get session status information and load session into memory if not already loaded

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getStatusLoadableUsingGET($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getStatusLoadableUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusLoadableUsingPOST**
> \Insign\Model\SessionStatusResult getStatusLoadableUsingPOST($sessionid, $id_vorgangsverwaltung)

Get session status information and load session into memory if not already loaded

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getStatusLoadableUsingPOST($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getStatusLoadableUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusUsingGET1**
> \Insign\Model\SessionStatusResult getStatusUsingGET1($sessionid)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getStatusUsingGET1($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getStatusUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusUsingPOST**
> \Insign\Model\SessionStatusResult getStatusUsingPOST($sessionid)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getStatusUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getStatusUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransaktionsnummerUsingGET**
> \Insign\Model\TransaktionsnummerResult getTransaktionsnummerUsingGET($sessionid)

Get session Transaction number (TAN)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getTransaktionsnummerUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getTransaktionsnummerUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\TransaktionsnummerResult**](../Model/TransaktionsnummerResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransaktionsnummerUsingPOST**
> \Insign\Model\TransaktionsnummerResult getTransaktionsnummerUsingPOST($sessionid)

Get session Transaction number (TAN)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getTransaktionsnummerUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getTransaktionsnummerUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\TransaktionsnummerResult**](../Model/TransaktionsnummerResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVersionUsingGET**
> string getVersionUsingGET()

Get inSign version

Retrieve application version.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getVersionUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getVersionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVersionUsingPOST**
> string getVersionUsingPOST()

Get inSign version

Retrieve application version.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getVersionUsingPOST();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->getVersionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **loadSessionUsingPOST**
> \Insign\Model\SessionResult loadSessionUsingPOST($input, $sessionid, $sessionidtoload)

Load session from database

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ConfigurationInformationForLoadingAProcess(); // \Insign\Model\ConfigurationInformationForLoadingAProcess | input
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtoload = "sessionidtoload_example"; // string | sessionidtoload

try {
    $result = $apiInstance->loadSessionUsingPOST($input, $sessionid, $sessionidtoload);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->loadSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ConfigurationInformationForLoadingAProcess**](../Model/ConfigurationInformationForLoadingAProcess.md)| input | [optional]
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtoload** | **string**| sessionidtoload | [optional]

### Return type

[**\Insign\Model\SessionResult**](../Model/SessionResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purgeSessionUsingDELETE**
> \Insign\Model\BasicResult purgeSessionUsingDELETE($sessionid, $sessionidtodelete)

Remove session by TAN oder sessionid. Removes session from memory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->purgeSessionUsingDELETE($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->purgeSessionUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purgeSessionUsingGET**
> \Insign\Model\BasicResult purgeSessionUsingGET($sessionid, $sessionidtodelete)

Remove session by TAN oder sessionid. Removes session from memory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->purgeSessionUsingGET($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->purgeSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purgeSessionUsingPOST**
> \Insign\Model\BasicResult purgeSessionUsingPOST($sessionid, $sessionidtodelete)

Remove session by TAN oder sessionid. Removes session from memory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->purgeSessionUsingPOST($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->purgeSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recoverdeleteSessionsUsingDELETE**
> \Insign\Model\BasicResult recoverdeleteSessionsUsingDELETE($sessionid)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->recoverdeleteSessionsUsingDELETE($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->recoverdeleteSessionsUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recoverdeleteSessionsUsingGET**
> \Insign\Model\BasicResult recoverdeleteSessionsUsingGET($sessionid)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->recoverdeleteSessionsUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->recoverdeleteSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recoverdeleteSessionsUsingPOST**
> \Insign\Model\BasicResult recoverdeleteSessionsUsingPOST($sessionid)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->recoverdeleteSessionsUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->recoverdeleteSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploaddocumentstreammultipartUsingPOST**
> string uploaddocumentstreammultipartUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user)

Upload a document into a previously configured session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filename = new \stdClass; // object | filename
$sessionid = new \stdClass; // object | sessionid
$docid = new \stdClass; // object | docid
$file = "/path/to/file.txt"; // \SplFileObject | file
$filesize = new \stdClass; // object | filesize
$pairing_token = new \stdClass; // object | pairingToken
$user = new \stdClass; // object | user

try {
    $result = $apiInstance->uploaddocumentstreammultipartUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->uploaddocumentstreammultipartUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filename** | [**object**](../Model/.md)| filename |
 **sessionid** | [**object**](../Model/.md)| sessionid |
 **docid** | [**object**](../Model/.md)| docid | [optional]
 **file** | **\SplFileObject**| file | [optional]
 **filesize** | [**object**](../Model/.md)| filesize | [optional]
 **pairing_token** | [**object**](../Model/.md)| pairingToken | [optional]
 **user** | [**object**](../Model/.md)| user | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploaddocumentstreammultipartieUsingPOST**
> string uploaddocumentstreammultipartieUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user)

Upload a document into a previously configured session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\MostCommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filename = new \stdClass; // object | filename
$sessionid = new \stdClass; // object | sessionid
$docid = new \stdClass; // object | docid
$file = "/path/to/file.txt"; // \SplFileObject | file
$filesize = new \stdClass; // object | filesize
$pairing_token = new \stdClass; // object | pairingToken
$user = new \stdClass; // object | user

try {
    $result = $apiInstance->uploaddocumentstreammultipartieUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MostCommonApi->uploaddocumentstreammultipartieUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filename** | [**object**](../Model/.md)| filename |
 **sessionid** | [**object**](../Model/.md)| sessionid |
 **docid** | [**object**](../Model/.md)| docid | [optional]
 **file** | **\SplFileObject**| file | [optional]
 **filesize** | [**object**](../Model/.md)| filesize | [optional]
 **pairing_token** | [**object**](../Model/.md)| pairingToken | [optional]
 **user** | [**object**](../Model/.md)| user | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

