# Insign\UserHandlingApi

All URIs are relative to *http://is2-cloud.test.flexperto.com/is2-horn-8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**adduserUsingPOST**](UserHandlingApi.md#adduserUsingPOST) | **POST** /configure/adduser | Create new user
[**changeSessionOwnerUsingPOST**](UserHandlingApi.md#changeSessionOwnerUsingPOST) | **POST** /configure/changeOwner | Change a sessions owner
[**createTokenRequestApiUserUsingPOST**](UserHandlingApi.md#createTokenRequestApiUserUsingPOST) | **POST** /configure/createSSOForApiuser | Create JWT Token for API-User so that he can access internal start module
[**createTokenRequestUsingPOST**](UserHandlingApi.md#createTokenRequestUsingPOST) | **POST** /configure/createSSO | Create OTP token for internal start module
[**deleteAllSessionsUsingDELETE**](UserHandlingApi.md#deleteAllSessionsUsingDELETE) | **DELETE** /persistence/deleteall | Mark all sessions of the sessions owner as deleted
[**deleteAllSessionsUsingGET**](UserHandlingApi.md#deleteAllSessionsUsingGET) | **GET** /persistence/deleteall | Mark all sessions of the sessions owner as deleted
[**deleteAllSessionsUsingPOST**](UserHandlingApi.md#deleteAllSessionsUsingPOST) | **POST** /persistence/deleteall | Mark all sessions of the sessions owner as deleted
[**deleteMarkedSessionsUsingDELETE**](UserHandlingApi.md#deleteMarkedSessionsUsingDELETE) | **DELETE** /persistence/deletemarked | Delete all session marked as deleted of the sessions owner permanently
[**deleteMarkedSessionsUsingGET**](UserHandlingApi.md#deleteMarkedSessionsUsingGET) | **GET** /persistence/deletemarked | Delete all session marked as deleted of the sessions owner permanently
[**deleteMarkedSessionsUsingPOST**](UserHandlingApi.md#deleteMarkedSessionsUsingPOST) | **POST** /persistence/deletemarked | Delete all session marked as deleted of the sessions owner permanently
[**deleteUserAndSessionsUsingDELETE**](UserHandlingApi.md#deleteUserAndSessionsUsingDELETE) | **DELETE** /configure/deleteuser | Remove a user
[**deleteUserAndSessionsUsingGET**](UserHandlingApi.md#deleteUserAndSessionsUsingGET) | **GET** /configure/deleteuser | Remove a user
[**deleteUserAndSessionsUsingPOST**](UserHandlingApi.md#deleteUserAndSessionsUsingPOST) | **POST** /configure/deleteuser | Remove a user
[**deletedocumentUsingDELETE**](UserHandlingApi.md#deletedocumentUsingDELETE) | **DELETE** /configure/deletedocument | Remove a document from a session
[**deletedocumentUsingGET**](UserHandlingApi.md#deletedocumentUsingGET) | **GET** /configure/deletedocument | Remove a document from a session
[**deletedocumentUsingPOST**](UserHandlingApi.md#deletedocumentUsingPOST) | **POST** /configure/deletedocument | Remove a document from a session
[**newTokenUsingGET**](UserHandlingApi.md#newTokenUsingGET) | **GET** /extern/newtoken | Reaktivate external user token and create new password for user if token was renewed
[**newTokenUsingPOST**](UserHandlingApi.md#newTokenUsingPOST) | **POST** /extern/newtoken | Reaktivate external user token and create new password for user if token was renewed
[**newTokenUsingPUT**](UserHandlingApi.md#newTokenUsingPUT) | **PUT** /extern/newtoken | Reaktivate external user token and create new password for user if token was renewed
[**updateExternUserUsingPOST**](UserHandlingApi.md#updateExternUserUsingPOST) | **POST** /extern/updateUser | Update external user information
[**updateExternUserUsingPUT**](UserHandlingApi.md#updateExternUserUsingPUT) | **PUT** /extern/updateUser | Update external user information
[**updateTokenUsingPOST**](UserHandlingApi.md#updateTokenUsingPOST) | **POST** /extern/updatetoken | Reaktivate external user token and create new password for user
[**updateTokenUsingPUT**](UserHandlingApi.md#updateTokenUsingPUT) | **PUT** /extern/updatetoken | Reaktivate external user token and create new password for user


# **adduserUsingPOST**
> \Insign\Model\UserID adduserUsingPOST($input)

Create new user

Usually no need to call this as internal users are automatically created on session creation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\UserInfo(); // \Insign\Model\UserInfo | input

try {
    $result = $apiInstance->adduserUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->adduserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\UserInfo**](../Model/UserInfo.md)| input |

### Return type

[**\Insign\Model\UserID**](../Model/UserID.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **changeSessionOwnerUsingPOST**
> \Insign\Model\BasicResult changeSessionOwnerUsingPOST($ch_owner)

Change a sessions owner

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ch_owner = new \Insign\Model\ChangeOwner(); // \Insign\Model\ChangeOwner | chOwner

try {
    $result = $apiInstance->changeSessionOwnerUsingPOST($ch_owner);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->changeSessionOwnerUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ch_owner** | [**\Insign\Model\ChangeOwner**](../Model/ChangeOwner.md)| chOwner |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createTokenRequestApiUserUsingPOST**
> string createTokenRequestApiUserUsingPOST($input)

Create JWT Token for API-User so that he can access internal start module

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\UserInfo(); // \Insign\Model\UserInfo | input

try {
    $result = $apiInstance->createTokenRequestApiUserUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->createTokenRequestApiUserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\UserInfo**](../Model/UserInfo.md)| input |

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createTokenRequestUsingPOST**
> string createTokenRequestUsingPOST($input)

Create OTP token for internal start module

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\UserInfo(); // \Insign\Model\UserInfo | input

try {
    $result = $apiInstance->createTokenRequestUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->createTokenRequestUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\UserInfo**](../Model/UserInfo.md)| input |

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAllSessionsUsingDELETE**
> \Insign\Model\BasicResult deleteAllSessionsUsingDELETE($sessionid)

Mark all sessions of the sessions owner as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteAllSessionsUsingDELETE($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deleteAllSessionsUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAllSessionsUsingGET**
> \Insign\Model\BasicResult deleteAllSessionsUsingGET($sessionid)

Mark all sessions of the sessions owner as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteAllSessionsUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deleteAllSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAllSessionsUsingPOST**
> \Insign\Model\BasicResult deleteAllSessionsUsingPOST($sessionid)

Mark all sessions of the sessions owner as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteAllSessionsUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deleteAllSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteMarkedSessionsUsingDELETE**
> \Insign\Model\BasicResult deleteMarkedSessionsUsingDELETE($sessionid)

Delete all session marked as deleted of the sessions owner permanently

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteMarkedSessionsUsingDELETE($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deleteMarkedSessionsUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteMarkedSessionsUsingGET**
> \Insign\Model\BasicResult deleteMarkedSessionsUsingGET($sessionid)

Delete all session marked as deleted of the sessions owner permanently

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteMarkedSessionsUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deleteMarkedSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteMarkedSessionsUsingPOST**
> \Insign\Model\BasicResult deleteMarkedSessionsUsingPOST($sessionid)

Delete all session marked as deleted of the sessions owner permanently

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deleteMarkedSessionsUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deleteMarkedSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingDELETE**
> deleteUserAndSessionsUsingDELETE($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingDELETE($user);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deleteUserAndSessionsUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingGET**
> deleteUserAndSessionsUsingGET($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingGET($user);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deleteUserAndSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingPOST**
> deleteUserAndSessionsUsingPOST($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingPOST($user);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deleteUserAndSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletedocumentUsingDELETE**
> \Insign\Model\BasicResult deletedocumentUsingDELETE($docid, $sessionid)

Remove a document from a session

Document is removed if possible (i.e. document is marked as deleteable).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deletedocumentUsingDELETE($docid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deletedocumentUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletedocumentUsingGET**
> \Insign\Model\BasicResult deletedocumentUsingGET($docid, $sessionid)

Remove a document from a session

Document is removed if possible (i.e. document is marked as deleteable).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deletedocumentUsingGET($docid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deletedocumentUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletedocumentUsingPOST**
> \Insign\Model\BasicResult deletedocumentUsingPOST($docid, $sessionid)

Remove a document from a session

Document is removed if possible (i.e. document is marked as deleteable).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deletedocumentUsingPOST($docid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->deletedocumentUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **newTokenUsingGET**
> \Insign\Model\ExternUserResult newTokenUsingGET($sessionid, $token)

Reaktivate external user token and create new password for user if token was renewed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->newTokenUsingGET($sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->newTokenUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **newTokenUsingPOST**
> \Insign\Model\ExternUserResult newTokenUsingPOST($sessionid, $token)

Reaktivate external user token and create new password for user if token was renewed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->newTokenUsingPOST($sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->newTokenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **newTokenUsingPUT**
> \Insign\Model\ExternUserResult newTokenUsingPUT($sessionid, $token)

Reaktivate external user token and create new password for user if token was renewed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->newTokenUsingPUT($sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->newTokenUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExternUserUsingPOST**
> \Insign\Model\ExternUserResult updateExternUserUsingPOST($input, $sessionid, $token)

Update external user information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\StartExtern(); // \Insign\Model\StartExtern | input
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->updateExternUserUsingPOST($input, $sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->updateExternUserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\StartExtern**](../Model/StartExtern.md)| input |
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExternUserUsingPUT**
> \Insign\Model\ExternUserResult updateExternUserUsingPUT($input, $sessionid, $token)

Update external user information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\StartExtern(); // \Insign\Model\StartExtern | input
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->updateExternUserUsingPUT($input, $sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->updateExternUserUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\StartExtern**](../Model/StartExtern.md)| input |
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTokenUsingPOST**
> \Insign\Model\ExternUserResult updateTokenUsingPOST($input, $sessionid)

Reaktivate external user token and create new password for user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ExternUserResult(); // \Insign\Model\ExternUserResult | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->updateTokenUsingPOST($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->updateTokenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTokenUsingPUT**
> \Insign\Model\ExternUserResult updateTokenUsingPUT($input, $sessionid)

Reaktivate external user token and create new password for user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\UserHandlingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ExternUserResult(); // \Insign\Model\ExternUserResult | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->updateTokenUsingPUT($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserHandlingApi->updateTokenUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

