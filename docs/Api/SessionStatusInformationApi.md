# Insign\SessionStatusInformationApi

All URIs are relative to *http://is2-cloud.test.flexperto.com/is2-horn-8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBestaetigungsdatumUsingGET**](SessionStatusInformationApi.md#getBestaetigungsdatumUsingGET) | **GET** /aushaendigung/getBestaetigungsdatum | Retrieve document receipt confirmation timestamp
[**getBestaetigungsdatumUsingPOST**](SessionStatusInformationApi.md#getBestaetigungsdatumUsingPOST) | **POST** /aushaendigung/getBestaetigungsdatum | Retrieve document receipt confirmation timestamp
[**getCheckStatusUsingGET**](SessionStatusInformationApi.md#getCheckStatusUsingGET) | **GET** /get/checkstatus | Get session status information
[**getCheckStatusUsingPOST**](SessionStatusInformationApi.md#getCheckStatusUsingPOST) | **POST** /get/checkstatus | Get session status information
[**getExternInfosUsingGET**](SessionStatusInformationApi.md#getExternInfosUsingGET) | **GET** /get/externInfos | Additional status information about session that is in external state
[**getExternInfosUsingPOST**](SessionStatusInformationApi.md#getExternInfosUsingPOST) | **POST** /get/externInfos | Additional status information about session that is in external state
[**getSessionsUsingGET2**](SessionStatusInformationApi.md#getSessionsUsingGET2) | **GET** /get/sessions | List of session
[**getSessionsUsingGET3**](SessionStatusInformationApi.md#getSessionsUsingGET3) | **GET** /get/usersessions | List of session that belong to a single user. You can also pass a user list separated by &#39;separator&#39; for multiple users
[**getSessionsUsingPOST1**](SessionStatusInformationApi.md#getSessionsUsingPOST1) | **POST** /get/querysessions | Retrieve information about a bunch of sessions
[**getSessionsUsingPOST2**](SessionStatusInformationApi.md#getSessionsUsingPOST2) | **POST** /get/sessions | List of session
[**getSessionsUsingPOST3**](SessionStatusInformationApi.md#getSessionsUsingPOST3) | **POST** /get/usersessions | List of session that belong to a single user. You can also pass a user list separated by &#39;separator&#39; for multiple users
[**getStatusLoadableUsingGET**](SessionStatusInformationApi.md#getStatusLoadableUsingGET) | **GET** /get/statuswithload | Get session status information and load session into memory if not already loaded
[**getStatusLoadableUsingPOST**](SessionStatusInformationApi.md#getStatusLoadableUsingPOST) | **POST** /get/statuswithload | Get session status information and load session into memory if not already loaded
[**getStatusUsingGET**](SessionStatusInformationApi.md#getStatusUsingGET) | **GET** /extern/status | Retrieve status about external user
[**getStatusUsingGET1**](SessionStatusInformationApi.md#getStatusUsingGET1) | **GET** /get/status | Get session status information
[**getStatusUsingPOST**](SessionStatusInformationApi.md#getStatusUsingPOST) | **POST** /get/status | Get session status information
[**getUserForSessionsUsingGET**](SessionStatusInformationApi.md#getUserForSessionsUsingGET) | **GET** /get/sessionowner | Get owner of a session
[**getUserForSessionsUsingPOST**](SessionStatusInformationApi.md#getUserForSessionsUsingPOST) | **POST** /get/sessionowner | Get owner of a session


# **getBestaetigungsdatumUsingGET**
> \Insign\Model\GenericResult getBestaetigungsdatumUsingGET($sessionid)

Retrieve document receipt confirmation timestamp

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getBestaetigungsdatumUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getBestaetigungsdatumUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\GenericResult**](../Model/GenericResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBestaetigungsdatumUsingPOST**
> \Insign\Model\GenericResult getBestaetigungsdatumUsingPOST($sessionid)

Retrieve document receipt confirmation timestamp

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getBestaetigungsdatumUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getBestaetigungsdatumUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\GenericResult**](../Model/GenericResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCheckStatusUsingGET**
> \Insign\Model\CheckStatusResult getCheckStatusUsingGET($sessionid, $id_vorgangsverwaltung)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getCheckStatusUsingGET($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getCheckStatusUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\CheckStatusResult**](../Model/CheckStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCheckStatusUsingPOST**
> \Insign\Model\CheckStatusResult getCheckStatusUsingPOST($sessionid, $id_vorgangsverwaltung)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getCheckStatusUsingPOST($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getCheckStatusUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\CheckStatusResult**](../Model/CheckStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExternInfosUsingGET**
> \Insign\Model\ExternUserInfo[] getExternInfosUsingGET($sessionid, $id_vorgangsverwaltung)

Additional status information about session that is in external state

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getExternInfosUsingGET($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getExternInfosUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternUserInfo[]**](../Model/ExternUserInfo.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExternInfosUsingPOST**
> \Insign\Model\ExternUserInfo[] getExternInfosUsingPOST($sessionid, $id_vorgangsverwaltung)

Additional status information about session that is in external state

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getExternInfosUsingPOST($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getExternInfosUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternUserInfo[]**](../Model/ExternUserInfo.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingGET2**
> \Insign\Model\SessionsResult getSessionsUsingGET2($sessionid)

List of session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getSessionsUsingGET2($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getSessionsUsingGET2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingGET3**
> \Insign\Model\SessionsResult getSessionsUsingGET3($separator, $user)

List of session that belong to a single user. You can also pass a user list separated by 'separator' for multiple users

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$separator = "separator_example"; // string | Separator for users (only required using list)
$user = "user_example"; // string | User ID(s)

try {
    $result = $apiInstance->getSessionsUsingGET3($separator, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getSessionsUsingGET3: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **separator** | **string**| Separator for users (only required using list) | [optional]
 **user** | **string**| User ID(s) | [optional]

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingPOST1**
> \Insign\Model\SessionsResult getSessionsUsingPOST1($iso3_country, $iso3_language, $country, $display_country, $display_language, $display_name, $display_script, $display_variant, $language, $script, $sessionids, $unicode_locale_attributes, $unicode_locale_keys, $variant)

Retrieve information about a bunch of sessions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$iso3_country = "iso3_country_example"; // string | 
$iso3_language = "iso3_language_example"; // string | 
$country = "country_example"; // string | 
$display_country = "display_country_example"; // string | 
$display_language = "display_language_example"; // string | 
$display_name = "display_name_example"; // string | 
$display_script = "display_script_example"; // string | 
$display_variant = "display_variant_example"; // string | 
$language = "language_example"; // string | 
$script = "script_example"; // string | 
$sessionids = new \Insign\Model\ListOfSessionIDs(); // \Insign\Model\ListOfSessionIDs | list of session to retrieve information about
$unicode_locale_attributes = array("unicode_locale_attributes_example"); // string[] | 
$unicode_locale_keys = array("unicode_locale_keys_example"); // string[] | 
$variant = "variant_example"; // string | 

try {
    $result = $apiInstance->getSessionsUsingPOST1($iso3_country, $iso3_language, $country, $display_country, $display_language, $display_name, $display_script, $display_variant, $language, $script, $sessionids, $unicode_locale_attributes, $unicode_locale_keys, $variant);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getSessionsUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iso3_country** | **string**|  | [optional]
 **iso3_language** | **string**|  | [optional]
 **country** | **string**|  | [optional]
 **display_country** | **string**|  | [optional]
 **display_language** | **string**|  | [optional]
 **display_name** | **string**|  | [optional]
 **display_script** | **string**|  | [optional]
 **display_variant** | **string**|  | [optional]
 **language** | **string**|  | [optional]
 **script** | **string**|  | [optional]
 **sessionids** | [**\Insign\Model\ListOfSessionIDs**](../Model/ListOfSessionIDs.md)| list of session to retrieve information about | [optional]
 **unicode_locale_attributes** | [**string[]**](../Model/string.md)|  | [optional]
 **unicode_locale_keys** | [**string[]**](../Model/string.md)|  | [optional]
 **variant** | **string**|  | [optional]

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingPOST2**
> \Insign\Model\SessionsResult getSessionsUsingPOST2($sessionid)

List of session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getSessionsUsingPOST2($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getSessionsUsingPOST2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingPOST3**
> \Insign\Model\SessionsResult getSessionsUsingPOST3($separator, $user)

List of session that belong to a single user. You can also pass a user list separated by 'separator' for multiple users

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$separator = "separator_example"; // string | Separator for users (only required using list)
$user = "user_example"; // string | User ID(s)

try {
    $result = $apiInstance->getSessionsUsingPOST3($separator, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getSessionsUsingPOST3: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **separator** | **string**| Separator for users (only required using list) | [optional]
 **user** | **string**| User ID(s) | [optional]

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusLoadableUsingGET**
> \Insign\Model\SessionStatusResult getStatusLoadableUsingGET($sessionid, $id_vorgangsverwaltung)

Get session status information and load session into memory if not already loaded

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getStatusLoadableUsingGET($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getStatusLoadableUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusLoadableUsingPOST**
> \Insign\Model\SessionStatusResult getStatusLoadableUsingPOST($sessionid, $id_vorgangsverwaltung)

Get session status information and load session into memory if not already loaded

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getStatusLoadableUsingPOST($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getStatusLoadableUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusUsingGET**
> \Insign\Model\SessionStatusResult getStatusUsingGET($sessionid, $token)

Retrieve status about external user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->getStatusUsingGET($sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getStatusUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusUsingGET1**
> \Insign\Model\SessionStatusResult getStatusUsingGET1($sessionid)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getStatusUsingGET1($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getStatusUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusUsingPOST**
> \Insign\Model\SessionStatusResult getStatusUsingPOST($sessionid)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getStatusUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getStatusUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUserForSessionsUsingGET**
> \Insign\Model\UserID getUserForSessionsUsingGET($sessionid)

Get owner of a session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getUserForSessionsUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getUserForSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\UserID**](../Model/UserID.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUserForSessionsUsingPOST**
> \Insign\Model\UserID getUserForSessionsUsingPOST($sessionid)

Get owner of a session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\SessionStatusInformationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getUserForSessionsUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionStatusInformationApi->getUserForSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\UserID**](../Model/UserID.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

