# Insign\AllPublicApi

All URIs are relative to *http://is2-cloud.test.flexperto.com/is2-horn-8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addAdditionalDocumentUsingPOST**](AllPublicApi.md#addAdditionalDocumentUsingPOST) | **POST** /configure/addAdditionalDocument | Add one more documents metadata to a session after session creation.
[**adduserUsingPOST**](AllPublicApi.md#adduserUsingPOST) | **POST** /configure/adduser | Create new user
[**changeSessionOwnerUsingPOST**](AllPublicApi.md#changeSessionOwnerUsingPOST) | **POST** /configure/changeOwner | Change a sessions owner
[**cleanUpDBUsingDELETE1**](AllPublicApi.md#cleanUpDBUsingDELETE1) | **DELETE** /configure/cleanupdb | Cleanup database
[**cleanUpDBUsingGET1**](AllPublicApi.md#cleanUpDBUsingGET1) | **GET** /configure/cleanupdb | Cleanup database
[**cleanUpDBUsingPOST1**](AllPublicApi.md#cleanUpDBUsingPOST1) | **POST** /configure/cleanupdb | Cleanup database
[**configuredocumentsUsingPOST**](AllPublicApi.md#configuredocumentsUsingPOST) | **POST** /configure/session | Creates a new session with the provided metadata for the documents
[**createVorgangsverwaltungSessionUsingGET**](AllPublicApi.md#createVorgangsverwaltungSessionUsingGET) | **GET** /configure/sessionVorgangsverwaltung | Creates a new session for process management
[**createVorgangsverwaltungSessionUsingPOST**](AllPublicApi.md#createVorgangsverwaltungSessionUsingPOST) | **POST** /configure/sessionVorgangsverwaltung | Creates a new session for process management
[**delegateUsingPOST**](AllPublicApi.md#delegateUsingPOST) | **POST** /extern/delegate | Redirect extern process to another user
[**delegateUsingPUT**](AllPublicApi.md#delegateUsingPUT) | **PUT** /extern/delegate | Redirect extern process to another user
[**deleteDocumentUsingDELETE**](AllPublicApi.md#deleteDocumentUsingDELETE) | **DELETE** /persistence/deletedocument | Remove document from session. Remove session if no documents left in the session after the operation.
[**deleteDocumentUsingGET**](AllPublicApi.md#deleteDocumentUsingGET) | **GET** /persistence/deletedocument | Remove document from session. Remove session if no documents left in the session after the operation.
[**deleteDocumentUsingPOST**](AllPublicApi.md#deleteDocumentUsingPOST) | **POST** /persistence/deletedocument | Remove document from session. Remove session if no documents left in the session after the operation.
[**deleteSessionUsingDELETE**](AllPublicApi.md#deleteSessionUsingDELETE) | **DELETE** /persistence/delete | Mark session as deleted
[**deleteSessionUsingGET**](AllPublicApi.md#deleteSessionUsingGET) | **GET** /persistence/delete | Mark session as deleted
[**deleteSessionUsingPOST**](AllPublicApi.md#deleteSessionUsingPOST) | **POST** /persistence/delete | Mark session as deleted
[**deleteUserAndSessionsUsingDELETE**](AllPublicApi.md#deleteUserAndSessionsUsingDELETE) | **DELETE** /configure/deleteuser | Remove a user
[**deleteUserAndSessionsUsingGET**](AllPublicApi.md#deleteUserAndSessionsUsingGET) | **GET** /configure/deleteuser | Remove a user
[**deleteUserAndSessionsUsingPOST**](AllPublicApi.md#deleteUserAndSessionsUsingPOST) | **POST** /configure/deleteuser | Remove a user
[**deletedocumentUsingDELETE**](AllPublicApi.md#deletedocumentUsingDELETE) | **DELETE** /configure/deletedocument | Remove a document from a session
[**deletedocumentUsingGET**](AllPublicApi.md#deletedocumentUsingGET) | **GET** /configure/deletedocument | Remove a document from a session
[**deletedocumentUsingPOST**](AllPublicApi.md#deletedocumentUsingPOST) | **POST** /configure/deletedocument | Remove a document from a session
[**downloadDocumentsMultiUsingGET1**](AllPublicApi.md#downloadDocumentsMultiUsingGET1) | **GET** /get/documents/downloadmulti | Download several session documents as zips in zip file
[**downloadDocumentsMultiUsingPOST1**](AllPublicApi.md#downloadDocumentsMultiUsingPOST1) | **POST** /get/documents/downloadmulti | Download several session documents as zips in zip file
[**downloadDocumentsProtectedUsingGET**](AllPublicApi.md#downloadDocumentsProtectedUsingGET) | **GET** /mail/documents/download | Retrieve all sessions documents as single zip file
[**downloadDocumentsProtectedUsingPOST**](AllPublicApi.md#downloadDocumentsProtectedUsingPOST) | **POST** /mail/documents/download | Retrieve all sessions documents as single zip file
[**downloadDocumentsUsingGET2**](AllPublicApi.md#downloadDocumentsUsingGET2) | **GET** /get/documents/download | Download all sessions documents as single zip file
[**downloadDocumentsUsingPOST2**](AllPublicApi.md#downloadDocumentsUsingPOST2) | **POST** /get/documents/download | Download all sessions documents as single zip file
[**exportSessionUsingGET**](AllPublicApi.md#exportSessionUsingGET) | **GET** /exportSession | Export a session as binary data.
[**exportSessionUsingPOST**](AllPublicApi.md#exportSessionUsingPOST) | **POST** /exportSession | Export a session as binary data.
[**getBestaetigungsdatumUsingGET**](AllPublicApi.md#getBestaetigungsdatumUsingGET) | **GET** /aushaendigung/getBestaetigungsdatum | Retrieve document receipt confirmation timestamp
[**getBestaetigungsdatumUsingPOST**](AllPublicApi.md#getBestaetigungsdatumUsingPOST) | **POST** /aushaendigung/getBestaetigungsdatum | Retrieve document receipt confirmation timestamp
[**getBlobDataUsingGET**](AllPublicApi.md#getBlobDataUsingGET) | **GET** /get/blobdata | Get session custom blob data
[**getBlobDataUsingPOST**](AllPublicApi.md#getBlobDataUsingPOST) | **POST** /get/blobdata | Get session custom blob data
[**getCheckStatusUsingGET**](AllPublicApi.md#getCheckStatusUsingGET) | **GET** /get/checkstatus | Get session status information
[**getCheckStatusUsingPOST**](AllPublicApi.md#getCheckStatusUsingPOST) | **POST** /get/checkstatus | Get session status information
[**getDocumentPageImageB64UsingGET**](AllPublicApi.md#getDocumentPageImageB64UsingGET) | **GET** /image/getdocumentpageimageb64 | Render a documents page as PNG Bitmap. Returns Base64 data.
[**getDocumentPageImageUsingGET**](AllPublicApi.md#getDocumentPageImageUsingGET) | **GET** /image/getdocumentpageimage | Render a documents page as PNG Bitmap
[**getDocumentThumbnailB64UsingGET**](AllPublicApi.md#getDocumentThumbnailB64UsingGET) | **GET** /image/getdocumentthumbnail64 | Render a documents page as PNG Thumbnail-Bitmap. Returns Base64 data.
[**getDocumentThumbnailUsingGET**](AllPublicApi.md#getDocumentThumbnailUsingGET) | **GET** /image/getdocumentthumbnail | Render a documents page as PNG Thumbnail-Bitmap
[**getDocumentUsingGET**](AllPublicApi.md#getDocumentUsingGET) | **GET** /get/document | Download single document containing the current signatures and modifications
[**getDocumentUsingPOST**](AllPublicApi.md#getDocumentUsingPOST) | **POST** /get/document | Download single document containing the current signatures and modifications
[**getEnvelopedDocumentsUsingGET1**](AllPublicApi.md#getEnvelopedDocumentsUsingGET1) | **GET** /get/documents/enveloped | Attach sessions current documents to the provided container pdf-file
[**getEnvelopedDocumentsUsingPOST1**](AllPublicApi.md#getEnvelopedDocumentsUsingPOST1) | **POST** /get/documents/enveloped | Attach sessions current documents to the provided container pdf-file
[**getEnvelopedDocumentsUsingPUT1**](AllPublicApi.md#getEnvelopedDocumentsUsingPUT1) | **PUT** /get/documents/enveloped | Attach sessions current documents to the provided container pdf-file
[**getGWGInfosUsingGET**](AllPublicApi.md#getGWGInfosUsingGET) | **GET** /get/documents/gwg | Retrieve information about ID-Card photos from a session
[**getGWGInfosUsingPOST**](AllPublicApi.md#getGWGInfosUsingPOST) | **POST** /get/documents/gwg | Retrieve information about ID-Card photos from a session
[**getSessionInfoUsingGET**](AllPublicApi.md#getSessionInfoUsingGET) | **GET** /getSessionInfoForExport | Get session metadata for export
[**getSessionInfoUsingPOST**](AllPublicApi.md#getSessionInfoUsingPOST) | **POST** /getSessionInfoForExport | Get session metadata for export
[**getSessionsUsingGET3**](AllPublicApi.md#getSessionsUsingGET3) | **GET** /get/usersessions | List of session that belong to a single user. You can also pass a user list separated by &#39;separator&#39; for multiple users
[**getSessionsUsingPOST1**](AllPublicApi.md#getSessionsUsingPOST1) | **POST** /get/querysessions | Retrieve information about a bunch of sessions
[**getSessionsUsingPOST3**](AllPublicApi.md#getSessionsUsingPOST3) | **POST** /get/usersessions | List of session that belong to a single user. You can also pass a user list separated by &#39;separator&#39; for multiple users
[**getStatusLoadableUsingGET**](AllPublicApi.md#getStatusLoadableUsingGET) | **GET** /get/statuswithload | Get session status information and load session into memory if not already loaded
[**getStatusLoadableUsingPOST**](AllPublicApi.md#getStatusLoadableUsingPOST) | **POST** /get/statuswithload | Get session status information and load session into memory if not already loaded
[**getStatusUsingGET**](AllPublicApi.md#getStatusUsingGET) | **GET** /extern/status | Retrieve status about external user
[**getStatusUsingGET1**](AllPublicApi.md#getStatusUsingGET1) | **GET** /get/status | Get session status information
[**getStatusUsingPOST**](AllPublicApi.md#getStatusUsingPOST) | **POST** /get/status | Get session status information
[**getTransaktionsnummerUsingGET**](AllPublicApi.md#getTransaktionsnummerUsingGET) | **GET** /get/vorgangsnummer | Get session Transaction number (TAN)
[**getTransaktionsnummerUsingPOST**](AllPublicApi.md#getTransaktionsnummerUsingPOST) | **POST** /get/vorgangsnummer | Get session Transaction number (TAN)
[**getUserForSessionsUsingGET**](AllPublicApi.md#getUserForSessionsUsingGET) | **GET** /get/sessionowner | Get owner of a session
[**getUserForSessionsUsingPOST**](AllPublicApi.md#getUserForSessionsUsingPOST) | **POST** /get/sessionowner | Get owner of a session
[**getVersionUsingGET**](AllPublicApi.md#getVersionUsingGET) | **GET** /configure/get/version | Get inSign version
[**getVersionUsingPOST**](AllPublicApi.md#getVersionUsingPOST) | **POST** /configure/get/version | Get inSign version
[**importSessionUsingGET**](AllPublicApi.md#importSessionUsingGET) | **GET** /importSession | Import binary session data
[**importSessionUsingPOST**](AllPublicApi.md#importSessionUsingPOST) | **POST** /importSession | Import binary session data
[**loadSessionUsingPOST**](AllPublicApi.md#loadSessionUsingPOST) | **POST** /persistence/loadsession | Load session from database
[**newTokenUsingGET**](AllPublicApi.md#newTokenUsingGET) | **GET** /extern/newtoken | Reaktivate external user token and create new password for user if token was renewed
[**newTokenUsingPOST**](AllPublicApi.md#newTokenUsingPOST) | **POST** /extern/newtoken | Reaktivate external user token and create new password for user if token was renewed
[**newTokenUsingPUT**](AllPublicApi.md#newTokenUsingPUT) | **PUT** /extern/newtoken | Reaktivate external user token and create new password for user if token was renewed
[**purgeSessionUsingDELETE**](AllPublicApi.md#purgeSessionUsingDELETE) | **DELETE** /persistence/purge | Remove session by TAN oder sessionid. Removes session from memory.
[**purgeSessionUsingGET**](AllPublicApi.md#purgeSessionUsingGET) | **GET** /persistence/purge | Remove session by TAN oder sessionid. Removes session from memory.
[**purgeSessionUsingPOST**](AllPublicApi.md#purgeSessionUsingPOST) | **POST** /persistence/purge | Remove session by TAN oder sessionid. Removes session from memory.
[**reconfigureSessionUsingPOST**](AllPublicApi.md#reconfigureSessionUsingPOST) | **POST** /reconfigureSession | Reconfigure a session
[**recoverdeleteSessionsUsingDELETE**](AllPublicApi.md#recoverdeleteSessionsUsingDELETE) | **DELETE** /persistence/recoverdeleted | Retrieve all sessions documents as single zip file
[**recoverdeleteSessionsUsingGET**](AllPublicApi.md#recoverdeleteSessionsUsingGET) | **GET** /persistence/recoverdeleted | Retrieve all sessions documents as single zip file
[**recoverdeleteSessionsUsingPOST**](AllPublicApi.md#recoverdeleteSessionsUsingPOST) | **POST** /persistence/recoverdeleted | Retrieve all sessions documents as single zip file
[**resetSignatureUsingPOST**](AllPublicApi.md#resetSignatureUsingPOST) | **POST** /extern/resetlastSignature | Redirect extern process to another user
[**resetSignatureUsingPUT**](AllPublicApi.md#resetSignatureUsingPUT) | **PUT** /extern/resetlastSignature | Redirect extern process to another user
[**resetSignaturesUsingGET**](AllPublicApi.md#resetSignaturesUsingGET) | **GET** /configure/resetsignatures | Reset sessions signatures
[**resetSignaturesUsingPOST**](AllPublicApi.md#resetSignaturesUsingPOST) | **POST** /configure/resetsignatures | Reset sessions signatures
[**restartsessionUsingGET**](AllPublicApi.md#restartsessionUsingGET) | **GET** /configure/restartsession | Reset sessions information
[**restartsessionUsingPOST**](AllPublicApi.md#restartsessionUsingPOST) | **POST** /configure/restartsession | Reset sessions information
[**sessionloeschenUsingGET**](AllPublicApi.md#sessionloeschenUsingGET) | **GET** /configure/ablehnen | Abort and delete session
[**sessionloeschenUsingPOST**](AllPublicApi.md#sessionloeschenUsingPOST) | **POST** /configure/ablehnen | Abort and delete session
[**setExternalUsingPOST**](AllPublicApi.md#setExternalUsingPOST) | **POST** /extern/begin | Start extern mode. Uses /beginmulti instead.
[**setExternalUsingPOST1**](AllPublicApi.md#setExternalUsingPOST1) | **POST** /extern/beginmulti | Start extern mode and creates users for this mode.
[**setExternalUsingPUT**](AllPublicApi.md#setExternalUsingPUT) | **PUT** /extern/begin | Start extern mode. Uses /beginmulti instead.
[**setExternalUsingPUT1**](AllPublicApi.md#setExternalUsingPUT1) | **PUT** /extern/beginmulti | Start extern mode and creates users for this mode.
[**setcallbackurlUsingGET**](AllPublicApi.md#setcallbackurlUsingGET) | **GET** /configure/setcallbackurl | Modify sessions callbackURL
[**setcallbackurlUsingPOST**](AllPublicApi.md#setcallbackurlUsingPOST) | **POST** /configure/setcallbackurl | Modify sessions callbackURL
[**systemCheckAutoJSONUsingGET**](AllPublicApi.md#systemCheckAutoJSONUsingGET) | **GET** /admin/systemcheckauto | System health check
[**systemCheckAutoJSONUsingPOST**](AllPublicApi.md#systemCheckAutoJSONUsingPOST) | **POST** /admin/systemcheckauto | System health check
[**updateExternUserUsingPOST**](AllPublicApi.md#updateExternUserUsingPOST) | **POST** /extern/updateUser | Update external user information
[**updateExternUserUsingPUT**](AllPublicApi.md#updateExternUserUsingPUT) | **PUT** /extern/updateUser | Update external user information
[**updateTokenUsingPOST**](AllPublicApi.md#updateTokenUsingPOST) | **POST** /extern/updatetoken | Reaktivate external user token and create new password for user
[**updateTokenUsingPUT**](AllPublicApi.md#updateTokenUsingPUT) | **PUT** /extern/updatetoken | Reaktivate external user token and create new password for user
[**uploaddocumentstreammultipartUsingPOST**](AllPublicApi.md#uploaddocumentstreammultipartUsingPOST) | **POST** /configure/uploaddocument | Upload a document into a previously configured session
[**uploaddocumentstreammultipartieUsingPOST**](AllPublicApi.md#uploaddocumentstreammultipartieUsingPOST) | **POST** /configure/uploaddocumentie | Upload a document into a previously configured session
[**uploadsummaryUsingPOST**](AllPublicApi.md#uploadsummaryUsingPOST) | **POST** /configure/uploadsummary | Add summary page to a previously configured session
[**zurueckholenUsingPOST**](AllPublicApi.md#zurueckholenUsingPOST) | **POST** /extern/abort | Return session from external state
[**zurueckholenUsingPUT**](AllPublicApi.md#zurueckholenUsingPUT) | **PUT** /extern/abort | Return session from external state


# **addAdditionalDocumentUsingPOST**
> \Insign\Model\BasicResult addAdditionalDocumentUsingPOST($input, $sessionid)

Add one more documents metadata to a session after session creation.

The document binary might be upload separately later on or attached directly to this call

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ConfigurationInformationForASingleDocument(); // \Insign\Model\ConfigurationInformationForASingleDocument | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->addAdditionalDocumentUsingPOST($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->addAdditionalDocumentUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ConfigurationInformationForASingleDocument**](../Model/ConfigurationInformationForASingleDocument.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **adduserUsingPOST**
> \Insign\Model\UserID adduserUsingPOST($input)

Create new user

Usually no need to call this as internal users are automatically created on session creation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\UserInfo(); // \Insign\Model\UserInfo | input

try {
    $result = $apiInstance->adduserUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->adduserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\UserInfo**](../Model/UserInfo.md)| input |

### Return type

[**\Insign\Model\UserID**](../Model/UserID.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **changeSessionOwnerUsingPOST**
> \Insign\Model\BasicResult changeSessionOwnerUsingPOST($ch_owner)

Change a sessions owner

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ch_owner = new \Insign\Model\ChangeOwner(); // \Insign\Model\ChangeOwner | chOwner

try {
    $result = $apiInstance->changeSessionOwnerUsingPOST($ch_owner);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->changeSessionOwnerUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ch_owner** | [**\Insign\Model\ChangeOwner**](../Model/ChangeOwner.md)| chOwner |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cleanUpDBUsingDELETE1**
> cleanUpDBUsingDELETE1()

Cleanup database

Delete all session that have previously been marked as deleted from the database now. Usually no need to call this as it is usually trigger by an internal nightly cron job.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $apiInstance->cleanUpDBUsingDELETE1();
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->cleanUpDBUsingDELETE1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cleanUpDBUsingGET1**
> cleanUpDBUsingGET1()

Cleanup database

Delete all session that have previously been marked as deleted from the database now. Usually no need to call this as it is usually trigger by an internal nightly cron job.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $apiInstance->cleanUpDBUsingGET1();
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->cleanUpDBUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cleanUpDBUsingPOST1**
> cleanUpDBUsingPOST1()

Cleanup database

Delete all session that have previously been marked as deleted from the database now. Usually no need to call this as it is usually trigger by an internal nightly cron job.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $apiInstance->cleanUpDBUsingPOST1();
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->cleanUpDBUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **configuredocumentsUsingPOST**
> \Insign\Model\ConfigureDocumentsResult configuredocumentsUsingPOST($input, $configid)

Creates a new session with the provided metadata for the documents

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ConfigureSession(); // \Insign\Model\ConfigureSession | input
$configid = "configid_example"; // string | configid

try {
    $result = $apiInstance->configuredocumentsUsingPOST($input, $configid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->configuredocumentsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ConfigureSession**](../Model/ConfigureSession.md)| input |
 **configid** | **string**| configid | [optional]

### Return type

[**\Insign\Model\ConfigureDocumentsResult**](../Model/ConfigureDocumentsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createVorgangsverwaltungSessionUsingGET**
> string createVorgangsverwaltungSessionUsingGET($sessionid, $from_editor)

Creates a new session for process management

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$from_editor = true; // bool | fromEditor

try {
    $result = $apiInstance->createVorgangsverwaltungSessionUsingGET($sessionid, $from_editor);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->createVorgangsverwaltungSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **from_editor** | **bool**| fromEditor | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createVorgangsverwaltungSessionUsingPOST**
> string createVorgangsverwaltungSessionUsingPOST($sessionid, $from_editor)

Creates a new session for process management

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$from_editor = true; // bool | fromEditor

try {
    $result = $apiInstance->createVorgangsverwaltungSessionUsingPOST($sessionid, $from_editor);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->createVorgangsverwaltungSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **from_editor** | **bool**| fromEditor | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **delegateUsingPOST**
> \Insign\Model\BasicResult delegateUsingPOST($input, $sessionid)

Redirect extern process to another user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ExternDelegate(); // \Insign\Model\ExternDelegate | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->delegateUsingPOST($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->delegateUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ExternDelegate**](../Model/ExternDelegate.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **delegateUsingPUT**
> \Insign\Model\BasicResult delegateUsingPUT($input, $sessionid)

Redirect extern process to another user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ExternDelegate(); // \Insign\Model\ExternDelegate | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->delegateUsingPUT($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->delegateUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ExternDelegate**](../Model/ExternDelegate.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDocumentUsingDELETE**
> \Insign\Model\BasicResult deleteDocumentUsingDELETE($documentidtodelete, $sessionid, $sessionidtodeletefrom)

Remove document from session. Remove session if no documents left in the session after the operation.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$documentidtodelete = "documentidtodelete_example"; // string | documentidtodelete
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodeletefrom = "sessionidtodeletefrom_example"; // string | sessionidtodeletefrom

try {
    $result = $apiInstance->deleteDocumentUsingDELETE($documentidtodelete, $sessionid, $sessionidtodeletefrom);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deleteDocumentUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **documentidtodelete** | **string**| documentidtodelete | [optional]
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodeletefrom** | **string**| sessionidtodeletefrom | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDocumentUsingGET**
> \Insign\Model\BasicResult deleteDocumentUsingGET($documentidtodelete, $sessionid, $sessionidtodeletefrom)

Remove document from session. Remove session if no documents left in the session after the operation.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$documentidtodelete = "documentidtodelete_example"; // string | documentidtodelete
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodeletefrom = "sessionidtodeletefrom_example"; // string | sessionidtodeletefrom

try {
    $result = $apiInstance->deleteDocumentUsingGET($documentidtodelete, $sessionid, $sessionidtodeletefrom);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deleteDocumentUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **documentidtodelete** | **string**| documentidtodelete | [optional]
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodeletefrom** | **string**| sessionidtodeletefrom | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDocumentUsingPOST**
> \Insign\Model\BasicResult deleteDocumentUsingPOST($documentidtodelete, $sessionid, $sessionidtodeletefrom)

Remove document from session. Remove session if no documents left in the session after the operation.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$documentidtodelete = "documentidtodelete_example"; // string | documentidtodelete
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodeletefrom = "sessionidtodeletefrom_example"; // string | sessionidtodeletefrom

try {
    $result = $apiInstance->deleteDocumentUsingPOST($documentidtodelete, $sessionid, $sessionidtodeletefrom);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deleteDocumentUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **documentidtodelete** | **string**| documentidtodelete | [optional]
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodeletefrom** | **string**| sessionidtodeletefrom | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSessionUsingDELETE**
> \Insign\Model\BasicResult deleteSessionUsingDELETE($sessionid, $sessionidtodelete)

Mark session as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->deleteSessionUsingDELETE($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deleteSessionUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSessionUsingGET**
> \Insign\Model\BasicResult deleteSessionUsingGET($sessionid, $sessionidtodelete)

Mark session as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->deleteSessionUsingGET($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deleteSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSessionUsingPOST**
> \Insign\Model\BasicResult deleteSessionUsingPOST($sessionid, $sessionidtodelete)

Mark session as deleted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->deleteSessionUsingPOST($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deleteSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingDELETE**
> deleteUserAndSessionsUsingDELETE($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingDELETE($user);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deleteUserAndSessionsUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingGET**
> deleteUserAndSessionsUsingGET($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingGET($user);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deleteUserAndSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAndSessionsUsingPOST**
> deleteUserAndSessionsUsingPOST($user)

Remove a user

Remove a user and all sessions belonging to this user (foruser/owner) from the database now.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | user

try {
    $apiInstance->deleteUserAndSessionsUsingPOST($user);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deleteUserAndSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| user |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletedocumentUsingDELETE**
> \Insign\Model\BasicResult deletedocumentUsingDELETE($docid, $sessionid)

Remove a document from a session

Document is removed if possible (i.e. document is marked as deleteable).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deletedocumentUsingDELETE($docid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deletedocumentUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletedocumentUsingGET**
> \Insign\Model\BasicResult deletedocumentUsingGET($docid, $sessionid)

Remove a document from a session

Document is removed if possible (i.e. document is marked as deleteable).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deletedocumentUsingGET($docid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deletedocumentUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletedocumentUsingPOST**
> \Insign\Model\BasicResult deletedocumentUsingPOST($docid, $sessionid)

Remove a document from a session

Document is removed if possible (i.e. document is marked as deleteable).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->deletedocumentUsingPOST($docid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->deletedocumentUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsMultiUsingGET1**
> downloadDocumentsMultiUsingGET1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download several session documents as zips in zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = array("sessionid_example"); // string[] | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsMultiUsingGET1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->downloadDocumentsMultiUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | [**string[]**](../Model/string.md)| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsMultiUsingPOST1**
> downloadDocumentsMultiUsingPOST1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download several session documents as zips in zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = array("sessionid_example"); // string[] | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsMultiUsingPOST1($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->downloadDocumentsMultiUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | [**string[]**](../Model/string.md)| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsProtectedUsingGET**
> downloadDocumentsProtectedUsingGET($sessionid, $bestaetigentoken, $mustberead)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$bestaetigentoken = "bestaetigentoken_example"; // string | bestaetigentoken
$mustberead = true; // bool | mustberead

try {
    $apiInstance->downloadDocumentsProtectedUsingGET($sessionid, $bestaetigentoken, $mustberead);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->downloadDocumentsProtectedUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **bestaetigentoken** | **string**| bestaetigentoken | [optional]
 **mustberead** | **bool**| mustberead | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsProtectedUsingPOST**
> downloadDocumentsProtectedUsingPOST($sessionid, $bestaetigentoken, $mustberead)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$bestaetigentoken = "bestaetigentoken_example"; // string | bestaetigentoken
$mustberead = true; // bool | mustberead

try {
    $apiInstance->downloadDocumentsProtectedUsingPOST($sessionid, $bestaetigentoken, $mustberead);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->downloadDocumentsProtectedUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **bestaetigentoken** | **string**| bestaetigentoken | [optional]
 **mustberead** | **bool**| mustberead | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsUsingGET2**
> downloadDocumentsUsingGET2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsUsingGET2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->downloadDocumentsUsingGET2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadDocumentsUsingPOST2**
> downloadDocumentsUsingPOST2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type)

Download all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$auditreport = true; // bool | auditreport
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$inc_bio_data = true; // bool | incBioData
$mustberead = true; // bool | mustberead
$type = "type_example"; // string | type

try {
    $apiInstance->downloadDocumentsUsingPOST2($sessionid, $auditreport, $id_vorgangsverwaltung, $inc_bio_data, $mustberead, $type);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->downloadDocumentsUsingPOST2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **auditreport** | **bool**| auditreport | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **inc_bio_data** | **bool**| incBioData | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **type** | **string**| type | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **exportSessionUsingGET**
> exportSessionUsingGET($compression_format, $compression_level, $session_id)

Export a session as binary data.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$compression_format = "compression_format_example"; // string | compression format may use 'gzip'
$compression_level = 56; // int | 1 to 9
$session_id = "session_id_example"; // string | session id

try {
    $apiInstance->exportSessionUsingGET($compression_format, $compression_level, $session_id);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->exportSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **compression_format** | **string**| compression format may use &#39;gzip&#39; | [optional]
 **compression_level** | **int**| 1 to 9 | [optional]
 **session_id** | **string**| session id | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **exportSessionUsingPOST**
> exportSessionUsingPOST($compression_format, $compression_level, $session_id)

Export a session as binary data.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$compression_format = "compression_format_example"; // string | compression format may use 'gzip'
$compression_level = 56; // int | 1 to 9
$session_id = "session_id_example"; // string | session id

try {
    $apiInstance->exportSessionUsingPOST($compression_format, $compression_level, $session_id);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->exportSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **compression_format** | **string**| compression format may use &#39;gzip&#39; | [optional]
 **compression_level** | **int**| 1 to 9 | [optional]
 **session_id** | **string**| session id | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBestaetigungsdatumUsingGET**
> \Insign\Model\GenericResult getBestaetigungsdatumUsingGET($sessionid)

Retrieve document receipt confirmation timestamp

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getBestaetigungsdatumUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getBestaetigungsdatumUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\GenericResult**](../Model/GenericResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBestaetigungsdatumUsingPOST**
> \Insign\Model\GenericResult getBestaetigungsdatumUsingPOST($sessionid)

Retrieve document receipt confirmation timestamp

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getBestaetigungsdatumUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getBestaetigungsdatumUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\GenericResult**](../Model/GenericResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBlobDataUsingGET**
> string getBlobDataUsingGET($sessionid)

Get session custom blob data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getBlobDataUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getBlobDataUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBlobDataUsingPOST**
> string getBlobDataUsingPOST($sessionid)

Get session custom blob data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getBlobDataUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getBlobDataUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCheckStatusUsingGET**
> \Insign\Model\CheckStatusResult getCheckStatusUsingGET($sessionid, $id_vorgangsverwaltung)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getCheckStatusUsingGET($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getCheckStatusUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\CheckStatusResult**](../Model/CheckStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCheckStatusUsingPOST**
> \Insign\Model\CheckStatusResult getCheckStatusUsingPOST($sessionid, $id_vorgangsverwaltung)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getCheckStatusUsingPOST($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getCheckStatusUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\CheckStatusResult**](../Model/CheckStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDocumentPageImageB64UsingGET**
> getDocumentPageImageB64UsingGET($docid, $page, $sessionid, $id_vorgangsverwaltung)

Render a documents page as PNG Bitmap. Returns Base64 data.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$page = 56; // int | page
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $apiInstance->getDocumentPageImageB64UsingGET($docid, $page, $sessionid, $id_vorgangsverwaltung);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getDocumentPageImageB64UsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **page** | **int**| page |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDocumentPageImageUsingGET**
> getDocumentPageImageUsingGET($docid, $page, $sessionid, $id_vorgangsverwaltung)

Render a documents page as PNG Bitmap

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$page = 56; // int | page
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $apiInstance->getDocumentPageImageUsingGET($docid, $page, $sessionid, $id_vorgangsverwaltung);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getDocumentPageImageUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **page** | **int**| page |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDocumentThumbnailB64UsingGET**
> getDocumentThumbnailB64UsingGET($docid, $page, $sessionid, $id_vorgangsverwaltung)

Render a documents page as PNG Thumbnail-Bitmap. Returns Base64 data.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$page = 56; // int | page
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $apiInstance->getDocumentThumbnailB64UsingGET($docid, $page, $sessionid, $id_vorgangsverwaltung);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getDocumentThumbnailB64UsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **page** | **int**| page |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDocumentThumbnailUsingGET**
> getDocumentThumbnailUsingGET($docid, $page, $sessionid, $id_vorgangsverwaltung)

Render a documents page as PNG Thumbnail-Bitmap

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$page = 56; // int | page
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $apiInstance->getDocumentThumbnailUsingGET($docid, $page, $sessionid, $id_vorgangsverwaltung);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getDocumentThumbnailUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **page** | **int**| page |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDocumentUsingGET**
> getDocumentUsingGET($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type)

Download single document containing the current signatures and modifications

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid
$externtoken = "externtoken_example"; // string | Token for external access if access by SSO User. Only used internally
$includebiodata = true; // bool | Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat).
$originalfile = false; // bool | Get the original file that was initially uploaded? Default: false.
$type = "type_example"; // string | Type of download. Internal use only. type='form' to keep form field in the document and skip signatures.

try {
    $apiInstance->getDocumentUsingGET($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getDocumentUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |
 **externtoken** | **string**| Token for external access if access by SSO User. Only used internally | [optional]
 **includebiodata** | **bool**| Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat). | [optional] [default to true]
 **originalfile** | **bool**| Get the original file that was initially uploaded? Default: false. | [optional] [default to false]
 **type** | **string**| Type of download. Internal use only. type&#x3D;&#39;form&#39; to keep form field in the document and skip signatures. | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDocumentUsingPOST**
> getDocumentUsingPOST($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type)

Download single document containing the current signatures and modifications

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid
$externtoken = "externtoken_example"; // string | Token for external access if access by SSO User. Only used internally
$includebiodata = true; // bool | Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat).
$originalfile = false; // bool | Get the original file that was initially uploaded? Default: false.
$type = "type_example"; // string | Type of download. Internal use only. type='form' to keep form field in the document and skip signatures.

try {
    $apiInstance->getDocumentUsingPOST($docid, $sessionid, $externtoken, $includebiodata, $originalfile, $type);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getDocumentUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |
 **externtoken** | **string**| Token for external access if access by SSO User. Only used internally | [optional]
 **includebiodata** | **bool**| Type of file. Includeing Biopmetric data (true) or without biometric data (flat type). Behaviour of flat type form fields (flattened, readonly) is as configured for this session. Behaviour of signatur fields as configured for this session (skipbiodata, flat). | [optional] [default to true]
 **originalfile** | **bool**| Get the original file that was initially uploaded? Default: false. | [optional] [default to false]
 **type** | **string**| Type of download. Internal use only. type&#x3D;&#39;form&#39; to keep form field in the document and skip signatures. | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEnvelopedDocumentsUsingGET1**
> getEnvelopedDocumentsUsingGET1($sessionid, $embed_bio_data, $file)

Attach sessions current documents to the provided container pdf-file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$embed_bio_data = true; // bool | embedBioData
$file = "/path/to/file.txt"; // \SplFileObject | file

try {
    $apiInstance->getEnvelopedDocumentsUsingGET1($sessionid, $embed_bio_data, $file);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getEnvelopedDocumentsUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **embed_bio_data** | **bool**| embedBioData | [optional] [default to true]
 **file** | **\SplFileObject**| file | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/pdf

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEnvelopedDocumentsUsingPOST1**
> getEnvelopedDocumentsUsingPOST1($sessionid, $embed_bio_data, $file)

Attach sessions current documents to the provided container pdf-file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = new \stdClass; // object | sessionid
$embed_bio_data = new \stdClass; // object | embedBioData
$file = "/path/to/file.txt"; // \SplFileObject | file

try {
    $apiInstance->getEnvelopedDocumentsUsingPOST1($sessionid, $embed_bio_data, $file);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getEnvelopedDocumentsUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | [**object**](../Model/.md)| sessionid |
 **embed_bio_data** | [**object**](../Model/.md)| embedBioData | [optional]
 **file** | **\SplFileObject**| file | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/pdf

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEnvelopedDocumentsUsingPUT1**
> getEnvelopedDocumentsUsingPUT1($sessionid, $embed_bio_data, $file)

Attach sessions current documents to the provided container pdf-file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$embed_bio_data = true; // bool | embedBioData
$file = "/path/to/file.txt"; // \SplFileObject | file

try {
    $apiInstance->getEnvelopedDocumentsUsingPUT1($sessionid, $embed_bio_data, $file);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getEnvelopedDocumentsUsingPUT1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **embed_bio_data** | **bool**| embedBioData | [optional] [default to true]
 **file** | **\SplFileObject**| file | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/pdf

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGWGInfosUsingGET**
> \Insign\Model\GWGInfo getGWGInfosUsingGET($sessionid, $id_vorgangsverwaltung)

Retrieve information about ID-Card photos from a session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getGWGInfosUsingGET($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getGWGInfosUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\GWGInfo**](../Model/GWGInfo.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGWGInfosUsingPOST**
> \Insign\Model\GWGInfo getGWGInfosUsingPOST($sessionid, $id_vorgangsverwaltung)

Retrieve information about ID-Card photos from a session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getGWGInfosUsingPOST($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getGWGInfosUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\GWGInfo**](../Model/GWGInfo.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionInfoUsingGET**
> \Insign\Model\InformationAboutASession getSessionInfoUsingGET($session_id)

Get session metadata for export

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$session_id = "session_id_example"; // string | sessionId

try {
    $result = $apiInstance->getSessionInfoUsingGET($session_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getSessionInfoUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **string**| sessionId |

### Return type

[**\Insign\Model\InformationAboutASession**](../Model/InformationAboutASession.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionInfoUsingPOST**
> \Insign\Model\InformationAboutASession getSessionInfoUsingPOST($session_id)

Get session metadata for export

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$session_id = "session_id_example"; // string | sessionId

try {
    $result = $apiInstance->getSessionInfoUsingPOST($session_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getSessionInfoUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **string**| sessionId |

### Return type

[**\Insign\Model\InformationAboutASession**](../Model/InformationAboutASession.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingGET3**
> \Insign\Model\SessionsResult getSessionsUsingGET3($separator, $user)

List of session that belong to a single user. You can also pass a user list separated by 'separator' for multiple users

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$separator = "separator_example"; // string | Separator for users (only required using list)
$user = "user_example"; // string | User ID(s)

try {
    $result = $apiInstance->getSessionsUsingGET3($separator, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getSessionsUsingGET3: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **separator** | **string**| Separator for users (only required using list) | [optional]
 **user** | **string**| User ID(s) | [optional]

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingPOST1**
> \Insign\Model\SessionsResult getSessionsUsingPOST1($iso3_country, $iso3_language, $country, $display_country, $display_language, $display_name, $display_script, $display_variant, $language, $script, $sessionids, $unicode_locale_attributes, $unicode_locale_keys, $variant)

Retrieve information about a bunch of sessions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$iso3_country = "iso3_country_example"; // string | 
$iso3_language = "iso3_language_example"; // string | 
$country = "country_example"; // string | 
$display_country = "display_country_example"; // string | 
$display_language = "display_language_example"; // string | 
$display_name = "display_name_example"; // string | 
$display_script = "display_script_example"; // string | 
$display_variant = "display_variant_example"; // string | 
$language = "language_example"; // string | 
$script = "script_example"; // string | 
$sessionids = new \Insign\Model\ListOfSessionIDs(); // \Insign\Model\ListOfSessionIDs | list of session to retrieve information about
$unicode_locale_attributes = array("unicode_locale_attributes_example"); // string[] | 
$unicode_locale_keys = array("unicode_locale_keys_example"); // string[] | 
$variant = "variant_example"; // string | 

try {
    $result = $apiInstance->getSessionsUsingPOST1($iso3_country, $iso3_language, $country, $display_country, $display_language, $display_name, $display_script, $display_variant, $language, $script, $sessionids, $unicode_locale_attributes, $unicode_locale_keys, $variant);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getSessionsUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iso3_country** | **string**|  | [optional]
 **iso3_language** | **string**|  | [optional]
 **country** | **string**|  | [optional]
 **display_country** | **string**|  | [optional]
 **display_language** | **string**|  | [optional]
 **display_name** | **string**|  | [optional]
 **display_script** | **string**|  | [optional]
 **display_variant** | **string**|  | [optional]
 **language** | **string**|  | [optional]
 **script** | **string**|  | [optional]
 **sessionids** | [**\Insign\Model\ListOfSessionIDs**](../Model/ListOfSessionIDs.md)| list of session to retrieve information about | [optional]
 **unicode_locale_attributes** | [**string[]**](../Model/string.md)|  | [optional]
 **unicode_locale_keys** | [**string[]**](../Model/string.md)|  | [optional]
 **variant** | **string**|  | [optional]

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingPOST3**
> \Insign\Model\SessionsResult getSessionsUsingPOST3($separator, $user)

List of session that belong to a single user. You can also pass a user list separated by 'separator' for multiple users

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$separator = "separator_example"; // string | Separator for users (only required using list)
$user = "user_example"; // string | User ID(s)

try {
    $result = $apiInstance->getSessionsUsingPOST3($separator, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getSessionsUsingPOST3: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **separator** | **string**| Separator for users (only required using list) | [optional]
 **user** | **string**| User ID(s) | [optional]

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusLoadableUsingGET**
> \Insign\Model\SessionStatusResult getStatusLoadableUsingGET($sessionid, $id_vorgangsverwaltung)

Get session status information and load session into memory if not already loaded

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getStatusLoadableUsingGET($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getStatusLoadableUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusLoadableUsingPOST**
> \Insign\Model\SessionStatusResult getStatusLoadableUsingPOST($sessionid, $id_vorgangsverwaltung)

Get session status information and load session into memory if not already loaded

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getStatusLoadableUsingPOST($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getStatusLoadableUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusUsingGET**
> \Insign\Model\SessionStatusResult getStatusUsingGET($sessionid, $token)

Retrieve status about external user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->getStatusUsingGET($sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getStatusUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusUsingGET1**
> \Insign\Model\SessionStatusResult getStatusUsingGET1($sessionid)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getStatusUsingGET1($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getStatusUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusUsingPOST**
> \Insign\Model\SessionStatusResult getStatusUsingPOST($sessionid)

Get session status information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getStatusUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getStatusUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\SessionStatusResult**](../Model/SessionStatusResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransaktionsnummerUsingGET**
> \Insign\Model\TransaktionsnummerResult getTransaktionsnummerUsingGET($sessionid)

Get session Transaction number (TAN)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getTransaktionsnummerUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getTransaktionsnummerUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\TransaktionsnummerResult**](../Model/TransaktionsnummerResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransaktionsnummerUsingPOST**
> \Insign\Model\TransaktionsnummerResult getTransaktionsnummerUsingPOST($sessionid)

Get session Transaction number (TAN)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getTransaktionsnummerUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getTransaktionsnummerUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\TransaktionsnummerResult**](../Model/TransaktionsnummerResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUserForSessionsUsingGET**
> \Insign\Model\UserID getUserForSessionsUsingGET($sessionid)

Get owner of a session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getUserForSessionsUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getUserForSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\UserID**](../Model/UserID.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUserForSessionsUsingPOST**
> \Insign\Model\UserID getUserForSessionsUsingPOST($sessionid)

Get owner of a session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getUserForSessionsUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getUserForSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\UserID**](../Model/UserID.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVersionUsingGET**
> string getVersionUsingGET()

Get inSign version

Retrieve application version.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getVersionUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getVersionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVersionUsingPOST**
> string getVersionUsingPOST()

Get inSign version

Retrieve application version.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getVersionUsingPOST();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->getVersionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **importSessionUsingGET**
> \Insign\Model\SessionResult importSessionUsingGET()

Import binary session data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->importSessionUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->importSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Insign\Model\SessionResult**](../Model/SessionResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/octet-stream
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **importSessionUsingPOST**
> \Insign\Model\SessionResult importSessionUsingPOST()

Import binary session data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->importSessionUsingPOST();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->importSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Insign\Model\SessionResult**](../Model/SessionResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/octet-stream
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **loadSessionUsingPOST**
> \Insign\Model\SessionResult loadSessionUsingPOST($input, $sessionid, $sessionidtoload)

Load session from database

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ConfigurationInformationForLoadingAProcess(); // \Insign\Model\ConfigurationInformationForLoadingAProcess | input
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtoload = "sessionidtoload_example"; // string | sessionidtoload

try {
    $result = $apiInstance->loadSessionUsingPOST($input, $sessionid, $sessionidtoload);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->loadSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ConfigurationInformationForLoadingAProcess**](../Model/ConfigurationInformationForLoadingAProcess.md)| input | [optional]
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtoload** | **string**| sessionidtoload | [optional]

### Return type

[**\Insign\Model\SessionResult**](../Model/SessionResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **newTokenUsingGET**
> \Insign\Model\ExternUserResult newTokenUsingGET($sessionid, $token)

Reaktivate external user token and create new password for user if token was renewed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->newTokenUsingGET($sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->newTokenUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **newTokenUsingPOST**
> \Insign\Model\ExternUserResult newTokenUsingPOST($sessionid, $token)

Reaktivate external user token and create new password for user if token was renewed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->newTokenUsingPOST($sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->newTokenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **newTokenUsingPUT**
> \Insign\Model\ExternUserResult newTokenUsingPUT($sessionid, $token)

Reaktivate external user token and create new password for user if token was renewed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->newTokenUsingPUT($sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->newTokenUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purgeSessionUsingDELETE**
> \Insign\Model\BasicResult purgeSessionUsingDELETE($sessionid, $sessionidtodelete)

Remove session by TAN oder sessionid. Removes session from memory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->purgeSessionUsingDELETE($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->purgeSessionUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purgeSessionUsingGET**
> \Insign\Model\BasicResult purgeSessionUsingGET($sessionid, $sessionidtodelete)

Remove session by TAN oder sessionid. Removes session from memory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->purgeSessionUsingGET($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->purgeSessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purgeSessionUsingPOST**
> \Insign\Model\BasicResult purgeSessionUsingPOST($sessionid, $sessionidtodelete)

Remove session by TAN oder sessionid. Removes session from memory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$sessionidtodelete = "sessionidtodelete_example"; // string | sessionidtodelete

try {
    $result = $apiInstance->purgeSessionUsingPOST($sessionid, $sessionidtodelete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->purgeSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]
 **sessionidtodelete** | **string**| sessionidtodelete | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **reconfigureSessionUsingPOST**
> \Insign\Model\Result reconfigureSessionUsingPOST($configure, $session_id)

Reconfigure a session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$configure = new \Insign\Model\ConfigureSession(); // \Insign\Model\ConfigureSession | configure
$session_id = "session_id_example"; // string | sessionId

try {
    $result = $apiInstance->reconfigureSessionUsingPOST($configure, $session_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->reconfigureSessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **configure** | [**\Insign\Model\ConfigureSession**](../Model/ConfigureSession.md)| configure |
 **session_id** | **string**| sessionId |

### Return type

[**\Insign\Model\Result**](../Model/Result.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recoverdeleteSessionsUsingDELETE**
> \Insign\Model\BasicResult recoverdeleteSessionsUsingDELETE($sessionid)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->recoverdeleteSessionsUsingDELETE($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->recoverdeleteSessionsUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recoverdeleteSessionsUsingGET**
> \Insign\Model\BasicResult recoverdeleteSessionsUsingGET($sessionid)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->recoverdeleteSessionsUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->recoverdeleteSessionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recoverdeleteSessionsUsingPOST**
> \Insign\Model\BasicResult recoverdeleteSessionsUsingPOST($sessionid)

Retrieve all sessions documents as single zip file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->recoverdeleteSessionsUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->recoverdeleteSessionsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetSignatureUsingPOST**
> \Insign\Model\RestSingleSignature resetSignatureUsingPOST($data, $sessionid)

Redirect extern process to another user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$data = new \Insign\Model\DeleteSignatureExtern(); // \Insign\Model\DeleteSignatureExtern | data
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->resetSignatureUsingPOST($data, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->resetSignatureUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Insign\Model\DeleteSignatureExtern**](../Model/DeleteSignatureExtern.md)| data |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RestSingleSignature**](../Model/RestSingleSignature.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetSignatureUsingPUT**
> \Insign\Model\RestSingleSignature resetSignatureUsingPUT($data, $sessionid)

Redirect extern process to another user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$data = new \Insign\Model\DeleteSignatureExtern(); // \Insign\Model\DeleteSignatureExtern | data
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->resetSignatureUsingPUT($data, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->resetSignatureUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Insign\Model\DeleteSignatureExtern**](../Model/DeleteSignatureExtern.md)| data |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RestSingleSignature**](../Model/RestSingleSignature.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetSignaturesUsingGET**
> \Insign\Model\Result resetSignaturesUsingGET($sessionid, $docid)

Reset sessions signatures

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$docid = "docid_example"; // string | docid

try {
    $result = $apiInstance->resetSignaturesUsingGET($sessionid, $docid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->resetSignaturesUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **docid** | **string**| docid | [optional]

### Return type

[**\Insign\Model\Result**](../Model/Result.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetSignaturesUsingPOST**
> \Insign\Model\Result resetSignaturesUsingPOST($sessionid, $docid)

Reset sessions signatures

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$docid = "docid_example"; // string | docid

try {
    $result = $apiInstance->resetSignaturesUsingPOST($sessionid, $docid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->resetSignaturesUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **docid** | **string**| docid | [optional]

### Return type

[**\Insign\Model\Result**](../Model/Result.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restartsessionUsingGET**
> \Insign\Model\RedirectView restartsessionUsingGET($sessionid)

Reset sessions information

Reset documents to inital state after upload. Clear all signatures and input fields. Generate new TAN number.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->restartsessionUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->restartsessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restartsessionUsingPOST**
> \Insign\Model\RedirectView restartsessionUsingPOST($sessionid)

Reset sessions information

Reset documents to inital state after upload. Clear all signatures and input fields. Generate new TAN number.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->restartsessionUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->restartsessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sessionloeschenUsingGET**
> object sessionloeschenUsingGET($sessionid, $gdpr_declined)

Abort and delete session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$gdpr_declined = true; // bool | gdprDeclined

try {
    $result = $apiInstance->sessionloeschenUsingGET($sessionid, $gdpr_declined);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->sessionloeschenUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **gdpr_declined** | **bool**| gdprDeclined | [optional]

### Return type

**object**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sessionloeschenUsingPOST**
> object sessionloeschenUsingPOST($sessionid, $gdpr_declined)

Abort and delete session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$gdpr_declined = true; // bool | gdprDeclined

try {
    $result = $apiInstance->sessionloeschenUsingPOST($sessionid, $gdpr_declined);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->sessionloeschenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **gdpr_declined** | **bool**| gdprDeclined | [optional]

### Return type

**object**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setExternalUsingPOST**
> \Insign\Model\ExternUserResult setExternalUsingPOST($input, $sessionid, $id_vorgangsverwaltung)

Start extern mode. Uses /beginmulti instead.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\StartExtern(); // \Insign\Model\StartExtern | input
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->setExternalUsingPOST($input, $sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->setExternalUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\StartExtern**](../Model/StartExtern.md)| input |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setExternalUsingPOST1**
> \Insign\Model\ExternMultiuserResult setExternalUsingPOST1($input, $sessionid, $id_vorgangsverwaltung)

Start extern mode and creates users for this mode.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ContainerForMultipleExternalUsers(); // \Insign\Model\ContainerForMultipleExternalUsers | input
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->setExternalUsingPOST1($input, $sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->setExternalUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ContainerForMultipleExternalUsers**](../Model/ContainerForMultipleExternalUsers.md)| input |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternMultiuserResult**](../Model/ExternMultiuserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setExternalUsingPUT**
> \Insign\Model\ExternUserResult setExternalUsingPUT($input, $sessionid, $id_vorgangsverwaltung)

Start extern mode. Uses /beginmulti instead.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\StartExtern(); // \Insign\Model\StartExtern | input
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->setExternalUsingPUT($input, $sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->setExternalUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\StartExtern**](../Model/StartExtern.md)| input |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setExternalUsingPUT1**
> \Insign\Model\ExternMultiuserResult setExternalUsingPUT1($input, $sessionid, $id_vorgangsverwaltung)

Start extern mode and creates users for this mode.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ContainerForMultipleExternalUsers(); // \Insign\Model\ContainerForMultipleExternalUsers | input
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->setExternalUsingPUT1($input, $sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->setExternalUsingPUT1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ContainerForMultipleExternalUsers**](../Model/ContainerForMultipleExternalUsers.md)| input |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternMultiuserResult**](../Model/ExternMultiuserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setcallbackurlUsingGET**
> \Insign\Model\SessionResult setcallbackurlUsingGET($sessionid, $callbackurl, $callbackurlabschliessen)

Modify sessions callbackURL

If you want to modify the callbackURL property of an existing session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$callbackurl = "callbackurl_example"; // string | callbackurl
$callbackurlabschliessen = "callbackurlabschliessen_example"; // string | callbackurlabschliessen

try {
    $result = $apiInstance->setcallbackurlUsingGET($sessionid, $callbackurl, $callbackurlabschliessen);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->setcallbackurlUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **callbackurl** | **string**| callbackurl | [optional]
 **callbackurlabschliessen** | **string**| callbackurlabschliessen | [optional]

### Return type

[**\Insign\Model\SessionResult**](../Model/SessionResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setcallbackurlUsingPOST**
> \Insign\Model\SessionResult setcallbackurlUsingPOST($sessionid, $callbackurl, $callbackurlabschliessen)

Modify sessions callbackURL

If you want to modify the callbackURL property of an existing session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$callbackurl = "callbackurl_example"; // string | callbackurl
$callbackurlabschliessen = "callbackurlabschliessen_example"; // string | callbackurlabschliessen

try {
    $result = $apiInstance->setcallbackurlUsingPOST($sessionid, $callbackurl, $callbackurlabschliessen);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->setcallbackurlUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **callbackurl** | **string**| callbackurl | [optional]
 **callbackurlabschliessen** | **string**| callbackurlabschliessen | [optional]

### Return type

[**\Insign\Model\SessionResult**](../Model/SessionResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **systemCheckAutoJSONUsingGET**
> string systemCheckAutoJSONUsingGET($dbschemacheck)

System health check

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dbschemacheck = false; // bool | dbschemacheck

try {
    $result = $apiInstance->systemCheckAutoJSONUsingGET($dbschemacheck);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->systemCheckAutoJSONUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbschemacheck** | **bool**| dbschemacheck | [optional] [default to false]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain;charset=UTF-8, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **systemCheckAutoJSONUsingPOST**
> string systemCheckAutoJSONUsingPOST($dbschemacheck)

System health check

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dbschemacheck = false; // bool | dbschemacheck

try {
    $result = $apiInstance->systemCheckAutoJSONUsingPOST($dbschemacheck);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->systemCheckAutoJSONUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbschemacheck** | **bool**| dbschemacheck | [optional] [default to false]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain;charset=UTF-8, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExternUserUsingPOST**
> \Insign\Model\ExternUserResult updateExternUserUsingPOST($input, $sessionid, $token)

Update external user information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\StartExtern(); // \Insign\Model\StartExtern | input
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->updateExternUserUsingPOST($input, $sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->updateExternUserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\StartExtern**](../Model/StartExtern.md)| input |
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExternUserUsingPUT**
> \Insign\Model\ExternUserResult updateExternUserUsingPUT($input, $sessionid, $token)

Update external user information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\StartExtern(); // \Insign\Model\StartExtern | input
$sessionid = "sessionid_example"; // string | sessionid
$token = "token_example"; // string | token

try {
    $result = $apiInstance->updateExternUserUsingPUT($input, $sessionid, $token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->updateExternUserUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\StartExtern**](../Model/StartExtern.md)| input |
 **sessionid** | **string**| sessionid |
 **token** | **string**| token |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTokenUsingPOST**
> \Insign\Model\ExternUserResult updateTokenUsingPOST($input, $sessionid)

Reaktivate external user token and create new password for user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ExternUserResult(); // \Insign\Model\ExternUserResult | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->updateTokenUsingPOST($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->updateTokenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTokenUsingPUT**
> \Insign\Model\ExternUserResult updateTokenUsingPUT($input, $sessionid)

Reaktivate external user token and create new password for user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\ExternUserResult(); // \Insign\Model\ExternUserResult | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->updateTokenUsingPUT($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->updateTokenUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\ExternUserResult**](../Model/ExternUserResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploaddocumentstreammultipartUsingPOST**
> string uploaddocumentstreammultipartUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user)

Upload a document into a previously configured session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filename = new \stdClass; // object | filename
$sessionid = new \stdClass; // object | sessionid
$docid = new \stdClass; // object | docid
$file = "/path/to/file.txt"; // \SplFileObject | file
$filesize = new \stdClass; // object | filesize
$pairing_token = new \stdClass; // object | pairingToken
$user = new \stdClass; // object | user

try {
    $result = $apiInstance->uploaddocumentstreammultipartUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->uploaddocumentstreammultipartUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filename** | [**object**](../Model/.md)| filename |
 **sessionid** | [**object**](../Model/.md)| sessionid |
 **docid** | [**object**](../Model/.md)| docid | [optional]
 **file** | **\SplFileObject**| file | [optional]
 **filesize** | [**object**](../Model/.md)| filesize | [optional]
 **pairing_token** | [**object**](../Model/.md)| pairingToken | [optional]
 **user** | [**object**](../Model/.md)| user | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploaddocumentstreammultipartieUsingPOST**
> string uploaddocumentstreammultipartieUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user)

Upload a document into a previously configured session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filename = new \stdClass; // object | filename
$sessionid = new \stdClass; // object | sessionid
$docid = new \stdClass; // object | docid
$file = "/path/to/file.txt"; // \SplFileObject | file
$filesize = new \stdClass; // object | filesize
$pairing_token = new \stdClass; // object | pairingToken
$user = new \stdClass; // object | user

try {
    $result = $apiInstance->uploaddocumentstreammultipartieUsingPOST($filename, $sessionid, $docid, $file, $filesize, $pairing_token, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->uploaddocumentstreammultipartieUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filename** | [**object**](../Model/.md)| filename |
 **sessionid** | [**object**](../Model/.md)| sessionid |
 **docid** | [**object**](../Model/.md)| docid | [optional]
 **file** | **\SplFileObject**| file | [optional]
 **filesize** | [**object**](../Model/.md)| filesize | [optional]
 **pairing_token** | [**object**](../Model/.md)| pairingToken | [optional]
 **user** | [**object**](../Model/.md)| user | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploadsummaryUsingPOST**
> uploadsummaryUsingPOST($file, $sessionid, $foruser)

Add summary page to a previously configured session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$file = "/path/to/file.txt"; // \SplFileObject | file
$sessionid = new \stdClass; // object | sessionid
$foruser = new \stdClass; // object | foruser

try {
    $apiInstance->uploadsummaryUsingPOST($file, $sessionid, $foruser);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->uploadsummaryUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **\SplFileObject**| file |
 **sessionid** | [**object**](../Model/.md)| sessionid |
 **foruser** | [**object**](../Model/.md)| foruser | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **zurueckholenUsingPOST**
> \Insign\Model\BasicResult zurueckholenUsingPOST($client_info, $oldsessionid, $sessionid)

Return session from external state

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$client_info = new \Insign\Model\ClientInfo(); // \Insign\Model\ClientInfo | clientInfo
$oldsessionid = "oldsessionid_example"; // string | oldsessionid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->zurueckholenUsingPOST($client_info, $oldsessionid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->zurueckholenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_info** | [**\Insign\Model\ClientInfo**](../Model/ClientInfo.md)| clientInfo |
 **oldsessionid** | **string**| oldsessionid |
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **zurueckholenUsingPUT**
> \Insign\Model\BasicResult zurueckholenUsingPUT($client_info, $oldsessionid, $sessionid)

Return session from external state

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\AllPublicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$client_info = new \Insign\Model\ClientInfo(); // \Insign\Model\ClientInfo | clientInfo
$oldsessionid = "oldsessionid_example"; // string | oldsessionid
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->zurueckholenUsingPUT($client_info, $oldsessionid, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllPublicApi->zurueckholenUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_info** | [**\Insign\Model\ClientInfo**](../Model/ClientInfo.md)| clientInfo |
 **oldsessionid** | **string**| oldsessionid |
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

