# Insign\InternalApi

All URIs are relative to *http://is2-cloud.test.flexperto.com/is2-horn-8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activateWifiDirectUsingGET**](InternalApi.md#activateWifiDirectUsingGET) | **GET** /configure/activateWifiDirect | Activate Wifi-Direct connection
[**activateWifiDirectUsingPOST**](InternalApi.md#activateWifiDirectUsingPOST) | **POST** /configure/activateWifiDirect | Activate Wifi-Direct connection
[**bestaetigenUsingGET**](InternalApi.md#bestaetigenUsingGET) | **GET** /aushaendigung/bestaetigen | Confirm receipt of documents
[**bestaetigenUsingPOST**](InternalApi.md#bestaetigenUsingPOST) | **POST** /aushaendigung/bestaetigen | Confirm receipt of documents
[**checkTokenUsingPOST**](InternalApi.md#checkTokenUsingPOST) | **POST** /rest/mobilepairing/checktoken | Check existing pairing token
[**closesessionUsingGET**](InternalApi.md#closesessionUsingGET) | **GET** /configure/closesession | Abort session and return to callback url
[**closesessionUsingPOST**](InternalApi.md#closesessionUsingPOST) | **POST** /configure/closesession | Abort session and return to callback url
[**createTokenRequestApiUserUsingPOST**](InternalApi.md#createTokenRequestApiUserUsingPOST) | **POST** /configure/createSSOForApiuser | Create JWT Token for API-User so that he can access internal start module
[**createTokenRequestUsingPOST**](InternalApi.md#createTokenRequestUsingPOST) | **POST** /configure/createSSO | Create OTP token for internal start module
[**deletePageUsingGET**](InternalApi.md#deletePageUsingGET) | **GET** /configure/deletePage | Remove a page from a document
[**deletePageUsingPOST**](InternalApi.md#deletePageUsingPOST) | **POST** /configure/deletePage | Remove a page from a document
[**deletesessionUsingGET**](InternalApi.md#deletesessionUsingGET) | **GET** /configure/deletesession | No Operation
[**deletesessionUsingPOST**](InternalApi.md#deletesessionUsingPOST) | **POST** /configure/deletesession | No Operation
[**dummyUsingGET**](InternalApi.md#dummyUsingGET) | **GET** /extern/delegate/finished | Confirm receipt of documents
[**dummyUsingPOST**](InternalApi.md#dummyUsingPOST) | **POST** /extern/delegate/finished | Confirm receipt of documents
[**expandlinkUsingGET**](InternalApi.md#expandlinkUsingGET) | **GET** /s/{id} | Shortened link expansion via redirect to full url
[**expandlinkUsingPOST**](InternalApi.md#expandlinkUsingPOST) | **POST** /s/{id} | Shortened link expansion via redirect to full url
[**getExternInfosUsingGET**](InternalApi.md#getExternInfosUsingGET) | **GET** /get/externInfos | Additional status information about session that is in external state
[**getExternInfosUsingPOST**](InternalApi.md#getExternInfosUsingPOST) | **POST** /get/externInfos | Additional status information about session that is in external state
[**getFullDocumentDataByIdsUsingGET**](InternalApi.md#getFullDocumentDataByIdsUsingGET) | **GET** /get/documents/by/ids | Retrieve metadata about one sessions document and annotations
[**getFullDocumentDataByIdsUsingPOST**](InternalApi.md#getFullDocumentDataByIdsUsingPOST) | **POST** /get/documents/by/ids | Retrieve metadata about one sessions document and annotations
[**getFullDocumentDataUsingGET**](InternalApi.md#getFullDocumentDataUsingGET) | **GET** /get/documents/full | Retrieve metadata about all sessions documents and annotations
[**getFullDocumentDataUsingPOST**](InternalApi.md#getFullDocumentDataUsingPOST) | **POST** /get/documents/full | Retrieve metadata about all sessions documents and annotations
[**getMailDataUsingPOST**](InternalApi.md#getMailDataUsingPOST) | **POST** /mail/get/data | Retrieve information about E-Mails and render Mail templates for client UI
[**getPairedDeviceUsingPOST**](InternalApi.md#getPairedDeviceUsingPOST) | **POST** /rest/mobilepairing/getpaireddevice | Retrieve information about paired signature devices
[**getPairingCodeInfoUsingPOST**](InternalApi.md#getPairingCodeInfoUsingPOST) | **POST** /rest/mobilepairing/getPairingCodeInfo | Request info about pairing code
[**getSessionsUsingGET2**](InternalApi.md#getSessionsUsingGET2) | **GET** /get/sessions | List of session
[**getSessionsUsingPOST2**](InternalApi.md#getSessionsUsingPOST2) | **POST** /get/sessions | List of session
[**getSignatureImageUsingGET**](InternalApi.md#getSignatureImageUsingGET) | **GET** /getimage/signature | Get a signatures bitmap as PNG image.
[**getWifiDirectInfoUsingGET**](InternalApi.md#getWifiDirectInfoUsingGET) | **GET** /configure/wifiDirectInfo | Wifi-Direct connection status
[**getWifiDirectInfoUsingPOST**](InternalApi.md#getWifiDirectInfoUsingPOST) | **POST** /configure/wifiDirectInfo | Wifi-Direct connection status
[**getpairedmobilesUsingGET**](InternalApi.md#getpairedmobilesUsingGET) | **GET** /rest/mobilepairing/getpairedmobiles | Retrieve infomation about paired signature devices
[**getpairedmobilesUsingPOST**](InternalApi.md#getpairedmobilesUsingPOST) | **POST** /rest/mobilepairing/getpairedmobiles | Retrieve infomation about paired signature devices
[**getpropertiesUsingGET**](InternalApi.md#getpropertiesUsingGET) | **GET** /configure/getproperties | Get a list of possible UI-properties
[**getpropertiesUsingPOST**](InternalApi.md#getpropertiesUsingPOST) | **POST** /configure/getproperties | Get a list of possible UI-properties
[**heartbeatUsingGET**](InternalApi.md#heartbeatUsingGET) | **GET** /heartbeat | Free sessions resources on server
[**heartbeatUsingPOST**](InternalApi.md#heartbeatUsingPOST) | **POST** /heartbeat | Free sessions resources on server
[**infoUsingDELETE**](InternalApi.md#infoUsingDELETE) | **DELETE** /dualscreen/info | Get information about clients
[**infoUsingGET**](InternalApi.md#infoUsingGET) | **GET** /dualscreen/info | Get information about clients
[**infoUsingHEAD**](InternalApi.md#infoUsingHEAD) | **HEAD** /dualscreen/info | Get information about clients
[**infoUsingOPTIONS**](InternalApi.md#infoUsingOPTIONS) | **OPTIONS** /dualscreen/info | Get information about clients
[**infoUsingPATCH**](InternalApi.md#infoUsingPATCH) | **PATCH** /dualscreen/info | Get information about clients
[**infoUsingPOST**](InternalApi.md#infoUsingPOST) | **POST** /dualscreen/info | Get information about clients
[**infoUsingPUT**](InternalApi.md#infoUsingPUT) | **PUT** /dualscreen/info | Get information about clients
[**initialCheckUsingGET**](InternalApi.md#initialCheckUsingGET) | **GET** /configure/initialCheck | Session sanity check
[**initialCheckUsingPOST**](InternalApi.md#initialCheckUsingPOST) | **POST** /configure/initialCheck | Session sanity check
[**needsPairingUsingPOST**](InternalApi.md#needsPairingUsingPOST) | **POST** /rest/mobilepairing/needspairing | Retrieve information about pairing needed
[**pairMobileUsingPOST**](InternalApi.md#pairMobileUsingPOST) | **POST** /rest/mobilepairing/pairmobile | Pair device
[**pushURLUsingDELETE**](InternalApi.md#pushURLUsingDELETE) | **DELETE** /dualscreen/pushcustomurl | Send custom URL to client an wake him up
[**pushURLUsingGET**](InternalApi.md#pushURLUsingGET) | **GET** /dualscreen/pushcustomurl | Send custom URL to client an wake him up
[**pushURLUsingHEAD**](InternalApi.md#pushURLUsingHEAD) | **HEAD** /dualscreen/pushcustomurl | Send custom URL to client an wake him up
[**pushURLUsingOPTIONS**](InternalApi.md#pushURLUsingOPTIONS) | **OPTIONS** /dualscreen/pushcustomurl | Send custom URL to client an wake him up
[**pushURLUsingPATCH**](InternalApi.md#pushURLUsingPATCH) | **PATCH** /dualscreen/pushcustomurl | Send custom URL to client an wake him up
[**pushURLUsingPOST**](InternalApi.md#pushURLUsingPOST) | **POST** /dualscreen/pushcustomurl | Send custom URL to client an wake him up
[**pushURLUsingPUT**](InternalApi.md#pushURLUsingPUT) | **PUT** /dualscreen/pushcustomurl | Send custom URL to client an wake him up
[**pusheventUsingPOST**](InternalApi.md#pusheventUsingPOST) | **POST** /dualscreen/event | Send event to client
[**pusheventUsingPUT**](InternalApi.md#pusheventUsingPUT) | **PUT** /dualscreen/event | Send event to client
[**redirectToVorgangsverwaltungUsingGET**](InternalApi.md#redirectToVorgangsverwaltungUsingGET) | **GET** /configure/fertigVorgangsverwaltung | Abort session and redirect to vorgangsverwaltung
[**redirectToVorgangsverwaltungUsingPOST**](InternalApi.md#redirectToVorgangsverwaltungUsingPOST) | **POST** /configure/fertigVorgangsverwaltung | Abort session and redirect to vorgangsverwaltung
[**redirectonfailureUsingGET**](InternalApi.md#redirectonfailureUsingGET) | **GET** /configure/failureredirect | Callback redirect on failure
[**redirectonfailureUsingPOST**](InternalApi.md#redirectonfailureUsingPOST) | **POST** /configure/failureredirect | Callback redirect on failure
[**registerUsingDELETE**](InternalApi.md#registerUsingDELETE) | **DELETE** /dualscreen/log | Push client log to server
[**registerUsingDELETE1**](InternalApi.md#registerUsingDELETE1) | **DELETE** /dualscreen/pull | Poll for new dualscreen client information
[**registerUsingGET**](InternalApi.md#registerUsingGET) | **GET** /dualscreen/log | Push client log to server
[**registerUsingGET1**](InternalApi.md#registerUsingGET1) | **GET** /dualscreen/pull | Poll for new dualscreen client information
[**registerUsingHEAD**](InternalApi.md#registerUsingHEAD) | **HEAD** /dualscreen/log | Push client log to server
[**registerUsingHEAD1**](InternalApi.md#registerUsingHEAD1) | **HEAD** /dualscreen/pull | Poll for new dualscreen client information
[**registerUsingOPTIONS**](InternalApi.md#registerUsingOPTIONS) | **OPTIONS** /dualscreen/log | Push client log to server
[**registerUsingOPTIONS1**](InternalApi.md#registerUsingOPTIONS1) | **OPTIONS** /dualscreen/pull | Poll for new dualscreen client information
[**registerUsingPATCH**](InternalApi.md#registerUsingPATCH) | **PATCH** /dualscreen/log | Push client log to server
[**registerUsingPATCH1**](InternalApi.md#registerUsingPATCH1) | **PATCH** /dualscreen/pull | Poll for new dualscreen client information
[**registerUsingPOST**](InternalApi.md#registerUsingPOST) | **POST** /dualscreen/log | Push client log to server
[**registerUsingPOST1**](InternalApi.md#registerUsingPOST1) | **POST** /dualscreen/pull | Poll for new dualscreen client information
[**registerUsingPUT**](InternalApi.md#registerUsingPUT) | **PUT** /dualscreen/log | Push client log to server
[**registerUsingPUT1**](InternalApi.md#registerUsingPUT1) | **PUT** /dualscreen/pull | Poll for new dualscreen client information
[**requestPairingCodeUsingPOST**](InternalApi.md#requestPairingCodeUsingPOST) | **POST** /rest/mobilepairing/requestcode | Request new pairing code
[**resendApplosPermanentLinkUsingPOST**](InternalApi.md#resendApplosPermanentLinkUsingPOST) | **POST** /applos/resendApplosPermanentLink | Resend applos permanent link
[**resetAusgehaendigtOriginalUsingGET**](InternalApi.md#resetAusgehaendigtOriginalUsingGET) | **GET** /get/resetAusgehaendigt | Set ausgehaendigtOriginal to false
[**resetAusgehaendigtOriginalUsingPOST**](InternalApi.md#resetAusgehaendigtOriginalUsingPOST) | **POST** /get/resetAusgehaendigt | Set ausgehaendigtOriginal to false
[**resetAusgehaendigtOriginalUsingPUT**](InternalApi.md#resetAusgehaendigtOriginalUsingPUT) | **PUT** /get/resetAusgehaendigt | Set ausgehaendigtOriginal to false
[**sendAllDocumentsUsingPOST**](InternalApi.md#sendAllDocumentsUsingPOST) | **POST** /mail/send | Send out E-Mails
[**sendApplosLinkUsingPOST**](InternalApi.md#sendApplosLinkUsingPOST) | **POST** /applos/send | Send out link for app-less signature
[**sessionabbrechenUsingGET**](InternalApi.md#sessionabbrechenUsingGET) | **GET** /configure/fertig | Abort session and return to callback url
[**sessionabbrechenUsingPOST**](InternalApi.md#sessionabbrechenUsingPOST) | **POST** /configure/fertig | Abort session and return to callback url
[**sessionbeendenUsingGET**](InternalApi.md#sessionbeendenUsingGET) | **GET** /configure/beenden | Finish session and return to callback url
[**sessionbeendenUsingPOST**](InternalApi.md#sessionbeendenUsingPOST) | **POST** /configure/beenden | Finish session and return to callback url
[**setDisplayUsingPOST**](InternalApi.md#setDisplayUsingPOST) | **POST** /get/setDisplay | Set displayname and customer name of a session
[**setDisplayUsingPUT**](InternalApi.md#setDisplayUsingPUT) | **PUT** /get/setDisplay | Set displayname and customer name of a session
[**setSessionGUIPropertyUsingGET**](InternalApi.md#setSessionGUIPropertyUsingGET) | **GET** /configure/updateGUIProperty | Modify a session UI property
[**setSessionGUIPropertyUsingPOST**](InternalApi.md#setSessionGUIPropertyUsingPOST) | **POST** /configure/updateGUIProperties | Modify a list of session UI properties
[**setSessionGUIPropertyUsingPOST1**](InternalApi.md#setSessionGUIPropertyUsingPOST1) | **POST** /configure/updateGUIProperty | Modify a session UI property
[**setSessionPropertyUsingGET**](InternalApi.md#setSessionPropertyUsingGET) | **GET** /configure/updateSessionConfigProperty | Modify a session mutable property
[**setSessionPropertyUsingPOST**](InternalApi.md#setSessionPropertyUsingPOST) | **POST** /configure/updateSessionConfigProperty | Modify a session mutable property
[**setSessionVarUsingPOST**](InternalApi.md#setSessionVarUsingPOST) | **POST** /configure/setSessionVar | Set Session Var
[**setWifiDirectIDUsingPOST**](InternalApi.md#setWifiDirectIDUsingPOST) | **POST** /rest/mobilepairing/setWifiID | Set Wifi-Direct-ID for pairingcode
[**storeMultipleDocumentsUsingPOST**](InternalApi.md#storeMultipleDocumentsUsingPOST) | **POST** /rest/store/documents | Store documents data (signatures, form fields)
[**storeSignatureUsingPOST**](InternalApi.md#storeSignatureUsingPOST) | **POST** /rest/store/signature | Store single signature
[**terminateWifiDirectUsingGET**](InternalApi.md#terminateWifiDirectUsingGET) | **GET** /configure/terminateWifiDirect | terminate Wifi-Direct connection
[**terminateWifiDirectUsingPOST**](InternalApi.md#terminateWifiDirectUsingPOST) | **POST** /configure/terminateWifiDirect | terminate Wifi-Direct connection
[**unpairCustomerUsingGET**](InternalApi.md#unpairCustomerUsingGET) | **GET** /rest/mobilepairing/unpairallCustomer | Unpair all additional (customer) devices
[**unpairCustomerUsingPOST**](InternalApi.md#unpairCustomerUsingPOST) | **POST** /rest/mobilepairing/unpairallCustomer | Unpair all additional (customer) devices
[**unpairUserUsingGET**](InternalApi.md#unpairUserUsingGET) | **GET** /rest/mobilepairing/unpairallUser | Unpair all additional user devices
[**unpairUserUsingPOST**](InternalApi.md#unpairUserUsingPOST) | **POST** /rest/mobilepairing/unpairallUser | Unpair all additional user devices
[**unpairUsingGET1**](InternalApi.md#unpairUsingGET1) | **GET** /rest/mobilepairing/unpairall | Unpair all paired users signature devices
[**unpairUsingPOST**](InternalApi.md#unpairUsingPOST) | **POST** /rest/mobilepairing/unpair | Unpair signature device
[**unpairUsingPOST1**](InternalApi.md#unpairUsingPOST1) | **POST** /rest/mobilepairing/unpairall | Unpair all paired users signature devices
[**unpairallforuserUsingGET**](InternalApi.md#unpairallforuserUsingGET) | **GET** /rest/mobilepairing/unpairallforuser | Unpair all paired users signature devices
[**unpairallforuserUsingPOST**](InternalApi.md#unpairallforuserUsingPOST) | **POST** /rest/mobilepairing/unpairallforuser | Unpair all paired users signature devices
[**uploadphotoApplosUsingPOST**](InternalApi.md#uploadphotoApplosUsingPOST) | **POST** /rest/store/photoApplos | Upload photo
[**uploadphotostream2UsingPOST1**](InternalApi.md#uploadphotostream2UsingPOST1) | **POST** /rest/store/photo | Upload photo
[**waitForNotificationUsingPOST1**](InternalApi.md#waitForNotificationUsingPOST1) | **POST** /waitForNotification | Multi device messaging. Long term polling
[**whoIsPairingMobileUsingPOST**](InternalApi.md#whoIsPairingMobileUsingPOST) | **POST** /rest/mobilepairing/whoispairingmobile | Determine type of pairing code


# **activateWifiDirectUsingGET**
> \Insign\Model\BasicResult activateWifiDirectUsingGET($activate)

Activate Wifi-Direct connection

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$activate = true; // bool | activate

try {
    $result = $apiInstance->activateWifiDirectUsingGET($activate);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->activateWifiDirectUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activate** | **bool**| activate |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **activateWifiDirectUsingPOST**
> \Insign\Model\BasicResult activateWifiDirectUsingPOST($activate)

Activate Wifi-Direct connection

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$activate = true; // bool | activate

try {
    $result = $apiInstance->activateWifiDirectUsingPOST($activate);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->activateWifiDirectUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activate** | **bool**| activate |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **bestaetigenUsingGET**
> string bestaetigenUsingGET($token)

Confirm receipt of documents

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$token = "token_example"; // string | token

try {
    $result = $apiInstance->bestaetigenUsingGET($token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->bestaetigenUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **string**| token |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **bestaetigenUsingPOST**
> string bestaetigenUsingPOST($token)

Confirm receipt of documents

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$token = "token_example"; // string | token

try {
    $result = $apiInstance->bestaetigenUsingPOST($token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->bestaetigenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **string**| token |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **checkTokenUsingPOST**
> \Insign\Model\BasicResult checkTokenUsingPOST($input)

Check existing pairing token

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$input = new \Insign\Model\CheckTokenInput(); // \Insign\Model\CheckTokenInput | input

try {
    $result = $apiInstance->checkTokenUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->checkTokenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\CheckTokenInput**](../Model/CheckTokenInput.md)| input |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **closesessionUsingGET**
> \Insign\Model\RedirectView closesessionUsingGET($sessionid)

Abort session and return to callback url

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->closesessionUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->closesessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **closesessionUsingPOST**
> \Insign\Model\RedirectView closesessionUsingPOST($sessionid)

Abort session and return to callback url

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->closesessionUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->closesessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createTokenRequestApiUserUsingPOST**
> string createTokenRequestApiUserUsingPOST($input)

Create JWT Token for API-User so that he can access internal start module

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\UserInfo(); // \Insign\Model\UserInfo | input

try {
    $result = $apiInstance->createTokenRequestApiUserUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->createTokenRequestApiUserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\UserInfo**](../Model/UserInfo.md)| input |

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createTokenRequestUsingPOST**
> string createTokenRequestUsingPOST($input)

Create OTP token for internal start module

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\UserInfo(); // \Insign\Model\UserInfo | input

try {
    $result = $apiInstance->createTokenRequestUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->createTokenRequestUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\UserInfo**](../Model/UserInfo.md)| input |

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePageUsingGET**
> \Insign\Model\BasicResult deletePageUsingGET($sessionid, $docid, $page)

Remove a page from a document

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$docid = "docid_example"; // string | docid
$page = 56; // int | page

try {
    $result = $apiInstance->deletePageUsingGET($sessionid, $docid, $page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->deletePageUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **docid** | **string**| docid | [optional]
 **page** | **int**| page | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePageUsingPOST**
> \Insign\Model\BasicResult deletePageUsingPOST($sessionid, $docid, $page)

Remove a page from a document

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$docid = "docid_example"; // string | docid
$page = 56; // int | page

try {
    $result = $apiInstance->deletePageUsingPOST($sessionid, $docid, $page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->deletePageUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **docid** | **string**| docid | [optional]
 **page** | **int**| page | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletesessionUsingGET**
> deletesessionUsingGET($sessionid)

No Operation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $apiInstance->deletesessionUsingGET($sessionid);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->deletesessionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletesessionUsingPOST**
> deletesessionUsingPOST($sessionid)

No Operation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $apiInstance->deletesessionUsingPOST($sessionid);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->deletesessionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dummyUsingGET**
> string dummyUsingGET($model)

Confirm receipt of documents

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$model = new \stdClass; // object | model

try {
    $result = $apiInstance->dummyUsingGET($model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->dummyUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**object**](../Model/.md)| model | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dummyUsingPOST**
> string dummyUsingPOST($model)

Confirm receipt of documents

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$model = new \stdClass; // object | model

try {
    $result = $apiInstance->dummyUsingPOST($model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->dummyUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**object**](../Model/.md)| model | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **expandlinkUsingGET**
> \Insign\Model\RedirectView expandlinkUsingGET($id)

Shortened link expansion via redirect to full url

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | id

try {
    $result = $apiInstance->expandlinkUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->expandlinkUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| id |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **expandlinkUsingPOST**
> \Insign\Model\RedirectView expandlinkUsingPOST($id)

Shortened link expansion via redirect to full url

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | id

try {
    $result = $apiInstance->expandlinkUsingPOST($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->expandlinkUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| id |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExternInfosUsingGET**
> \Insign\Model\ExternUserInfo[] getExternInfosUsingGET($sessionid, $id_vorgangsverwaltung)

Additional status information about session that is in external state

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getExternInfosUsingGET($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getExternInfosUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternUserInfo[]**](../Model/ExternUserInfo.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExternInfosUsingPOST**
> \Insign\Model\ExternUserInfo[] getExternInfosUsingPOST($sessionid, $id_vorgangsverwaltung)

Additional status information about session that is in external state

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->getExternInfosUsingPOST($sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getExternInfosUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\ExternUserInfo[]**](../Model/ExternUserInfo.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFullDocumentDataByIdsUsingGET**
> \Insign\Model\SessionData getFullDocumentDataByIdsUsingGET($ids, $sessionid, $id_vorgangsverwaltung, $include_annotations)

Retrieve metadata about one sessions document and annotations

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ids = "ids_example"; // string | ids
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$include_annotations = false; // bool | includeAnnotations

try {
    $result = $apiInstance->getFullDocumentDataByIdsUsingGET($ids, $sessionid, $id_vorgangsverwaltung, $include_annotations);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getFullDocumentDataByIdsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | **string**| ids |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **include_annotations** | **bool**| includeAnnotations | [optional] [default to false]

### Return type

[**\Insign\Model\SessionData**](../Model/SessionData.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFullDocumentDataByIdsUsingPOST**
> \Insign\Model\SessionData getFullDocumentDataByIdsUsingPOST($ids, $sessionid, $id_vorgangsverwaltung, $include_annotations)

Retrieve metadata about one sessions document and annotations

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ids = "ids_example"; // string | ids
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$include_annotations = false; // bool | includeAnnotations

try {
    $result = $apiInstance->getFullDocumentDataByIdsUsingPOST($ids, $sessionid, $id_vorgangsverwaltung, $include_annotations);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getFullDocumentDataByIdsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | **string**| ids |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **include_annotations** | **bool**| includeAnnotations | [optional] [default to false]

### Return type

[**\Insign\Model\SessionData**](../Model/SessionData.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFullDocumentDataUsingGET**
> \Insign\Model\SessionData getFullDocumentDataUsingGET($sessionid, $id_vorgangsverwaltung, $include_annotations)

Retrieve metadata about all sessions documents and annotations

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$include_annotations = false; // bool | includeAnnotations

try {
    $result = $apiInstance->getFullDocumentDataUsingGET($sessionid, $id_vorgangsverwaltung, $include_annotations);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getFullDocumentDataUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **include_annotations** | **bool**| includeAnnotations | [optional] [default to false]

### Return type

[**\Insign\Model\SessionData**](../Model/SessionData.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFullDocumentDataUsingPOST**
> \Insign\Model\SessionData getFullDocumentDataUsingPOST($sessionid, $id_vorgangsverwaltung, $include_annotations)

Retrieve metadata about all sessions documents and annotations

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$include_annotations = false; // bool | includeAnnotations

try {
    $result = $apiInstance->getFullDocumentDataUsingPOST($sessionid, $id_vorgangsverwaltung, $include_annotations);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getFullDocumentDataUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **include_annotations** | **bool**| includeAnnotations | [optional] [default to false]

### Return type

[**\Insign\Model\SessionData**](../Model/SessionData.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMailDataUsingPOST**
> \Insign\Model\MailDataMultiResult getMailDataUsingPOST($sessionid, $iso3_country, $iso3_language, $country, $display_country, $display_language, $display_name, $display_script, $display_variant, $extern, $fertig, $id_vorgangsverwaltung, $include_text, $input, $language, $language2, $multi_extern, $mustberead, $script, $unicode_locale_attributes, $unicode_locale_keys, $variant)

Retrieve information about E-Mails and render Mail templates for client UI

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$iso3_country = "iso3_country_example"; // string | 
$iso3_language = "iso3_language_example"; // string | 
$country = "country_example"; // string | 
$display_country = "display_country_example"; // string | 
$display_language = "display_language_example"; // string | 
$display_name = "display_name_example"; // string | 
$display_script = "display_script_example"; // string | 
$display_variant = "display_variant_example"; // string | 
$extern = true; // bool | extern
$fertig = true; // bool | fertig
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung
$include_text = true; // bool | includeText
$input = new \Insign\Model\SendDocuments(); // \Insign\Model\SendDocuments | input
$language = "language_example"; // string | language
$language2 = "language_example"; // string | 
$multi_extern = true; // bool | multiExtern
$mustberead = true; // bool | mustberead
$script = "script_example"; // string | 
$unicode_locale_attributes = array("unicode_locale_attributes_example"); // string[] | 
$unicode_locale_keys = array("unicode_locale_keys_example"); // string[] | 
$variant = "variant_example"; // string | 

try {
    $result = $apiInstance->getMailDataUsingPOST($sessionid, $iso3_country, $iso3_language, $country, $display_country, $display_language, $display_name, $display_script, $display_variant, $extern, $fertig, $id_vorgangsverwaltung, $include_text, $input, $language, $language2, $multi_extern, $mustberead, $script, $unicode_locale_attributes, $unicode_locale_keys, $variant);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getMailDataUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **iso3_country** | **string**|  | [optional]
 **iso3_language** | **string**|  | [optional]
 **country** | **string**|  | [optional]
 **display_country** | **string**|  | [optional]
 **display_language** | **string**|  | [optional]
 **display_name** | **string**|  | [optional]
 **display_script** | **string**|  | [optional]
 **display_variant** | **string**|  | [optional]
 **extern** | **bool**| extern | [optional]
 **fertig** | **bool**| fertig | [optional]
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]
 **include_text** | **bool**| includeText | [optional]
 **input** | [**\Insign\Model\SendDocuments**](../Model/SendDocuments.md)| input | [optional]
 **language** | **string**| language | [optional]
 **language2** | **string**|  | [optional]
 **multi_extern** | **bool**| multiExtern | [optional]
 **mustberead** | **bool**| mustberead | [optional]
 **script** | **string**|  | [optional]
 **unicode_locale_attributes** | [**string[]**](../Model/string.md)|  | [optional]
 **unicode_locale_keys** | [**string[]**](../Model/string.md)|  | [optional]
 **variant** | **string**|  | [optional]

### Return type

[**\Insign\Model\MailDataMultiResult**](../Model/MailDataMultiResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPairedDeviceUsingPOST**
> \Insign\Model\PairedDevices getPairedDeviceUsingPOST($input)

Retrieve information about paired signature devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\PairingInput(); // \Insign\Model\PairingInput | input

try {
    $result = $apiInstance->getPairedDeviceUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getPairedDeviceUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\PairingInput**](../Model/PairingInput.md)| input |

### Return type

[**\Insign\Model\PairedDevices**](../Model/PairedDevices.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPairingCodeInfoUsingPOST**
> \Insign\Model\RequestPairingCodeResult getPairingCodeInfoUsingPOST($input)

Request info about pairing code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\PairingInput(); // \Insign\Model\PairingInput | input

try {
    $result = $apiInstance->getPairingCodeInfoUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getPairingCodeInfoUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\PairingInput**](../Model/PairingInput.md)| input |

### Return type

[**\Insign\Model\RequestPairingCodeResult**](../Model/RequestPairingCodeResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingGET2**
> \Insign\Model\SessionsResult getSessionsUsingGET2($sessionid)

List of session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getSessionsUsingGET2($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getSessionsUsingGET2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSessionsUsingPOST2**
> \Insign\Model\SessionsResult getSessionsUsingPOST2($sessionid)

List of session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getSessionsUsingPOST2($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getSessionsUsingPOST2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\SessionsResult**](../Model/SessionsResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSignatureImageUsingGET**
> getSignatureImageUsingGET($docid, $sessionid, $signid, $height, $width)

Get a signatures bitmap as PNG image.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$docid = "docid_example"; // string | docid
$sessionid = "sessionid_example"; // string | sessionid
$signid = "signid_example"; // string | signid
$height = 3.4; // float | height
$width = 3.4; // float | width

try {
    $apiInstance->getSignatureImageUsingGET($docid, $sessionid, $signid, $height, $width);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getSignatureImageUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **docid** | **string**| docid |
 **sessionid** | **string**| sessionid |
 **signid** | **string**| signid |
 **height** | **float**| height | [optional]
 **width** | **float**| width | [optional]

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/png

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getWifiDirectInfoUsingGET**
> \Insign\Model\WifiDirectInfo getWifiDirectInfoUsingGET()

Wifi-Direct connection status

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getWifiDirectInfoUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getWifiDirectInfoUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Insign\Model\WifiDirectInfo**](../Model/WifiDirectInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getWifiDirectInfoUsingPOST**
> \Insign\Model\WifiDirectInfo getWifiDirectInfoUsingPOST()

Wifi-Direct connection status

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getWifiDirectInfoUsingPOST();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getWifiDirectInfoUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Insign\Model\WifiDirectInfo**](../Model/WifiDirectInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getpairedmobilesUsingGET**
> \Insign\Model\PairedMobilesResult getpairedmobilesUsingGET($sessionid)

Retrieve infomation about paired signature devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getpairedmobilesUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getpairedmobilesUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\PairedMobilesResult**](../Model/PairedMobilesResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getpairedmobilesUsingPOST**
> \Insign\Model\PairedMobilesResult getpairedmobilesUsingPOST($sessionid)

Retrieve infomation about paired signature devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->getpairedmobilesUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getpairedmobilesUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\PairedMobilesResult**](../Model/PairedMobilesResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getpropertiesUsingGET**
> \Insign\Model\GUIPorpertiesList getpropertiesUsingGET()

Get a list of possible UI-properties

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getpropertiesUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getpropertiesUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Insign\Model\GUIPorpertiesList**](../Model/GUIPorpertiesList.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getpropertiesUsingPOST**
> \Insign\Model\GUIPorpertiesList getpropertiesUsingPOST()

Get a list of possible UI-properties

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getpropertiesUsingPOST();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->getpropertiesUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Insign\Model\GUIPorpertiesList**](../Model/GUIPorpertiesList.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **heartbeatUsingGET**
> \Insign\Model\BasicResult heartbeatUsingGET($sessionid)

Free sessions resources on server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->heartbeatUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->heartbeatUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **heartbeatUsingPOST**
> \Insign\Model\BasicResult heartbeatUsingPOST($sessionid)

Free sessions resources on server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->heartbeatUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->heartbeatUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **infoUsingDELETE**
> map[string,\Insign\Model\ClientState] infoUsingDELETE()

Get information about clients

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->infoUsingDELETE();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->infoUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**map[string,\Insign\Model\ClientState]**](../Model/ClientState.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **infoUsingGET**
> map[string,\Insign\Model\ClientState] infoUsingGET()

Get information about clients

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->infoUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->infoUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**map[string,\Insign\Model\ClientState]**](../Model/ClientState.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **infoUsingHEAD**
> map[string,\Insign\Model\ClientState] infoUsingHEAD()

Get information about clients

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->infoUsingHEAD();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->infoUsingHEAD: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**map[string,\Insign\Model\ClientState]**](../Model/ClientState.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **infoUsingOPTIONS**
> map[string,\Insign\Model\ClientState] infoUsingOPTIONS()

Get information about clients

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->infoUsingOPTIONS();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->infoUsingOPTIONS: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**map[string,\Insign\Model\ClientState]**](../Model/ClientState.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **infoUsingPATCH**
> map[string,\Insign\Model\ClientState] infoUsingPATCH()

Get information about clients

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->infoUsingPATCH();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->infoUsingPATCH: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**map[string,\Insign\Model\ClientState]**](../Model/ClientState.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **infoUsingPOST**
> map[string,\Insign\Model\ClientState] infoUsingPOST()

Get information about clients

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->infoUsingPOST();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->infoUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**map[string,\Insign\Model\ClientState]**](../Model/ClientState.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **infoUsingPUT**
> map[string,\Insign\Model\ClientState] infoUsingPUT()

Get information about clients

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->infoUsingPUT();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->infoUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**map[string,\Insign\Model\ClientState]**](../Model/ClientState.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **initialCheckUsingGET**
> \Insign\Model\InitialCheckResult initialCheckUsingGET($sessionid)

Session sanity check

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->initialCheckUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->initialCheckUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\InitialCheckResult**](../Model/InitialCheckResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **initialCheckUsingPOST**
> \Insign\Model\InitialCheckResult initialCheckUsingPOST($sessionid)

Session sanity check

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->initialCheckUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->initialCheckUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\InitialCheckResult**](../Model/InitialCheckResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **needsPairingUsingPOST**
> \Insign\Model\NeedsPairingResult needsPairingUsingPOST($input)

Retrieve information about pairing needed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\PairingInput(); // \Insign\Model\PairingInput | input

try {
    $result = $apiInstance->needsPairingUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->needsPairingUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\PairingInput**](../Model/PairingInput.md)| input |

### Return type

[**\Insign\Model\NeedsPairingResult**](../Model/NeedsPairingResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pairMobileUsingPOST**
> \Insign\Model\MobilePairingResult pairMobileUsingPOST($input)

Pair device

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$input = new \Insign\Model\MobilePairingInput(); // \Insign\Model\MobilePairingInput | input

try {
    $result = $apiInstance->pairMobileUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->pairMobileUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\MobilePairingInput**](../Model/MobilePairingInput.md)| input |

### Return type

[**\Insign\Model\MobilePairingResult**](../Model/MobilePairingResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pushURLUsingDELETE**
> pushURLUsingDELETE($client_id, $url)

Send custom URL to client an wake him up

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$client_id = "client_id_example"; // string | clientId
$url = "url_example"; // string | url

try {
    $apiInstance->pushURLUsingDELETE($client_id, $url);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->pushURLUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **string**| clientId |
 **url** | **string**| url |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pushURLUsingGET**
> pushURLUsingGET($client_id, $url)

Send custom URL to client an wake him up

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$client_id = "client_id_example"; // string | clientId
$url = "url_example"; // string | url

try {
    $apiInstance->pushURLUsingGET($client_id, $url);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->pushURLUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **string**| clientId |
 **url** | **string**| url |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pushURLUsingHEAD**
> pushURLUsingHEAD($client_id, $url)

Send custom URL to client an wake him up

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$client_id = "client_id_example"; // string | clientId
$url = "url_example"; // string | url

try {
    $apiInstance->pushURLUsingHEAD($client_id, $url);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->pushURLUsingHEAD: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **string**| clientId |
 **url** | **string**| url |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pushURLUsingOPTIONS**
> pushURLUsingOPTIONS($client_id, $url)

Send custom URL to client an wake him up

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$client_id = "client_id_example"; // string | clientId
$url = "url_example"; // string | url

try {
    $apiInstance->pushURLUsingOPTIONS($client_id, $url);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->pushURLUsingOPTIONS: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **string**| clientId |
 **url** | **string**| url |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pushURLUsingPATCH**
> pushURLUsingPATCH($client_id, $url)

Send custom URL to client an wake him up

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$client_id = "client_id_example"; // string | clientId
$url = "url_example"; // string | url

try {
    $apiInstance->pushURLUsingPATCH($client_id, $url);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->pushURLUsingPATCH: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **string**| clientId |
 **url** | **string**| url |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pushURLUsingPOST**
> pushURLUsingPOST($client_id, $url)

Send custom URL to client an wake him up

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$client_id = "client_id_example"; // string | clientId
$url = "url_example"; // string | url

try {
    $apiInstance->pushURLUsingPOST($client_id, $url);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->pushURLUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **string**| clientId |
 **url** | **string**| url |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pushURLUsingPUT**
> pushURLUsingPUT($client_id, $url)

Send custom URL to client an wake him up

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$client_id = "client_id_example"; // string | clientId
$url = "url_example"; // string | url

try {
    $apiInstance->pushURLUsingPUT($client_id, $url);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->pushURLUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **string**| clientId |
 **url** | **string**| url |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pusheventUsingPOST**
> pusheventUsingPOST($data, $sessionid, $type)

Send event to client

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$data = new \stdClass; // object | data
$sessionid = "sessionid_example"; // string | sessionid
$type = "type_example"; // string | type

try {
    $apiInstance->pusheventUsingPOST($data, $sessionid, $type);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->pusheventUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | **object**| data |
 **sessionid** | **string**| sessionid |
 **type** | **string**| type |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pusheventUsingPUT**
> pusheventUsingPUT($data, $sessionid, $type)

Send event to client

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$data = new \stdClass; // object | data
$sessionid = "sessionid_example"; // string | sessionid
$type = "type_example"; // string | type

try {
    $apiInstance->pusheventUsingPUT($data, $sessionid, $type);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->pusheventUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | **object**| data |
 **sessionid** | **string**| sessionid |
 **type** | **string**| type |

### Return type

void (empty response body)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **redirectToVorgangsverwaltungUsingGET**
> \Insign\Model\RedirectView redirectToVorgangsverwaltungUsingGET($sessionid)

Abort session and redirect to vorgangsverwaltung

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->redirectToVorgangsverwaltungUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->redirectToVorgangsverwaltungUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **redirectToVorgangsverwaltungUsingPOST**
> \Insign\Model\RedirectView redirectToVorgangsverwaltungUsingPOST($sessionid)

Abort session and redirect to vorgangsverwaltung

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->redirectToVorgangsverwaltungUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->redirectToVorgangsverwaltungUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **redirectonfailureUsingGET**
> \Insign\Model\RedirectView redirectonfailureUsingGET($sessionid)

Callback redirect on failure

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->redirectonfailureUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->redirectonfailureUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **redirectonfailureUsingPOST**
> \Insign\Model\RedirectView redirectonfailureUsingPOST($sessionid)

Callback redirect on failure

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->redirectonfailureUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->redirectonfailureUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingDELETE**
> registerUsingDELETE($clientid, $log)

Push client log to server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$log = "log_example"; // string | log

try {
    $apiInstance->registerUsingDELETE($clientid, $log);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **log** | **string**| log |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingDELETE1**
> string registerUsingDELETE1($clientid, $pushnotificationstoken, $secret, $startup)

Poll for new dualscreen client information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$pushnotificationstoken = "pushnotificationstoken_example"; // string | pushnotificationstoken
$secret = "secret_example"; // string | secret
$startup = false; // bool | startup

try {
    $result = $apiInstance->registerUsingDELETE1($clientid, $pushnotificationstoken, $secret, $startup);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingDELETE1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **pushnotificationstoken** | **string**| pushnotificationstoken | [optional]
 **secret** | **string**| secret | [optional]
 **startup** | **bool**| startup | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingGET**
> registerUsingGET($clientid, $log)

Push client log to server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$log = "log_example"; // string | log

try {
    $apiInstance->registerUsingGET($clientid, $log);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **log** | **string**| log |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingGET1**
> string registerUsingGET1($clientid, $pushnotificationstoken, $secret, $startup)

Poll for new dualscreen client information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$pushnotificationstoken = "pushnotificationstoken_example"; // string | pushnotificationstoken
$secret = "secret_example"; // string | secret
$startup = false; // bool | startup

try {
    $result = $apiInstance->registerUsingGET1($clientid, $pushnotificationstoken, $secret, $startup);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **pushnotificationstoken** | **string**| pushnotificationstoken | [optional]
 **secret** | **string**| secret | [optional]
 **startup** | **bool**| startup | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingHEAD**
> registerUsingHEAD($clientid, $log)

Push client log to server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$log = "log_example"; // string | log

try {
    $apiInstance->registerUsingHEAD($clientid, $log);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingHEAD: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **log** | **string**| log |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingHEAD1**
> string registerUsingHEAD1($clientid, $pushnotificationstoken, $secret, $startup)

Poll for new dualscreen client information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$pushnotificationstoken = "pushnotificationstoken_example"; // string | pushnotificationstoken
$secret = "secret_example"; // string | secret
$startup = false; // bool | startup

try {
    $result = $apiInstance->registerUsingHEAD1($clientid, $pushnotificationstoken, $secret, $startup);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingHEAD1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **pushnotificationstoken** | **string**| pushnotificationstoken | [optional]
 **secret** | **string**| secret | [optional]
 **startup** | **bool**| startup | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingOPTIONS**
> registerUsingOPTIONS($clientid, $log)

Push client log to server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$log = "log_example"; // string | log

try {
    $apiInstance->registerUsingOPTIONS($clientid, $log);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingOPTIONS: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **log** | **string**| log |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingOPTIONS1**
> string registerUsingOPTIONS1($clientid, $pushnotificationstoken, $secret, $startup)

Poll for new dualscreen client information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$pushnotificationstoken = "pushnotificationstoken_example"; // string | pushnotificationstoken
$secret = "secret_example"; // string | secret
$startup = false; // bool | startup

try {
    $result = $apiInstance->registerUsingOPTIONS1($clientid, $pushnotificationstoken, $secret, $startup);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingOPTIONS1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **pushnotificationstoken** | **string**| pushnotificationstoken | [optional]
 **secret** | **string**| secret | [optional]
 **startup** | **bool**| startup | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingPATCH**
> registerUsingPATCH($clientid, $log)

Push client log to server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$log = "log_example"; // string | log

try {
    $apiInstance->registerUsingPATCH($clientid, $log);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingPATCH: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **log** | **string**| log |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingPATCH1**
> string registerUsingPATCH1($clientid, $pushnotificationstoken, $secret, $startup)

Poll for new dualscreen client information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$pushnotificationstoken = "pushnotificationstoken_example"; // string | pushnotificationstoken
$secret = "secret_example"; // string | secret
$startup = false; // bool | startup

try {
    $result = $apiInstance->registerUsingPATCH1($clientid, $pushnotificationstoken, $secret, $startup);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingPATCH1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **pushnotificationstoken** | **string**| pushnotificationstoken | [optional]
 **secret** | **string**| secret | [optional]
 **startup** | **bool**| startup | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingPOST**
> registerUsingPOST($clientid, $log)

Push client log to server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$log = "log_example"; // string | log

try {
    $apiInstance->registerUsingPOST($clientid, $log);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **log** | **string**| log |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingPOST1**
> string registerUsingPOST1($clientid, $pushnotificationstoken, $secret, $startup)

Poll for new dualscreen client information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$pushnotificationstoken = "pushnotificationstoken_example"; // string | pushnotificationstoken
$secret = "secret_example"; // string | secret
$startup = false; // bool | startup

try {
    $result = $apiInstance->registerUsingPOST1($clientid, $pushnotificationstoken, $secret, $startup);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **pushnotificationstoken** | **string**| pushnotificationstoken | [optional]
 **secret** | **string**| secret | [optional]
 **startup** | **bool**| startup | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingPUT**
> registerUsingPUT($clientid, $log)

Push client log to server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$log = "log_example"; // string | log

try {
    $apiInstance->registerUsingPUT($clientid, $log);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **log** | **string**| log |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUsingPUT1**
> string registerUsingPUT1($clientid, $pushnotificationstoken, $secret, $startup)

Poll for new dualscreen client information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clientid = "clientid_example"; // string | clientid
$pushnotificationstoken = "pushnotificationstoken_example"; // string | pushnotificationstoken
$secret = "secret_example"; // string | secret
$startup = false; // bool | startup

try {
    $result = $apiInstance->registerUsingPUT1($clientid, $pushnotificationstoken, $secret, $startup);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->registerUsingPUT1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientid** | **string**| clientid |
 **pushnotificationstoken** | **string**| pushnotificationstoken | [optional]
 **secret** | **string**| secret | [optional]
 **startup** | **bool**| startup | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **requestPairingCodeUsingPOST**
> \Insign\Model\RequestPairingCodeResult requestPairingCodeUsingPOST($input)

Request new pairing code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\PairingInput(); // \Insign\Model\PairingInput | input

try {
    $result = $apiInstance->requestPairingCodeUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->requestPairingCodeUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\PairingInput**](../Model/PairingInput.md)| input |

### Return type

[**\Insign\Model\RequestPairingCodeResult**](../Model/RequestPairingCodeResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resendApplosPermanentLinkUsingPOST**
> \Insign\Model\RequestPairingCodeResult resendApplosPermanentLinkUsingPOST($input, $sessionid, $user)

Resend applos permanent link

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$input = new \Insign\Model\SendApplosPermanent(); // \Insign\Model\SendApplosPermanent | input
$sessionid = "sessionid_example"; // string | sessionid
$user = "user_example"; // string | user

try {
    $result = $apiInstance->resendApplosPermanentLinkUsingPOST($input, $sessionid, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->resendApplosPermanentLinkUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\SendApplosPermanent**](../Model/SendApplosPermanent.md)| input |
 **sessionid** | **string**| sessionid | [optional]
 **user** | **string**| user | [optional]

### Return type

[**\Insign\Model\RequestPairingCodeResult**](../Model/RequestPairingCodeResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetAusgehaendigtOriginalUsingGET**
> \Insign\Model\BasicResult resetAusgehaendigtOriginalUsingGET($sessionid)

Set ausgehaendigtOriginal to false

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->resetAusgehaendigtOriginalUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->resetAusgehaendigtOriginalUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetAusgehaendigtOriginalUsingPOST**
> \Insign\Model\BasicResult resetAusgehaendigtOriginalUsingPOST($sessionid)

Set ausgehaendigtOriginal to false

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->resetAusgehaendigtOriginalUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->resetAusgehaendigtOriginalUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetAusgehaendigtOriginalUsingPUT**
> \Insign\Model\BasicResult resetAusgehaendigtOriginalUsingPUT($sessionid)

Set ausgehaendigtOriginal to false

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->resetAusgehaendigtOriginalUsingPUT($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->resetAusgehaendigtOriginalUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendAllDocumentsUsingPOST**
> \Insign\Model\BasicResult sendAllDocumentsUsingPOST($input, $sessionid, $id_vorgangsverwaltung)

Send out E-Mails

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\SendDocuments(); // \Insign\Model\SendDocuments | input
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->sendAllDocumentsUsingPOST($input, $sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->sendAllDocumentsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\SendDocuments**](../Model/SendDocuments.md)| input |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendApplosLinkUsingPOST**
> \Insign\Model\RequestPairingCodeResult sendApplosLinkUsingPOST($input, $sessionid, $user)

Send out link for app-less signature

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\SendApplos(); // \Insign\Model\SendApplos | input
$sessionid = "sessionid_example"; // string | sessionid
$user = "user_example"; // string | user

try {
    $result = $apiInstance->sendApplosLinkUsingPOST($input, $sessionid, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->sendApplosLinkUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\SendApplos**](../Model/SendApplos.md)| input |
 **sessionid** | **string**| sessionid |
 **user** | **string**| user | [optional]

### Return type

[**\Insign\Model\RequestPairingCodeResult**](../Model/RequestPairingCodeResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sessionabbrechenUsingGET**
> object sessionabbrechenUsingGET($sessionid, $gdpr_declined, $redirect)

Abort session and return to callback url

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$gdpr_declined = true; // bool | gdprDeclined
$redirect = true; // bool | redirect

try {
    $result = $apiInstance->sessionabbrechenUsingGET($sessionid, $gdpr_declined, $redirect);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->sessionabbrechenUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **gdpr_declined** | **bool**| gdprDeclined | [optional]
 **redirect** | **bool**| redirect | [optional] [default to true]

### Return type

**object**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sessionabbrechenUsingPOST**
> object sessionabbrechenUsingPOST($sessionid, $gdpr_declined, $redirect)

Abort session and return to callback url

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$gdpr_declined = true; // bool | gdprDeclined
$redirect = true; // bool | redirect

try {
    $result = $apiInstance->sessionabbrechenUsingPOST($sessionid, $gdpr_declined, $redirect);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->sessionabbrechenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **gdpr_declined** | **bool**| gdprDeclined | [optional]
 **redirect** | **bool**| redirect | [optional] [default to true]

### Return type

**object**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sessionbeendenUsingGET**
> \Insign\Model\RedirectView sessionbeendenUsingGET($sessionid)

Finish session and return to callback url

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->sessionbeendenUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->sessionbeendenUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sessionbeendenUsingPOST**
> \Insign\Model\RedirectView sessionbeendenUsingPOST($sessionid)

Finish session and return to callback url

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->sessionbeendenUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->sessionbeendenUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\RedirectView**](../Model/RedirectView.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setDisplayUsingPOST**
> \Insign\Model\BasicResult setDisplayUsingPOST($input, $sessionid)

Set displayname and customer name of a session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\SessionDisplay(); // \Insign\Model\SessionDisplay | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->setDisplayUsingPOST($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->setDisplayUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\SessionDisplay**](../Model/SessionDisplay.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setDisplayUsingPUT**
> \Insign\Model\BasicResult setDisplayUsingPUT($input, $sessionid)

Set displayname and customer name of a session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$input = new \Insign\Model\SessionDisplay(); // \Insign\Model\SessionDisplay | input
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->setDisplayUsingPUT($input, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->setDisplayUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\SessionDisplay**](../Model/SessionDisplay.md)| input |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setSessionGUIPropertyUsingGET**
> \Insign\Model\Result setSessionGUIPropertyUsingGET($property_name, $property_value, $sessionid)

Modify a session UI property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$property_name = "property_name_example"; // string | propertyName
$property_value = array('key' => "property_value_example"); // map[string,string] | propertyValue
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->setSessionGUIPropertyUsingGET($property_name, $property_value, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->setSessionGUIPropertyUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_name** | **string**| propertyName |
 **property_value** | [**map[string,string]**](../Model/string.md)| propertyValue |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\Result**](../Model/Result.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setSessionGUIPropertyUsingPOST**
> \Insign\Model\Result setSessionGUIPropertyUsingPOST($properties, $sessionid)

Modify a list of session UI properties

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$properties = new \stdClass; // object | properties
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->setSessionGUIPropertyUsingPOST($properties, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->setSessionGUIPropertyUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **properties** | **object**| properties |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\Result**](../Model/Result.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setSessionGUIPropertyUsingPOST1**
> \Insign\Model\Result setSessionGUIPropertyUsingPOST1($property_name, $property_value, $sessionid)

Modify a session UI property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$property_name = "property_name_example"; // string | propertyName
$property_value = array('key' => "property_value_example"); // map[string,string] | propertyValue
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->setSessionGUIPropertyUsingPOST1($property_name, $property_value, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->setSessionGUIPropertyUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_name** | **string**| propertyName |
 **property_value** | [**map[string,string]**](../Model/string.md)| propertyValue |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\Result**](../Model/Result.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setSessionPropertyUsingGET**
> \Insign\Model\Result setSessionPropertyUsingGET($property_name, $property_value, $sessionid)

Modify a session mutable property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$property_name = "property_name_example"; // string | propertyName
$property_value = array('key' => "property_value_example"); // map[string,string] | propertyValue
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->setSessionPropertyUsingGET($property_name, $property_value, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->setSessionPropertyUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_name** | **string**| propertyName |
 **property_value** | [**map[string,string]**](../Model/string.md)| propertyValue |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\Result**](../Model/Result.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setSessionPropertyUsingPOST**
> \Insign\Model\Result setSessionPropertyUsingPOST($property_name, $property_value, $sessionid)

Modify a session mutable property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$property_name = "property_name_example"; // string | propertyName
$property_value = array('key' => "property_value_example"); // map[string,string] | propertyValue
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->setSessionPropertyUsingPOST($property_name, $property_value, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->setSessionPropertyUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_name** | **string**| propertyName |
 **property_value** | [**map[string,string]**](../Model/string.md)| propertyValue |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\Result**](../Model/Result.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setSessionVarUsingPOST**
> \Insign\Model\BasicResult setSessionVarUsingPOST($sessionid, $vars)

Set Session Var

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid
$vars = new \Insign\Model\SetSessionVar(); // \Insign\Model\SetSessionVar | vars

try {
    $result = $apiInstance->setSessionVarUsingPOST($sessionid, $vars);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->setSessionVarUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |
 **vars** | [**\Insign\Model\SetSessionVar**](../Model/SetSessionVar.md)| vars |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setWifiDirectIDUsingPOST**
> \Insign\Model\BasicResult setWifiDirectIDUsingPOST($input)

Set Wifi-Direct-ID for pairingcode

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$input = new \Insign\Model\SetWifiID(); // \Insign\Model\SetWifiID | input

try {
    $result = $apiInstance->setWifiDirectIDUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->setWifiDirectIDUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\SetWifiID**](../Model/SetWifiID.md)| input |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **storeMultipleDocumentsUsingPOST**
> \Insign\Model\BasicResult storeMultipleDocumentsUsingPOST($data, $sessionid, $id_vorgangsverwaltung)

Store documents data (signatures, form fields)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$data = new \Insign\Model\SessionData(); // \Insign\Model\SessionData | data
$sessionid = "sessionid_example"; // string | sessionid
$id_vorgangsverwaltung = "id_vorgangsverwaltung_example"; // string | idVorgangsverwaltung

try {
    $result = $apiInstance->storeMultipleDocumentsUsingPOST($data, $sessionid, $id_vorgangsverwaltung);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->storeMultipleDocumentsUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Insign\Model\SessionData**](../Model/SessionData.md)| data |
 **sessionid** | **string**| sessionid |
 **id_vorgangsverwaltung** | **string**| idVorgangsverwaltung | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **storeSignatureUsingPOST**
> \Insign\Model\StoreSignatureResult storeSignatureUsingPOST($data)

Store single signature

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$data = new \Insign\Model\StoreSignature(); // \Insign\Model\StoreSignature | data

try {
    $result = $apiInstance->storeSignatureUsingPOST($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->storeSignatureUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Insign\Model\StoreSignature**](../Model/StoreSignature.md)| data |

### Return type

[**\Insign\Model\StoreSignatureResult**](../Model/StoreSignatureResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **terminateWifiDirectUsingGET**
> \Insign\Model\BasicResult terminateWifiDirectUsingGET()

terminate Wifi-Direct connection

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->terminateWifiDirectUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->terminateWifiDirectUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **terminateWifiDirectUsingPOST**
> \Insign\Model\BasicResult terminateWifiDirectUsingPOST()

terminate Wifi-Direct connection

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->terminateWifiDirectUsingPOST();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->terminateWifiDirectUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unpairCustomerUsingGET**
> \Insign\Model\BasicResult unpairCustomerUsingGET($sessionid)

Unpair all additional (customer) devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->unpairCustomerUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->unpairCustomerUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unpairCustomerUsingPOST**
> \Insign\Model\BasicResult unpairCustomerUsingPOST($sessionid)

Unpair all additional (customer) devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->unpairCustomerUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->unpairCustomerUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unpairUserUsingGET**
> \Insign\Model\BasicResult unpairUserUsingGET($sessionid)

Unpair all additional user devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->unpairUserUsingGET($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->unpairUserUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unpairUserUsingPOST**
> \Insign\Model\BasicResult unpairUserUsingPOST($sessionid)

Unpair all additional user devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->unpairUserUsingPOST($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->unpairUserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unpairUsingGET1**
> \Insign\Model\BasicResult unpairUsingGET1($sessionid)

Unpair all paired users signature devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->unpairUsingGET1($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->unpairUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unpairUsingPOST**
> \Insign\Model\BasicResult unpairUsingPOST($input)

Unpair signature device

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$input = new \Insign\Model\UnpairInput(); // \Insign\Model\UnpairInput | input

try {
    $result = $apiInstance->unpairUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->unpairUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\UnpairInput**](../Model/UnpairInput.md)| input |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unpairUsingPOST1**
> \Insign\Model\BasicResult unpairUsingPOST1($sessionid)

Unpair all paired users signature devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->unpairUsingPOST1($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->unpairUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| sessionid | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unpairallforuserUsingGET**
> \Insign\Model\BasicResult unpairallforuserUsingGET($userid)

Unpair all paired users signature devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$userid = "userid_example"; // string | userid

try {
    $result = $apiInstance->unpairallforuserUsingGET($userid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->unpairallforuserUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userid** | **string**| userid | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unpairallforuserUsingPOST**
> \Insign\Model\BasicResult unpairallforuserUsingPOST($userid)

Unpair all paired users signature devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$userid = "userid_example"; // string | userid

try {
    $result = $apiInstance->unpairallforuserUsingPOST($userid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->unpairallforuserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userid** | **string**| userid | [optional]

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploadphotoApplosUsingPOST**
> \Insign\Model\BasicResult uploadphotoApplosUsingPOST($json)

Upload photo

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$json = new \Insign\Model\PhotoUpload(); // \Insign\Model\PhotoUpload | json

try {
    $result = $apiInstance->uploadphotoApplosUsingPOST($json);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->uploadphotoApplosUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **json** | [**\Insign\Model\PhotoUpload**](../Model/PhotoUpload.md)| json |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploadphotostream2UsingPOST1**
> string uploadphotostream2UsingPOST1($filename, $pairing_token, $sessionid, $uploader, $user, $displayname, $filesize, $productid)

Upload photo

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filename = "filename_example"; // string | filename
$pairing_token = "pairing_token_example"; // string | pairingToken
$sessionid = "sessionid_example"; // string | sessionid
$uploader = "uploader_example"; // string | uploader
$user = "user_example"; // string | user
$displayname = "displayname_example"; // string | displayname
$filesize = 789; // int | filesize
$productid = "productid_example"; // string | productid

try {
    $result = $apiInstance->uploadphotostream2UsingPOST1($filename, $pairing_token, $sessionid, $uploader, $user, $displayname, $filesize, $productid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->uploadphotostream2UsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filename** | **string**| filename |
 **pairing_token** | **string**| pairingToken |
 **sessionid** | **string**| sessionid |
 **uploader** | **string**| uploader |
 **user** | **string**| user |
 **displayname** | **string**| displayname | [optional]
 **filesize** | **int**| filesize | [optional]
 **productid** | **string**| productid | [optional]

### Return type

**string**

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/octet-stream
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **waitForNotificationUsingPOST1**
> \Insign\Model\GenericResult waitForNotificationUsingPOST1($json_polling, $sessionid)

Multi device messaging. Long term polling

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = Insign\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$json_polling = new \Insign\Model\Polling(); // \Insign\Model\Polling | jsonPolling
$sessionid = "sessionid_example"; // string | sessionid

try {
    $result = $apiInstance->waitForNotificationUsingPOST1($json_polling, $sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->waitForNotificationUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **json_polling** | [**\Insign\Model\Polling**](../Model/Polling.md)| jsonPolling |
 **sessionid** | **string**| sessionid |

### Return type

[**\Insign\Model\GenericResult**](../Model/GenericResult.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **whoIsPairingMobileUsingPOST**
> \Insign\Model\BasicResult whoIsPairingMobileUsingPOST($input)

Determine type of pairing code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Insign\Api\InternalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$input = new \Insign\Model\MobilePairingInput(); // \Insign\Model\MobilePairingInput | input

try {
    $result = $apiInstance->whoIsPairingMobileUsingPOST($input);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InternalApi->whoIsPairingMobileUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**\Insign\Model\MobilePairingInput**](../Model/MobilePairingInput.md)| input |

### Return type

[**\Insign\Model\BasicResult**](../Model/BasicResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

