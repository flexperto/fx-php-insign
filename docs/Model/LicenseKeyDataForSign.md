# LicenseKeyDataForSign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | [**\Insign\Model\LicenseKeyData**](LicenseKeyData.md) |  | [optional] 
**privatekeypem** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


