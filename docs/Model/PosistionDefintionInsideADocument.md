# PosistionDefintionInsideADocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**h** | **double** | Height of the signature | [optional] 
**page** | **int** | Page of the element in the PDF document. | [optional] 
**w** | **double** | Width of the signature | [optional] 
**x0** | **double** | X-Position. Interpretation depends on QES-Provider | [optional] 
**y0** | **double** | Y-Position. Interpretation depends on QES-Provider | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


