# DocumentDataStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additional_info** | **string** | Additional information for the Document. since v3.6 | [optional] 
**displayname** | **string** | Display name for the Document | [optional] 
**docchecksum** | **string** | Document checksum | [optional] 
**docid** | **string** | Document ID of the Document | 
**docname** | **string** | Document name | [optional] 
**hasbeenchanged** | **bool** | Document must be handed out (again) | [optional] 
**hasbeenedited** | **bool** | Document has been edited (fields have been filled) | [optional] 
**hasbeenread** | **bool** | Document has been read | [optional] 
**hasbeensigned** | **bool** | Document was signed at least once | [optional] 
**hasbeensigned_completely** | **bool** | Document has been signed completely (all required and non required fields) | [optional] 
**hasbeensigned_required** | **bool** | Document was signed at least once and all required fields have been signed | [optional] 
**hasrequired** | **bool** | Document has required fields that haven&#39;t been signed | [optional] 
**isbipro** | **bool** | Document was recognized as BiPro conform | [optional] 
**number_of_signatures** | **int** | Number of signatures which were placed | [optional] 
**number_of_signatures_needed** | **int** | Number of required signature fields | [optional] 
**number_of_signatures_needed_done** | **int** | Number of required signatures which were placed | [optional] 
**number_of_signatures_needed_with_optional** | **int** | Number of signature fields (required and non required) | [optional] 
**numberofpages** | **int** | Number of pages of the document | [optional] 
**signatur_fields_status_list** | [**\Insign\Model\SignatureFieldStatus[]**](SignatureFieldStatus.md) | List of status information for signature fields per document. since v3.5.12 | [optional] 
**user_ausgefuellt** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


