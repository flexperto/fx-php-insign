# SendDocuments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_info** | [**\Insign\Model\ClientInfo**](ClientInfo.md) |  | [optional] 
**customer_name** | **string** | Customer name | [optional] 
**datei** | **bool** | Were the documents also handed out by file? | [optional] 
**extern_users** | **string[]** | List of online editing users | [optional] 
**mail_language** | **string** | Mail language | [optional] 
**note** | **string** | Personal note in the e-mail | [optional] 
**papier** | **bool** | Were the documents also handed out by paper? | [optional] 
**qes_mail_type** | **string** | QES e-mail type | [optional] 
**qes_start_link** | **string** | QES start link | [optional] 
**recipient** | **string** | Recipient e-mail address | 
**recipientbcc** | **string** | Recipient e-mail BCC address | [optional] 
**recipientcc** | **string** | Recipient e-mail CC address | [optional] 
**recipientsms** | **string** | SMS recipient phone number | [optional] 
**salutation** | **string** | Salutation in the e-mail | [optional] 
**send_fertig** | **bool** | Send documents after completing a process | 
**send_mustberead** | **bool** | Only send required documents | 
**sender** | **string** | Sender of the e-mail | [optional] 
**smsonly** | **bool** | Document and password by SMS? | [optional] 
**subject** | **string** | Subject of the e-mail | [optional] 
**vorgang_name** | **string** | Process name | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


