# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** |  | [optional] 
**common_name** | **string** |  | [optional] 
**display_name** | **string** |  | [optional] 
**do_not_encode_password** | **bool** |  | [optional] 
**existing_common_name** | **string** |  | [optional] 
**given_name** | **string** |  | [optional] 
**groups** | [**\Insign\Model\Group[]**](Group.md) |  | [optional] 
**inactive_date** | **string** |  | [optional] 
**mail** | **string** |  | [optional] 
**manager** | **string** |  | [optional] 
**password** | **string** |  | [optional] 
**remove_inactive_date** | **bool** |  | [optional] 
**sur_name** | **string** |  | [optional] 
**telephone** | **string** |  | [optional] 
**title** | **string** |  | [optional] 
**uid** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


