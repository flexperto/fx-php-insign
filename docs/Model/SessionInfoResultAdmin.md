# SessionInfoResultAdmin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ausgehaendigt** | **bool** | Has the process been handed out? | [optional] 
**aushaendigen_pflicht** | **bool** | Do the required documents of the process need to be handed out? | [optional] 
**completed** | **bool** | Completed | [optional] 
**controller** | **string** | Controller of the process | [optional] 
**created** | **string** | Time of creation of the session. Human readable Format | [optional] 
**created_date** | **string** | Time of creation of the session | [optional] 
**created_time** | **string** | Time of creation of the session | [optional] 
**created_timestamp** | **int** | Time of creation of the session | [optional] 
**custom_info** | **string** | Custom info about the session | [optional] 
**displaycustomer** | **string** | Display name for the customer | [optional] 
**displayname** | **string** | Display name for the session | 
**docids** | **string[]** | Document IDs of the Documents in this Session | [optional] 
**dsgvo_declined** | **string** | User who declined the GDPR consent. Null if no user did so | [optional] 
**empfaenger_extern** | **string** | The external editors e-mail addresses | [optional] 
**empfaenger_sms_extern** | **string** | The external editors phone numbers | [optional] 
**error** | **int** | Error code 0&#x3D;OK | 
**extern** | **bool** | In external editing | [optional] 
**extern_enabled** | **bool** | Can the process be made available for external editing | [optional] 
**extern_for_this_user** | **bool** | Extern process for this user | [optional] 
**extern_per_sms** | **bool** | External link by SMS | [optional] 
**full_name** | **string** | User full name | [optional] 
**has_session** | **bool** | Session available for this record | [optional] 
**has_trace** | **bool** | Trace avialable for this session | [optional] 
**in_qes** | **bool** | Is the process in QES | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**modified** | **string** | Last modification to the session. Human readable Format | [optional] 
**modified_date** | **string** | Last modification to the session | [optional] 
**modified_time** | **string** | Last modification to the session | [optional] 
**modified_timestamp** | **int** | Last modification to the session | [optional] 
**multiple_users** | **bool** | Has the process been handed out to multiple users? | [optional] 
**number_of_documents** | **int** | Number of documents in this session | [optional] 
**number_of_form_fields** | **int** | Number of form fields in this session | [optional] 
**number_of_form_fields_filled** | **int** | Number of filled out form fields in this session | [optional] 
**number_of_signatures** | **int** | Number of signatures which were placed | [optional] 
**number_of_signatures_needed** | **int** | Number of required signature fields | [optional] 
**number_of_signatures_needed_done** | **int** | Number of required signatures which were placed | [optional] 
**number_of_signatures_needed_with_optional** | **int** | Number of signature fields (required and non required) | [optional] 
**offline** | **bool** | In offline editing on a device | [optional] 
**offline_available** | **bool** | Is the process available for offline editing? | [optional] 
**process_step** | **string** | Last process step | [optional] 
**qes_result_preliminary** | **bool** | Is QES result preliminary | [optional] 
**sessionid** | **string** | Session ID of the session | 
**sms_versand_enabled** | **bool** | SMS sending enabled | [optional] 
**status** | **string** | Status string of the session | [optional] 
**thumb_nail** | **string** | Session Thumbnail Base64 encoded image | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 
**transaktionsnummer** | **string** | transaction number | [optional] 
**user** | **string** | User ID | [optional] 
**write_audit** | **bool** | Should audit data be generated and a report be available for new processes? | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


