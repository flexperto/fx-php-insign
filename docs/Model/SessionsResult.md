# SessionsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **int** | Error code 0&#x3D;OK | 
**full_name** | **string** | Full name of the user | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**sessions** | [**\Insign\Model\SessionInfoResult[]**](SessionInfoResult.md) | List of processes | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


