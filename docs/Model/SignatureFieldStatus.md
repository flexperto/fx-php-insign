# SignatureFieldStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayname** | **string** | Display info for the roll of the field. since v3.4.0 | [optional] 
**document_id** | **string** | Document ID of the document with this field. since v3.1.2 | [optional] 
**field_id** | **string** | Field ID of the signature field. since v3.1.2 | [optional] 
**mandatory** | **bool** | Required field? since v3.1.2 | [optional] 
**position_index** | **int** | Position index for the signature field. since v3.5.12 | [optional] 
**quick_info_parsed_role** | **string** | Assigned role from Quickinfo. since v3.5.12 | [optional] 
**quickinfo** | **string** | Quickinfo for the signature. since v3.4.0 | [optional] 
**role** | **string** | Roll (Quickinfo) of the field. since v3.1.2 | [optional] 
**sign_timestamp** | **string** | Time of signing. since v3.4.0 | [optional] 
**signed** | **bool** | Has the field been signed? since v3.1.2 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


