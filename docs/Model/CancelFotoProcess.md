# CancelFotoProcess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienttype** | **string** | Client type | 
**pairing_token** | **string** | Pairing token when calling via the smartphone app | [optional] 
**sessionid** | **string** | Session ID | [optional] 
**user** | **string** | User ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


