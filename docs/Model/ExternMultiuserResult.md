# ExternMultiuserResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **int** | Error code 0&#x3D;OK | 
**extern_users** | [**\Insign\Model\ExternUserResult[]**](ExternUserResult.md) | List of the users created | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**messages** | **map[string,string]** | Error message if error!&#x3D;0 | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 
**usermessage** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


