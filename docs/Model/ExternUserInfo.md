# ExternUserInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dsgvo_declined** | **bool** |  | [optional] 
**extern_user** | **string** |  | [optional] 
**ident_review** | **bool** |  | [optional] 
**order_number** | **int** |  | [optional] 
**vorgangfertig** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


