# ConfigurationInformationForASingleDocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additional_info** | **string** | Other information about the document. | [optional] 
**allow_form_editing** | **bool** | Specifies whether the document may be changed via the PDF editor. Optional. Default value is true. | [optional] 
**crypt_owner_password** | **string** | Owner Password of the PDF file if available. | [optional] 
**crypt_user_password** | **string** | User Password of the PDF file if available. | [optional] 
**disable_append_mode** | **bool** | Disable Append mode. Do not activate this unless instructed to do so. | [optional] 
**displayname** | **string** | Display name of the document in the GUI, emails, etc. It is configurable via application properties if and when this is used. | 
**file** | **string** | The actual document as a byte array. Base-64 encoded. Null allowed when uploading via /configure/uploaddocument. | [optional] 
**file_download_password** | **string** | Password needed to download the file from fileURL | [optional] 
**file_download_user** | **string** | User needed to download the file from fileURL | [optional] 
**file_url** | **string** | URL where inSign can download the document | [optional] 
**id** | **string** | Document ID, to refer to later. Leave empty to automatically generate a UID. | 
**maybedeletedbyuser** | **bool** | Document can be deleted by user. | [optional] 
**mustberead** | **bool** | Document must be read. | [optional] 
**owner_password_for_reading** | **string** | Owner Password to open the document. | [optional] 
**pre_filled_fields** | [**\Insign\Model\Annotation[]**](Annotation.md) | Configure non-signature annotations in the document | [optional] 
**scan_sig_tags** | **bool** | This document will be scanned for SIG-Tags. This overrides inSign configuration for this document. Optional. Default value is null. | [optional] 
**signatures** | [**\Insign\Model\ConfigureSignature[]**](ConfigureSignature.md) | The preconfigured signatures in this document. For documents with BiPro signature fields leave this empty. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


