# SendNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienttype** | **string** | Type of client. smartphone or pc | 
**notification** | [**\Insign\Model\ClientNotification**](ClientNotification.md) | Message | [optional] 
**pairing_token** | **string** | Pairing token | [optional] 
**sessionid** | **string** | Session ID | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


