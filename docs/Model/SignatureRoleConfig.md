# SignatureRoleConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rollen_zu_primaer_unterschrift** | **string[]** | If the unterschriftenRollenbasiertAktiv flag is active, this flag specifies which technical signature roles should use the primary signature method (consultant or logged in permanent end user). A * can be used here as a wildcard, for all roles that cannot otherwise be assigned. If the field is passed with null, the fields already defined when the session was created will be used. If an empty array is passed, the previous roles are overwritten. | [optional] 
**rollen_zu_sekundaer_unterschrift** | **string[]** | If the unterschriftenRollenbasiertAktiv flag is active, this flag specifies which technical signature roles should use the secondary (customer or similar) signature method. A * can be used here as a wildcard, for all roles that can not otherwise be assigned. If the field is passed with null, the fields already defined when the session was created will be used. If an empty array is passed, the previous roles are overwritten. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


