# QuerySessions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enddate** | **string** |  | [optional] 
**sessionid** | **string** |  | [optional] 
**startdate** | **string** |  | [optional] 
**tan** | **string** |  | [optional] 
**userid** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


