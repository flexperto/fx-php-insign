# ApplicationContext

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application_name** | **string** |  | [optional] 
**autowire_capable_bean_factory** | [**\Insign\Model\AutowireCapableBeanFactory**](AutowireCapableBeanFactory.md) |  | [optional] 
**bean_definition_count** | **int** |  | [optional] 
**bean_definition_names** | **string[]** |  | [optional] 
**class_loader** | [**\Insign\Model\ClassLoader**](ClassLoader.md) |  | [optional] 
**display_name** | **string** |  | [optional] 
**environment** | [**\Insign\Model\Environment**](Environment.md) |  | [optional] 
**id** | **string** |  | [optional] 
**parent** | [**\Insign\Model\ApplicationContext**](ApplicationContext.md) |  | [optional] 
**parent_bean_factory** | [**\Insign\Model\BeanFactory**](BeanFactory.md) |  | [optional] 
**startup_date** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


