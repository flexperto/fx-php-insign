# AppConfigurationData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_version** | **string** |  | [optional] 
**app_version_tag** | **string** |  | [optional] 
**configuration_element** | [**\Insign\Model\ConfigurationElement[]**](ConfigurationElement.md) |  | [optional] 
**configuration_element_meta_data** | [**map[string,\Insign\Model\SourcedConfigurationElement[]]**](array.md) |  | [optional] 
**configuration_element_tags** | **string[]** |  | [optional] 
**custom_app_versions** | **string[]** |  | [optional] 
**m_boolean** | **string** |  | [optional] 
**m_double** | **string** |  | [optional] 
**m_integer** | **string** |  | [optional] 
**m_string** | **string** |  | [optional] 
**save_file_location** | **string** |  | [optional] 
**save_file_location_writeable** | **bool** |  | [optional] 
**save_zk_location** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


