# PropertyAudit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**changed** | [**\DateTime**](\DateTime.md) |  | [optional] 
**dbid** | **int** |  | [optional] 
**newvalue** | **string** |  | [optional] 
**oldvalue** | **string** |  | [optional] 
**propertykey** | **string** |  | [optional] 
**userid** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


