# SourcedConfigurationElement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cannotbe_overridden** | **bool** |  | [optional] 
**desc** | **string** |  | [optional] 
**key** | **string** |  | [optional] 
**source** | **string** |  | [optional] 
**value** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


