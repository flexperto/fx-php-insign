# DocumentData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additional_info** | **string** | Additional Info String for the dsocument | [optional] 
**annotations** | [**\Insign\Model\Annotation[]**](Annotation.md) | The annotations of a document | [optional] 
**can_reset_signature_extern** | **bool** | Indicates if there is a saved state for the document | [optional] 
**canbedeleted** | **bool** | File can be deleted by the user | [optional] 
**canbeedited_extern** | **bool** | Document can be edited in external mode | [optional] 
**canbesigned** | **bool** | Document can only be signed if there are signature fields | [optional] 
**displayname** | **string** | Display name of the document | [optional] 
**docchecksum** | **string** | Document checksum | [optional] 
**docid** | **string** | ID of the document | 
**docname** | **string** | Document name | [optional] 
**dsgvo_roles** | **string[]** | Signatureroles that still need to accept the gdpr popup | [optional] 
**formfillingallowed** | **bool** | Filling in allowed | [optional] 
**gwg_image** | **bool** | Is the document a copy of an identity document? | [optional] 
**hasbeenchanged** | **bool** | Document must be handed out (again) | [optional] 
**hasbeenedited** | **bool** | Document has already been edited at least once (fields have been filled in) | [optional] 
**hasbeenread** | **bool** | Document has already been read | [optional] 
**hasbeensigned** | **bool** | Document was signed at least once | [optional] 
**hasbeensigned_completely** | **bool** | Document has been completely signed (all available signature fields) | [optional] 
**hasbeensigned_required** | **bool** | Document has been signed at least once and all mandatory fields have already been signed | [optional] 
**hasrequired** | **bool** | Document has fields that are required and possibly not filled out | [optional] 
**is_uploaded_by_user** | **bool** | Document is uploaded by user | [optional] 
**is_user_ausgefuellt** | **bool** | All compulsory text fields have been filled in or filling out has been skipped | [optional] 
**isbipro** | **bool** | Document recognized as BiPRO compliant document | [optional] 
**isformular** | **bool** | Document was recognized as a form | [optional] 
**mustberead** | **bool** | Document must be read | [optional] 
**mustbesigned** | **bool** | Document must be signed | [optional] 
**number_of_added_pages** | **int** | Number of pages added to the document later | [optional] 
**number_of_signatures** | **int** | Number of signatures | [optional] 
**number_of_signatures_needed** | **int** | Number of mandatory signature fields | [optional] 
**number_of_signatures_needed_done** | **int** | Number of required signatures that have been placed | [optional] 
**number_of_signatures_needed_with_optional** | **int** | Number of all signature fields | [optional] 
**numberofpages** | **int** | Number of pages of the document | [optional] 
**ratios** | [**\Insign\Model\PageRatio[]**](PageRatio.md) | Aspect ratio of each page of the document | [optional] 
**typeicon** | **string** | Icon id of the file type | [optional] 
**uploaded_by_user** | **bool** |  | [optional] 
**user_ausgefuellt** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


