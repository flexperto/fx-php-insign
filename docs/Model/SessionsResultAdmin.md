# SessionsResultAdmin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_date_format** | **string** | Created date format | [optional] 
**error** | **int** | Error code 0&#x3D;OK | 
**full_name** | **string** | Full name of the user | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**sessions** | [**\Insign\Model\SessionInfoResultAdmin[]**](SessionInfoResultAdmin.md) | List of processes | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


