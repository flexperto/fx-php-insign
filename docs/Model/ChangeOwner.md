# ChangeOwner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newowner** | **string** | the user ID of the new owner | 
**sessionid** | **string** | The session ID of the session to be changed | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


