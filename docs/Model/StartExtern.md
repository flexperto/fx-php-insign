# StartExtern

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**callback_url** | **string** | Specific callback URL for this user | [optional] 
**client_info** | [**\Insign\Model\ClientInfo**](ClientInfo.md) |  | [optional] 
**mail_language** | **string** | Mail language | [optional] 
**note** | **string** | Personal note in the e-mail | [optional] 
**order_number** | **int** | Index number to determine the order | [optional] 
**password** | **string** | If not empty this password will be set | [optional] 
**qes_config** | [**\Insign\Model\ConfigurationForTheQualifiedElectronicSignature**](ConfigurationForTheQualifiedElectronicSignature.md) | Optional: Initial person-data for QES. Not used if qes is not set. | [optional] 
**recipient** | **string** | E-mail address of the recipient | 
**recipientbcc** | **string** | E-mail address of the BCC recipient | [optional] 
**recipientcc** | **string** | E-mail address of the CC recipient | [optional] 
**recipientsms** | **string** | Phone number of the SMS recipient | [optional] 
**roles** | **string[]** | The recipient&#39;s roles | [optional] 
**roletype** | **string** | Type of roll, currently &#39;owner&#39; and &#39;signer&#39; are used to display different texts when completing a process. pdfviewer.hinweis.allsigned.&lt;roletype&gt; is displayed | [optional] 
**salutation** | **string** | Salutation in the e-mail | [optional] 
**send_emails** | **bool** | Should e-mails be sent? | [optional] 
**send_sms** | **bool** | Should SMS be sent | [optional] 
**sender** | **string** | Sender of the e-mail | [optional] 
**single_sign_on_enabled** | **bool** | If singleSignOnEnabled is true the user is signed in automatically after clicking the link. The returned token is a one time token and deactivated after one time use | [optional] 
**smsonly** | **bool** | Document and password by SMS | [optional] 
**subject** | **string** | Subject of the e-mail | [optional] 
**sync_edit** | **bool** | Editor live synchronisation | [optional] 
**text** | **string** | Contents of the e-mail | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


