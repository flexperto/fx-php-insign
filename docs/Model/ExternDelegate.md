# ExternDelegate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mail** | **string** |  | [optional] 
**mail_language** | **string** |  | [optional] 
**number** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


