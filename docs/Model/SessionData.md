# SessionData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ausgehaendigt_original** | **bool** | Has the document been handed out | [optional] 
**autocomplete_entries** | [**map[string,\Insign\Model\Autocomplete]**](Autocomplete.md) | Autocomplete entries | [optional] 
**db_version** | **string** | Database version | [optional] 
**displaycustomer** | **string** | Display name for the customer | [optional] 
**displayname** | **string** | Display name for the session | [optional] 
**documents** | [**\Insign\Model\DocumentData[]**](DocumentData.md) | List of documents | [optional] 
**dsgvo_abfrage** | **bool** | GDPR consent request | [optional] 
**dsgvo_approved_roles** | **string[]** | Role that accepted the GDPR popup | [optional] 
**error** | **int** | Error code 0&#x3D;OK | [optional] 
**full_name** | **string** | Full name of the user | [optional] 
**mail_mode** | **string** | Mode for sending | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**modified_timestamp** | **string** | Modified timestamp for offline processes | [optional] 
**need_to_acceept_to_sign_text** | **string** | Custom text displayed if GDPR consent is required | [optional] 
**publickey** | **string** | RSA public key | [optional] 
**reset_signature_for_extern** | **bool** | Is the option available? | [optional] 
**reset_signature_for_extern_id** | **string** | Doc-ID that can be reseted extern? | [optional] 
**tan** | **string** | transaction number | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 
**version** | **string** | Application version | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


