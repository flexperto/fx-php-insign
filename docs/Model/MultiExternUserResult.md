# MultiExternUserResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **int** |  | [optional] 
**group_list** | [**\Insign\Model\Group[]**](Group.md) |  | [optional] 
**message** | **string** |  | [optional] 
**trace** | **string** |  | [optional] 
**user_list** | [**\Insign\Model\ExternUserResult[]**](ExternUserResult.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


