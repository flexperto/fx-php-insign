# QESDataContainer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**birthday** | [**\DateTime**](\DateTime.md) |  | [optional] 
**city** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**default_position** | [**\Insign\Model\PagePosition**](PagePosition.md) |  | [optional] 
**doc_id_map** | **map[string,string]** |  | [optional] 
**email** | **string** |  | [optional] 
**first_names** | **string** |  | [optional] 
**gender** | **string** |  | [optional] 
**ident_url** | **string** |  | [optional] 
**in_qes** | **bool** |  | [optional] 
**is_aborted** | **bool** |  | [optional] 
**last_names** | **string** |  | [optional] 
**mail_subject** | **string** |  | [optional] 
**mail_text** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**mobile_phone** | **string** |  | [optional] 
**nationality** | **string** |  | [optional] 
**num_of_qes** | **int** |  | [optional] 
**password** | **string** |  | [optional] 
**place_of_birth** | **string** |  | [optional] 
**positions** | [**map[string,\Insign\Model\PagePosition]**](PagePosition.md) |  | [optional] 
**preliminary_state** | **map[string,object]** |  | [optional] 
**qes_start_link** | **string** |  | [optional] 
**qes_tan** | **string** |  | [optional] 
**qes_type** | **string** |  | [optional] 
**result_preliminary** | **bool** |  | [optional] 
**street** | **string** |  | [optional] 
**street_number** | **string** |  | [optional] 
**success** | **bool** |  | [optional] 
**username** | **string** |  | [optional] 
**zip_code** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


