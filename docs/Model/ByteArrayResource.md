# ByteArrayResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**byte_array** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**file** | [**\Insign\Model\File**](File.md) |  | [optional] 
**filename** | **string** |  | [optional] 
**input_stream** | [**\Insign\Model\InputStream**](InputStream.md) |  | [optional] 
**open** | **bool** |  | [optional] 
**readable** | **bool** |  | [optional] 
**uri** | [**\Insign\Model\URI**](URI.md) |  | [optional] 
**url** | [**\Insign\Model\URL**](URL.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


