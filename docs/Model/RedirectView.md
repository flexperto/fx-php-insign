# RedirectView

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application_context** | [**\Insign\Model\ApplicationContext**](ApplicationContext.md) |  | [optional] 
**attributes_map** | **object** |  | [optional] 
**bean_name** | **string** |  | [optional] 
**content_type** | **string** |  | [optional] 
**expose_path_variables** | **bool** |  | [optional] 
**hosts** | **string[]** |  | [optional] 
**propagate_query_properties** | **bool** |  | [optional] 
**redirect_view** | **bool** |  | [optional] 
**request_context_attribute** | **string** |  | [optional] 
**static_attributes** | **object** |  | [optional] 
**url** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


