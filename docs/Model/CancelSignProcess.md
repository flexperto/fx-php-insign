# CancelSignProcess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienttype** | **string** |  | 
**docid** | **string** | Dokument ID | 
**dontsendbacktopc** | **bool** |  | [optional] 
**position** | [**\Insign\Model\PosistionDefintionInsideADocument**](PosistionDefintionInsideADocument.md) |  | [optional] 
**sessionid** | **string** | Session ID | 
**signatureid** | **string** | Signature ID | [optional] 
**smartphoneregistered** | **bool** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


