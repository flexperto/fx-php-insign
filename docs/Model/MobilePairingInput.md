# MobilePairingInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**device_display_name** | **string** | Name of the device (optional) | [optional] 
**device_id** | **string** | Device ID (IMEI) of the device (optional) | [optional] 
**device_phone_number** | **string** | Phone number of the device (optional) | [optional] 
**is_applos** | **bool** | Is the device connected without the app? | [optional] 
**is_no_sign** | **bool** | Is the device being paired without signing afterwards? | [optional] 
**pairing_code** | **string** | Pairing code | 
**re_pair_code** | **string** | Code to re-pair permanent without the app with another device | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


