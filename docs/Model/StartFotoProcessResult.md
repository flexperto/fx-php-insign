# StartFotoProcessResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienttype** | **string** | Type of client. smartphone or pc | 
**error** | **int** | Error code 0&#x3D;OK | 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**notification** | **string** | message text | [optional] 
**notificationid** | **string** | ID of the message | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


