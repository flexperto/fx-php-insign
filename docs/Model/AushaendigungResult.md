# AushaendigungResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**abschliesen** | **bool** | Indicates if the hand out occurred when completing the process | [optional] 
**bestaetigt_timestamp** | [**\DateTime**](\DateTime.md) | Indicates when the hand out was confirmed, if a confirmation workflow for hand outs is configured. (Recipient confirms receiption of the documents by clicking a link in the hand out e-mail) | [optional] 
**doc_infos** | [**\Insign\Model\DocumentInfo[]**](DocumentInfo.md) | Returns the DocumentInfo for the documents which were handed out | [optional] 
**email** | **string[]** | Lists the e-mail addresses to which the documents were handed out | [optional] 
**error** | **int** | Error code 0&#x3D;OK | 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**messages** | **map[string,string]** | Error message if error!&#x3D;0 | [optional] 
**sms** | **string[]** | Lists the phone numbers to which the documents were handed out | [optional] 
**timestamp** | [**\DateTime**](\DateTime.md) | Point in time at which the documents were handed out | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 
**typen** | **string[]** | Indicates how the documents were handout out, i.e. e-mail, file, paper, sms | [optional] 
**zip_password** | **string** | Returns the zip file&#39;s password | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


