# DeliveryConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**abgeschlossen_email_betreff** | **string** | The subject of the email that will be sent on completion. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**abgeschlossen_email_inhalt** | **string** | Content of the email that will be sent on completion. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**alle_email_betreff** | **string** | Subject of the email containing all documents. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**alle_email_inhalt** | **string** | Content of the email containing all documents. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**document_email_download** | **bool** | This value determines whether emails should contain a link to download the documents instead of the documents themselves as an attachment. since v2.3 | [optional] 
**email_empfaenger** | **string** | Default Email Recipient. Unless specified otherwise, the value from the user configuration (for example LDAP) or from the application properties is used. | [optional] 
**email_empfaenger_bcc** | **string** | Default value other Email Recipients (BCC). This email is not visible in the graphical interface. If no value is entered here, the value from the application properties is used. | [optional] 
**email_empfaenger_kopie** | **string** | Default value other Email Recipients (CC). Unless specified otherwise, the value from the user configuration (for example LDAP) or from the application properties is used. | [optional] 
**email_empfaenger_read_only** | **bool** | Switches editing of the recipient&#39;s email address to on or off if an address is transferred via interface or admin settings. | [optional] 
**empfaenger_bcc_extern** | **string** | Default value other external email recipients (BCC). This email is not visible in the graphical interface. | [optional] 
**empfaenger_cc_extern** | **string** | Default value other email recipients (CC) for external processing. | [optional] 
**empfaenger_extern** | **string** | Default value Email Recipient for external processing. | [optional] 
**empfaenger_read_only_extern** | **bool** | Switches editing of the external user&#39;s email address to on or off if an address is transferred via interface or admin settings. | [optional] 
**empfaenger_sms** | **string** | Default value SMS recipient. | [optional] 
**empfaenger_sms_extern** | **string** | Default value SMS recipient for external processing. | [optional] 
**erinnerung_email_betreff** | **string** | The subject of the email sent to the customer as a reminder. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**erinnerung_email_inhalt** | **string** | Content of the email sent to the customer as a reminder. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**extern_email_betreff** | **string** | Subject of the email for external processing. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt: | [optional] 
**extern_email_inhalt** | **string** | Content of the email for external processing. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**mustberead_email_betreff** | **string** | Subject of the email containing the required documents. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**mustberead_email_inhalt** | **string** | Content of the email containing the required documents. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**passwort_email_betreff** | **string** | The subject of the password email. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**passwort_email_inhalt** | **string** | Content of the password email. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**reply_to** | **string** | Default value specifying which email replies must be sent to | [optional] 
**return_path** | **string** | Default value to which server an error message should be sent if the email cannot be delivered | [optional] 
**tan_sms_text** | **string** | Content of the SMS containing the transaction number. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**unterschrieben_email_betreff** | **string** | Subject of the first email containing the signed documents. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**unterschrieben_email_inhalt** | **string** | Content of the first email containing the signed documents. Multiple variables can be used in the string. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**zurueckholen_email_betreff** | **string** | The subject of the email sent to the customer when retrieving a process. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 
**zurueckholen_email_inhalt** | **string** | Content of the email sent to the customer when retrieving a process. Available placeholders: &amp;lt;enumref:de.is2.sign.service.rest.json.TemplateVar&amp;gt; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


