# Polling

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**abort** | **bool** |  | [optional] 
**keys** | **string[]** | request keys. Client could send multiple keys by one request. | [optional] 
**notified_key** | **string** | key of the waiting polling object which has been notified. Server side notification use only | [optional] 
**waiting** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


