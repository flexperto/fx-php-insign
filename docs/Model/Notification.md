# Notification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contenthtml** | **string** |  | [optional] 
**contentmd** | **string** |  | [optional] 
**dbid** | **int** |  | [optional] 
**enddate** | [**\DateTime**](\DateTime.md) |  | [optional] 
**ordernum** | **int** |  | [optional] 
**startdate** | [**\DateTime**](\DateTime.md) |  | [optional] 
**userid** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


