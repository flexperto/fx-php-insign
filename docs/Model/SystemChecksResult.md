# SystemChecksResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checkitems** | [**\Insign\Model\SystemCheckItemInfo[]**](SystemCheckItemInfo.md) |  | [optional] 
**error** | **int** |  | [optional] 
**message** | **string** |  | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


