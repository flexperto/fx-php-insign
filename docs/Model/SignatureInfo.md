# SignatureInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance_traveled** | **double** | Signature length | [optional] 
**meters_per_second** | **double** | Speed | [optional] 
**number_of_interruptions** | **int** | Number of interruptions | [optional] 
**samples_per_second** | **double** | Sampling rate per second | [optional] 
**time** | **int** | Time in milliseconds | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


