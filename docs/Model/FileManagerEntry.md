# FileManagerEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**name** | **string** |  | [optional] 
**publicaccessible** | **bool** |  | [optional] 
**size** | **int** |  | [optional] 
**type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


