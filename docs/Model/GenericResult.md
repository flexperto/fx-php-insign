# GenericResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **int** | Error code 0&#x3D;OK | 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**messages** | **map[string,string]** | Error message if error!&#x3D;0 | [optional] 
**result** | **object** | Contains objects as the result | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


