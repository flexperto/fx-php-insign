# DocumentRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**docid** | **string** | The document ID | 
**sessionid** | **string** | The session ID | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


