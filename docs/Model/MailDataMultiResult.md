# MailDataMultiResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **int** | Error code 0&#x3D;OK | 
**mail_data_results** | [**map[string,\Insign\Model\MailDataResult]**](MailDataResult.md) | The list Mail Data for each user | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


