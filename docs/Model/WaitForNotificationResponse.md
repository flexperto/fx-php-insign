# WaitForNotificationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **int** | Error code 0&#x3D;OK | 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**messages** | **map[string,string]** | Error message if error!&#x3D;0 | [optional] 
**notification** | [**\Insign\Model\ClientNotification**](ClientNotification.md) | ID of the message that is responsible for the call | [optional] 
**sessionid** | **string** | Session ID | 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 
**transaktionsnummer** | **string** | Transaction number | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


