# UploadResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**docid** | **string** | Document ID | [optional] 
**error** | **int** | Error code 0&#x3D;OK | 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**success** | **string** | Success message | [optional] 
**trace** | **string** | Stacktrace | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


