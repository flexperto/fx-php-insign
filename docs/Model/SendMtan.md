# SendMtan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**docid** | **string** | Document ID | 
**recipientsms** | **string** | SMS recipient phone number used for the MTAN | [optional] 
**signatureid** | **string** | Signature ID | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


