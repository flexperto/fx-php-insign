# ClientInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**browser_height** | **int** |  | [optional] 
**browser_width** | **int** |  | [optional] 
**location** | [**\Insign\Model\GPSData**](GPSData.md) |  | [optional] 
**screen_height** | **int** |  | [optional] 
**screen_width** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


