# OfflineUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offline_i_ds** | **string[]** |  | [optional] 
**pairing_token** | **string** |  | [optional] 
**user** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


