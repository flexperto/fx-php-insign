# DocumentInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checksum** | **string** | Checksum of the document | [optional] 
**displayname** | **string** | Display name for the document | [optional] 
**file_size** | **int** | File size of the document | [optional] 
**id** | **string** | ID of the document | [optional] 
**number_of_pages** | **int** | Number of pages in the document | [optional] 
**password** | **string** | Password for the document | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


