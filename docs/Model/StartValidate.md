# StartValidate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**docid** | **string** |  | 
**sessionid** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


