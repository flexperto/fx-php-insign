# StatusUndDatenDerQESVideoIdentifikation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**birthday** | [**\DateTime**](\DateTime.md) |  | [optional] 
**city** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**first_names** | **string** |  | [optional] 
**gender** | **string** |  | [optional] 
**last_names** | **string** |  | [optional] 
**mobile_phone** | **string** |  | [optional] 
**nationality** | **string** |  | [optional] 
**place_of_birth** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**street** | **string** |  | [optional] 
**street_number** | **string** |  | [optional] 
**zip_code** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


