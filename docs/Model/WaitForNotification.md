# WaitForNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienttype** | **string** | Type of client. smartphone or pc | 
**new_device** | **bool** | Waiting for pairing with customer device | [optional] 
**pairing_token** | **string** | Pairing token | [optional] 
**sessionid** | **string** | Session ID | 
**smartphoneregistered** | **bool** | Smartphone incicator | [optional] 
**user_id** | **string** | User ID in pairing mode | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


