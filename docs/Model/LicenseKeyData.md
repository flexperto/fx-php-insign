# LicenseKeyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**feature_i_ds** | **string[]** |  | [optional] 
**issueremail** | **string** |  | [optional] 
**signature** | **string** |  | [optional] 
**valid_from** | **string** |  | [optional] 
**valid_until** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


