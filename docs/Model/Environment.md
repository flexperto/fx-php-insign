# Environment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_profiles** | **string[]** |  | [optional] 
**default_profiles** | **string[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


