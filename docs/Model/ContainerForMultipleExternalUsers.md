# ContainerForMultipleExternalUsers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**extern_users** | [**\Insign\Model\StartExtern[]**](StartExtern.md) | List of recipients | 
**in_order** | **bool** | Edit in order? | [optional] 
**keep_exisiting** | **bool** | Remove existing users? | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


