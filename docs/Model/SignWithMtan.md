# SignWithMtan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**docid** | **string** | Dokument-ID | 
**mtan** | **string** | MTAN | 
**signatureid** | **string** | Signatur-ID | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


