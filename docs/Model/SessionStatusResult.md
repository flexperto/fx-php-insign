# SessionStatusResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aushaendigungen** | [**\Insign\Model\AushaendigungResult[]**](AushaendigungResult.md) | Sum of the handouts with additional information. | [optional] 
**document_data** | [**\Insign\Model\DocumentDataStatus[]**](DocumentDataStatus.md) | Metadata list of documents since v3.5.0 | [optional] 
**error** | **int** | Error code 0&#x3D;OK | 
**gdpr_declined** | **bool** | Was the consent to data collection denied by at least one person before signing? If the query is deactivated for the session, then &#39;false&#39; is returned. since v3.8.29 | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**modified_timestamp** | **int** | Last change to the session. | [optional] 
**number_of_mandatory_signature_fields** | **int** | Number of all mandatory signature fields of all documents since v3.1.2 | [optional] 
**number_of_mandatory_signatures** | **int** | Number of all already signed mandatory signature fields of all documents since v3.1.2 | [optional] 
**number_of_optional_signature_fields** | **int** | Number of all optional signature fields of all documents since v3.1.2 | [optional] 
**number_of_optional_signatures** | **int** | Number of all already signed optional signature fields (for all documents) since v3.1.2 | [optional] 
**number_of_signatures** | **int** | Number of signatures already completed for all documents | [optional] 
**number_of_signatures_fields** | **int** | Number of all signature fields (optional and mandatory) for all documents | [optional] 
**qes_status** | [**\Insign\Model\StatusUndDatenDerQESVideoIdentifikation**](StatusUndDatenDerQESVideoIdentifikation.md) | Status and data of the QES / video identification | [optional] 
**signatur_fields_status_list** | [**\Insign\Model\SignatureFieldStatus[]**](SignatureFieldStatus.md) | List of status information for all signature fields since v3.1.2 | [optional] 
**sucessfully_completed** | **bool** | Has the process been completed successfully? I.e. Everything was read and signed and \&quot;Finish\&quot; was clicked. If you chose &#39;prefer to read on paper&#39; or aborted, then this will be false. | [optional] 
**trace** | **string** | Stack Trace if exceptions have occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


