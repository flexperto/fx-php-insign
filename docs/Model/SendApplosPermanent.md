# SendApplosPermanent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienttype** | **string** | Type of client: smartphone or smartphonekunde | [optional] 
**pairing_token** | **string** | Pairing token or mobile ID which the device received during pairing | [optional] 
**recipient** | **string** | E-mail address of the recipient | [optional] 
**recipientbcc** | **string** | E-mail address of the BCC recipient | [optional] 
**recipientcc** | **string** | E-mail address of the CC recipient | [optional] 
**recipientsms** | **string** | Phone number of the SMS recipient | [optional] 
**sender** | **string** | Sender of the e-mail | [optional] 
**sessionid** | **string** | Session ID | [optional] 
**user_id** | **string** | User ID which the device received during pairing | [optional] 
**vorgangsverwaltung** | **bool** | Is the resend link sent from the process manager | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


