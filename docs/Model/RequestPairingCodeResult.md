# RequestPairingCodeResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | Pairing Code | 
**error** | **int** | Error code 0&#x3D;OK | 
**lifetime** | **int** | Pairing Code lifetime in milliseconds | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**messages** | **map[string,string]** | Error message if error!&#x3D;0 | [optional] 
**mtan_lifetime** | **int** | MTAN lifetime in milliseconds | [optional] 
**send_applos_mail** | **bool** |  | [optional] 
**send_applos_sms** | **bool** |  | [optional] 
**send_mtan** | **bool** |  | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


