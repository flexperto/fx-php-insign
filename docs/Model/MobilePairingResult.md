# MobilePairingResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **int** | Error code 0&#x3D;OK | 
**is_no_sign** | **bool** | Is the device being paired without signing afterwards? | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**messages** | **map[string,string]** | Error message if error!&#x3D;0 | [optional] 
**pairing_token** | **string** | Pairing token for future sign ins | 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 
**user_id** | **string** | User ID for future sign ins | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


