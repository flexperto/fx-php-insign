# ExternUserResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **int** | Error code 0&#x3D;OK | 
**extern_access_link** | **string** | The link with which the external user can access the session | [optional] 
**extern_user** | **string** | The e-mail address of the user for authentication | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**messages** | **map[string,string]** | Error message if error!&#x3D;0 | [optional] 
**order_number** | **int** | Index number to determine the order | [optional] 
**password** | **string** | The password passed by the API or the automatically generated password | [optional] 
**send_emails** | **bool** | Flag if mails will be sent or not | [optional] 
**token** | **string** | One time token with which the user is signed in, must be renewed (/extern/updateToken) to log in again | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 
**usermessage** | **string** | Message for the user | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


