# Delivery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**abschliesen** | **bool** |  | [optional] 
**docids** | **string[]** |  | [optional] 
**email** | **string[]** |  | [optional] 
**timestamp** | [**\DateTime**](\DateTime.md) |  | [optional] 
**typen** | **string[]** |  | [optional] 
**user_ip** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


