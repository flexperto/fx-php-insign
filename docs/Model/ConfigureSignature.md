# ConfigureSignature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**anrede** | **string** | Form of address for this signature field | [optional] 
**displayname** | **string** | Functional role for the signature (Who is supposed to sign here). The text is displayed on the signature pad and in acrobat reader. If empty the technical role will be used here. | [optional] 
**gebdat** | [**\DateTime**](\DateTime.md) | Date of birth for this signature field | [optional] 
**id** | **string** | Signatur ID, must be distinct per document | 
**nachname** | **string** | Last name for this signature field | [optional] 
**ort** | **string** | Place for this signature field | [optional] 
**posindex** | **int** | Order of the signatures in the PDF and for the client(UI). Spaces or duplicates aren&#39;t a problem. The position (page,y,x) is used as a secondary criteria for sorting | [optional] 
**position** | [**\Insign\Model\PosistionDefintionInsideADocument**](PosistionDefintionInsideADocument.md) | Position of the signature in the PDF by reference or coordinates | [optional] 
**required** | **bool** | Is the signature required? | [optional] 
**role** | **string** | Technical role for the signature (Who is supposed to sign here) | [optional] 
**textsearch** | **string** | Scan the documents content for this text and re-calculate the positions and number of fields according to to matches. The meaning of coordinates given in &#39;position&#39; will change in this case. These x0 and y0 are now meant to be offsets to the match-positions lower left corner. Use trailing and/or leading spaces to avoid &#39;N2&#39; to be found in &#39;N25&#39;. The value must be a valid Java regular expression. See more Info regarding this feature at https://confluence.is2.de/display/IKB/Documentation+for+external+requests | [optional] 
**vorname** | **string** | First name for this signature field | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


