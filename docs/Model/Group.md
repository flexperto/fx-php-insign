# Group

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**common_name** | **string** |  | [optional] 
**existing_common_name** | **string** |  | [optional] 
**members** | **string[]** |  | [optional] 
**short_name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


