# SetSessionVar

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_annotation_id** | **string** |  | [optional] 
**set_image_annotation_id** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


