# GPSData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accuracy** | **double** | Accuracy in meters | [optional] 
**altitude** | **double** | Altitude above sea level in meters | [optional] 
**altitude_accuracy** | **double** | Accuracy of the altitude in meters | [optional] 
**heading** | **double** | Direction of movement | [optional] 
**latitude** | **double** | Latitude | [optional] 
**longitude** | **double** | Longitude | [optional] 
**speed** | **double** | Movement speed | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


