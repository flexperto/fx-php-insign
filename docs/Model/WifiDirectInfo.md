# WifiDirectInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** |  | [optional] 
**can_be_activated** | **bool** |  | [optional] 
**connected** | **bool** |  | [optional] 
**connectedpeer** | [**\Insign\Model\PeerInfo**](PeerInfo.md) |  | [optional] 
**discoveredpeers** | [**\Insign\Model\PeerInfo[]**](PeerInfo.md) |  | [optional] 
**error** | **int** |  | [optional] 
**found** | **bool** |  | [optional] 
**retrycount** | **int** |  | [optional] 
**searchpeer** | [**\Insign\Model\PeerInfo**](PeerInfo.md) |  | [optional] 
**status** | **string** |  | [optional] 
**trace** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


