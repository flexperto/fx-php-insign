# PhotoUpload

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayname** | **string** | Display name | [optional] 
**filename** | **string** | Filename | [optional] 
**filesize** | **int** | Size of the file | [optional] 
**image** | **string** | Image as base64 | [optional] 
**pairing_token** | **string** | Token of the paired device | [optional] 
**photoid** | **string** | Image ID | [optional] 
**productid** | **string** | Optional Product ID | [optional] 
**sessionid** | **string** | Session ID | 
**uploader** | **string** | Name of the uploader | [optional] 
**user** | **string** | User ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


