# PairedMobileResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**device_id** | **string** | Device ID (IMEI) of the device | [optional] 
**display_name** | **string** | Display name for the device | [optional] 
**email** | **string** | Email Id used to connect in case of permanent pairing without the app | [optional] 
**id** | **string** | ID of the device | [optional] 
**inital_creation** | **string** | Time of pairing | [optional] 
**is_applos** | **bool** | Is paired without the app? | [optional] 
**last_used** | **string** | Time when the device was last used with inSign | [optional] 
**phone_number** | **string** | Mobile number used to connect in case of permanent pairing without the app | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


