# EditorDefaultSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **string** |  | [optional] 
**languages** | [**map[string,\Insign\Model\Messages]**](Messages.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


