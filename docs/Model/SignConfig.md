# SignConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_kunde_support** | **bool** | This value determines whether the customer can sign via the inSign app. | [optional] 
**app_support** | **bool** | Allow use of the inSign Mobile App for the session owner? | [optional] 
**applos_foto_kunde_mail_betreff** | **string** | E-mail subject for the e-mail containing the link for taking pictures without the inSign app. &lt;br&gt; Here you can use the same variables as with the flag \&quot;applosMailText\&quot;. | [optional] 
**applos_foto_kunde_mail_text** | **string** | E-mail text for the email containing the link for taking pictures without the inSign app. &lt;br&gt; Here you can use the same variables as with the flag \&quot;applosMailText\&quot;. | [optional] 
**applos_foto_kunde_sms_text** | **string** | SMS text for the SMS containing the link for taking pictures without the inSign app. &lt;br&gt; The same variables are available as with the \&quot;applosMailText\&quot; flag, and the rules for the link are the same as for the \&quot;applosMailText\&quot; flag. | [optional] 
**applos_foto_mail_betreff** | **string** | E-mail subject for the e-mail containing the link for taking pictures without the inSign app. &lt;br&gt; Here you can use the same variables as with the flag \&quot;applosMailText\&quot;. | [optional] 
**applos_foto_mail_text** | **string** | E-mail text for the email containing the link for taking pictures without the inSign app. &lt;br&gt; Here you can use the same variables as with the flag \&quot;applosMailText\&quot;. | [optional] 
**applos_foto_sms_text** | **string** | SMS text for the SMS containing the link for taking pictures without the inSign app. &lt;br&gt; The same variables are available as with the \&quot;applosMailText\&quot; flag, and the rules for the link are the same as for the \&quot;applosMailText\&quot; flag. | [optional] 
**applos_kunde_mail_betreff** | **string** | E-mail subject for the e-mail containing the link for signing without the inSign app. &lt;br&gt; Here you can use the same variables as with the flag \&quot;applosMailText\&quot;. | [optional] 
**applos_kunde_mail_text** | **string** | E-mail text for the e-mail to the customer in which the link for signing without inSign app is included.&lt;br&gt;The following placeholders can be used in this text.&lt;br&gt;${$RootUrl} - The URL of the insign-Installation (eg.: http://localhost:8080/insign).&lt;br&gt;${$PairingCode} - The code for pairing between inSign and Browser.&lt;br&gt;${$SessionName} - The name of the process (displayname).&lt;br&gt;${$KundeName} - The name of the customer.&lt;br&gt;${$BeraterName} - The name of the consultant (Session owner).&lt;br&gt;${$SignAppLink} - Link for the applos call.To use the placeholders in the text, the following syntax must be followed: ${&amp;lt;Variable&amp;gt;} | [optional] 
**applos_kunde_per_email** | **bool** | This value determines whether the customer can sign without inSign app via e-mail. | [optional] 
**applos_kunde_per_email_empfaenger** | **string** | E-mail address of the customer for signature without inSign app. | [optional] 
**applos_kunde_per_sms** | **bool** | This value determines whether the customer can sign without inSign app via sms. | [optional] 
**applos_kunde_per_sms_empfaenger** | **string** | Telephone number of the customer for signature without inSign-App via SMS. | [optional] 
**applos_kunde_sms_text** | **string** | SMS text for the SMS in which the link for signing without inSign app is included. &lt;br&gt; The same variables are available as with the \&quot;applosMailText\&quot; flag, and the rules for the link are the same as for the \&quot;applosMailText\&quot; flag. | [optional] 
**applos_mail_betreff** | **string** | E-mail subject for the e-mail containing the link for signing without the inSign app. &lt;br&gt; Here you can use the same variables as with the flag \&quot;applosMailText\&quot;. | [optional] 
**applos_mail_text** | **string** | Email body for signature without the app by email mode&lt;br&gt;The following placeholders may be used:&lt;br&gt;${$RootUrl} - inSign webapp root URL (e.g.: http://localhost:8080/insign).&lt;br&gt;${$PairingCode} - the supplied pairing code.&lt;br&gt;${$SessionName} - the session name (i.e. displayname).&lt;br&gt;${$KundeName} - customer full name.&lt;br&gt;${$BeraterName} - session owner full name.&lt;br&gt;${$SignAppLink} - the supplied link.Use the follwing syntax for the placeholders: ${&amp;lt;Variable&amp;gt;} | [optional] 
**applos_per_email** | **bool** | Allow use of signature without the app mode via email for the session owner? | [optional] 
**applos_per_email_empfaenger** | **string** | Session owner email address for signature without the app by email mode | [optional] 
**applos_per_sms** | **bool** | Allow use of the signature without the app mode via SMS for the session owner? | [optional] 
**applos_per_sms_empfaenger** | **string** | Session owner mobile number for signature without the app by sms mode | [optional] 
**applos_per_touch** | **bool** | Override touch detection for the session owner? If set, the session owner must use a touch deveice to sign directly | [optional] 
**applos_sms_text** | **string** | SMS text for the SMS in which the link for signing without the inSign app is included. &lt;br&gt; The same variables are available as with the \&quot;applosMailText\&quot; flag, and the rules for the link are the same as for the \&quot;applosMailText\&quot; flag. | [optional] 
**berater_pairing_temporary** | **bool** | Determines if the consultant phone pairing only stays active for the session. Default is false. | [optional] 
**upad_support** | **bool** | Allow use of Hardware-Signature-Pad? | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


