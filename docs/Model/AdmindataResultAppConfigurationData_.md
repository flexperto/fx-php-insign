# AdmindataResultAppConfigurationData_

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_data** | [**\Insign\Model\AppConfigurationData**](AppConfigurationData.md) |  | [optional] 
**error** | **int** |  | [optional] 
**message** | **string** |  | [optional] 
**trace** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


