# ConfigurationForTheQualifiedElectronicSignature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**birthday** | [**\DateTime**](\DateTime.md) | Birthday | [optional] 
**city** | **string** | City | [optional] 
**country** | **string** | Country (according to ISO 3166 - ALPHA 2) | [optional] 
**default_position** | [**\Insign\Model\PosistionDefintionInsideADocument**](PosistionDefintionInsideADocument.md) | Default position for QES. Gets overriden by specific document position | [optional] 
**email** | **string** | Email Address | [optional] 
**first_names** | **string** | First name(s) | [optional] 
**gender** | **string** | Gender: male/female | [optional] 
**last_names** | **string** | Surname(s) | [optional] 
**mobile_phone** | **string** | Mobile Number | [optional] 
**nationality** | **string** | Nationality (according to ISO 3166 - ALPHA 2) | [optional] 
**password** | **string** | Password for the user | [optional] 
**place_of_birth** | **string** | Place of birth | [optional] 
**positions** | [**map[string,\Insign\Model\PosistionDefintionInsideADocument]**](PosistionDefintionInsideADocument.md) | Position of the QES in each document. If neither this nor a defaultPosisition is set, an invisible QES will be placed in the document | [optional] 
**qes_type** | **string** | To enable the QES set to &#39;QES&#39;, to use only Video Ident &#39;IDENT&#39;. Disabled by setting to &#39;NONE&#39;. | 
**street** | **string** | Street | [optional] 
**street_number** | **string** | House number | [optional] 
**username** | **string** | Username for the login | [optional] 
**zip_code** | **string** | Postal Code | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


