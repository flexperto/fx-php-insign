# ListOfSessionIDs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionids** | **string[]** | List of Session IDs | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


