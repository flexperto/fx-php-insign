# SetWifiID

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pairing_token** | **string** | Pairing token which the device received during paring | 
**user_id** | **string** | User ID which the device received during paring | 
**wifi_id** | **string** | Wifi-Direct-ID of the device | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


