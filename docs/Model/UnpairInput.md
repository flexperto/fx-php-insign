# UnpairInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pairing_token** | **string** | Pairing token or Mobile ID which the device received when pairing | [optional] 
**sessionid** | **string** | Session ID | [optional] 
**user_id** | **string** | User ID which the device received while pairing | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


