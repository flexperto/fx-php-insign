# PagePosition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fieldref** | **string** |  | [optional] 
**h** | **double** |  | [optional] 
**page** | **int** |  | [optional] 
**w** | **double** |  | [optional] 
**x0** | **double** |  | [optional] 
**y0** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


