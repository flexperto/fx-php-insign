# CheckboxTagConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checked** | **bool** |  | [optional] 
**height** | **int** |  | [optional] 
**required** | **bool** |  | [optional] 
**tag** | **string** |  | [optional] 
**width** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


