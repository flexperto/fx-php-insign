# InformationAboutASession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**completed** | **bool** | Completed | [optional] 
**created** | [**\DateTime**](\DateTime.md) | Time of creation of the session. Human readable Format | [optional] 
**created_date** | **string** | Time of creation of the session | [optional] 
**created_time** | **string** | Time of creation of the session | [optional] 
**created_timestamp** | **int** | Time of creation of the session | [optional] 
**displayname** | **string** | Displayname for the session | 
**docids** | **string[]** | Document IDs of the Documents in this Session | [optional] 
**error** | **int** | Error code 0&#x3D;OK | 
**extern** | **bool** | In external editing | [optional] 
**extern_enabled** | **bool** | Can the process be made available for external editing | [optional] 
**in_sign_version** | **string** | inSign version | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**messages** | **map[string,string]** | Error message if error!&#x3D;0 | [optional] 
**modified** | [**\DateTime**](\DateTime.md) | Last modification to the session. Human readable Format | [optional] 
**modified_date** | **string** | Last modification to the session | [optional] 
**modified_time** | **string** | Last modification to the session | [optional] 
**modified_timestamp** | **int** | Last modification to the session | [optional] 
**number_of_documents** | **int** | Number of documents in this session | [optional] 
**number_of_signatures** | **int** | Number of signatures which were placed | [optional] 
**number_of_signatures_needed** | **int** | Number of required signature fields | [optional] 
**number_of_signatures_needed_done** | **int** | Number of required signatures which were placed | [optional] 
**number_of_signatures_needed_with_optional** | **int** | Number of signature fields (required and non required) | [optional] 
**offline** | **bool** | In offline editing on a device | [optional] 
**process_step** | **string** | Last process step | [optional] 
**session_size** | **int** | The size of the binary document files in the session | [optional] 
**sessionid** | **string** | Session ID of the session | 
**sms_versand_enabled** | **bool** | SMS sending enabled | [optional] 
**thumb_nail** | **string** | Session Thumbnail Base64 encoded image | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 
**transaktionsnummer** | **string** | transaction number | [optional] 
**user** | **string** | User ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


