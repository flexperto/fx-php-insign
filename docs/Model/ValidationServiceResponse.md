# ValidationServiceResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **int** |  | [optional] 
**fields** | **map[string,string]** | &lt;Feldid, Fehlermeldung&gt; | [optional] 
**message** | **string** |  | [optional] 
**no_skip** | **bool** |  | [optional] 
**warn_fields** | **map[string,string]** | &lt;Field ID, Waring message&gt; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


