# ClientNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienttype** | **string** |  | [optional] 
**lifetime** | **int** |  | [optional] 
**notification** | **string** |  | [optional] 
**notification_title** | **string** |  | [optional] 
**notificationid** | **string** |  | [optional] 
**ready** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


