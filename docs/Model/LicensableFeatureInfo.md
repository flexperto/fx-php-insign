# LicensableFeatureInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additional_feature_i_ds** | **string[]** |  | [optional] 
**feature_description** | **string** |  | [optional] 
**feature_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


