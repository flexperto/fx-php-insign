# SendApplos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienttype** | **string** | Type of client: smartphone or smartphonekunde | 
**function** | **string** | Functionality for which the pairing will be used. Sign or Foto | [optional] 
**pairing_code** | **string** | Pairing code | [optional] 
**recipient** | **string** | E-mail address of the recipient | [optional] 
**recipientbcc** | **string** | E-mail address of the BCC recipient | [optional] 
**recipientcc** | **string** | E-mail address of the CC recipient | [optional] 
**recipientsms** | **string** | Phone number of the SMS recipient | [optional] 
**sender** | **string** | Sender of the e-mail | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


