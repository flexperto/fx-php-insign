# OfflineSyncInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pairing_token** | **string** |  | [optional] 
**session_list** | [**map[string,\Insign\Model\SessionData]**](SessionData.md) |  | [optional] 
**user** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


