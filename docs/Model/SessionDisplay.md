# SessionDisplay

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displaycustomer** | **string** | Display name for the customer | [optional] 
**displayname** | **string** | Display name | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


