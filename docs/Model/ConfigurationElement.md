# ConfigurationElement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default_value** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**internal** | **bool** |  | [optional] 
**key** | **string** |  | [optional] 
**pattern** | **string** |  | [optional] 
**possible_values** | **string[]** |  | [optional] 
**tag** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**value** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


