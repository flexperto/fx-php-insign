# GuiPropertiesResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default_value** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**error** | **int** | Error code 0&#x3D;OK | 
**key** | **string** |  | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**pattern** | **string** |  | [optional] 
**possible_values** | **string[]** |  | [optional] 
**sessionid** | **string** | The session ID of the newly generared session | [optional] 
**tag** | **string** |  | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 
**type** | **string** |  | [optional] 
**value** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


