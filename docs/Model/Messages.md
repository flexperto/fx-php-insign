# Messages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_maximum_incl** | **string** |  | [optional] 
**error_minimum_incl** | **string** |  | [optional] 
**error_notempty** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


