# RequestInfoForSignature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**doc_id** | **string** |  | [optional] 
**sessionid** | **string** |  | [optional] 
**signature_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


