# SummaryResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ausgehaendigt** | [**\Insign\Model\Delivery[]**](Delivery.md) |  | [optional] 
**documents_abgeschlossen** | [**\Insign\Model\DocumentInfo[]**](DocumentInfo.md) |  | [optional] 
**documents_ausgehaendigt** | [**\Insign\Model\DocumentInfo[]**](DocumentInfo.md) |  | [optional] 
**error** | **int** |  | [optional] 
**message** | **string** |  | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


