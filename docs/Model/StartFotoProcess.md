# StartFotoProcess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**annotation_id** | **string** |  | [optional] 
**clienttype** | **string** |  | 
**document** | **string** |  | [optional] 
**reset_annotation_id** | **bool** |  | [optional] 
**sessionid** | **string** | Session ID | 
**smartphoneregistered** | **bool** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


