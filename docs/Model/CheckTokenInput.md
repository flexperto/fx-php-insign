# CheckTokenInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pairing_token** | **string** | Pairing token which the device received during paring | 
**user_id** | **string** | User ID which the device received during paring | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


