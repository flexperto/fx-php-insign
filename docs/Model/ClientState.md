# ClientState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** |  | [optional] 
**events** | [**map[string,\Insign\Model\ClientEvent[]]**](array.md) |  | [optional] 
**lastactive** | [**\DateTime**](\DateTime.md) |  | [optional] 
**pushnotificationstoken** | **string** |  | [optional] 
**sessionid** | **string** |  | [optional] 
**start_url** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


