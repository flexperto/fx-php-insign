# StartSignProcess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienttype** | **string** |  | 
**docid** | **string** | Document ID | 
**position** | [**\Insign\Model\PosistionDefintionInsideADocument**](PosistionDefintionInsideADocument.md) |  | [optional] 
**sessionid** | **string** | Session ID | 
**signatureid** | **string** | Signature ID | [optional] 
**smartphoneregistered** | **bool** |  | 
**userid** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


