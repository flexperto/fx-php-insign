# CheckStatusResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ausfuellbar** | **bool** | Does this session contain fillable fields? | [optional] 
**completed** | **bool** | Was the process completed successfully? I.e. everything was signed and read and finish was clicked. If &#39;read on paper&#39; is selected or the process is cancelled this value is false. | [optional] 
**dsgvo_declined** | **string** | Has the GDPR consent been declined? | [optional] 
**error** | **int** | Error code 0&#x3D;OK | 
**extern** | **bool** | Is the process being edited by another user? | [optional] 
**in_qes** | **bool** | Is process in QES mode? | [optional] 
**message** | **string** | Error message if error!&#x3D;0 | [optional] 
**number_of_signatures_needed** | **int** | Number of required signature fields | [optional] 
**number_of_signatures_needed_done** | **int** | Number of placed required signatures | [optional] 
**offline** | **bool** | Is the process being edited offline on another device? | [optional] 
**offline_available** | **bool** | Is the process available for offline editing? | [optional] 
**process_step** | **string** | The process step the session is currently in | [optional] 
**qes_result_preliminary** | **bool** | Is QES not yet final? | [optional] 
**sessionid** | **string** | Session ID of the session | [optional] 
**status** | **string** | Status that is displayed in the process manager | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


