# GWGInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document** | **string** | The ID photos taken in PDF format encoded as Base64. Null if no photos were taken | [optional] 
**error** | **int** | Error code 0&#x3D;OK | [optional] 
**has_all_images** | **bool** | Were the required ID photos taken? | [optional] 
**message** | **string** | Stack trace if exceptions occurred | [optional] 
**session_completed** | **bool** | Was the session to which the document belongs completed? | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


