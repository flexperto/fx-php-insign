# MailDataResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **int** |  | [optional] 
**mail_language** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**note** | **string** |  | [optional] 
**recipient** | **string** |  | [optional] 
**recipientbcc** | **string** |  | [optional] 
**recipientcc** | **string** |  | [optional] 
**recipientsms** | **string** |  | [optional] 
**salutation** | **string** |  | [optional] 
**subject** | **string** |  | [optional] 
**text** | **string** |  | [optional] 
**trace** | **string** | Stack trace if exceptions occurred | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


