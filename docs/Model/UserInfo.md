# UserInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customizing** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**full_name** | **string** |  | [optional] 
**id** | **string** |  | [optional] 
**language** | **string** |  | [optional] 
**meta_info** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**roles** | **string** |  | [optional] 
**surname** | **string** |  | [optional] 
**title** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


