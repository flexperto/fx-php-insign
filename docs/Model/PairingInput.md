# PairingInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienttype** | **string** |  | 
**code** | **string** | existing pairing code for query pairing code info | [optional] 
**doc_id** | **string** | Document ID | [optional] 
**is_no_sign** | **bool** | Pairing without signing | [optional] 
**sessionid** | **string** | Session ID | 
**signature_id** | **string** | Signature ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


