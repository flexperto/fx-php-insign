# Schema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref** | **string** |  | [optional] 
**schema** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**disallow** | [**\Insign\Model\Schema[]**](Schema.md) |  | [optional] 
**extends** | [**\Insign\Model\Schema[]**](Schema.md) |  | [optional] 
**id** | **string** |  | [optional] 
**readonly** | **bool** |  | [optional] 
**required** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


