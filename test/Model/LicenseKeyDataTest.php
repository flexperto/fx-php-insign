<?php
/**
 * LicenseKeyDataTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * inSign
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.21.2 build:1
 * Contact: insign-support@is2.de
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Insign;

/**
 * LicenseKeyDataTest Class Doc Comment
 *
 * @category    Class
 * @description LicenseKeyData
 * @package     Insign
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LicenseKeyDataTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LicenseKeyData"
     */
    public function testLicenseKeyData()
    {
    }

    /**
     * Test attribute "company"
     */
    public function testPropertyCompany()
    {
    }

    /**
     * Test attribute "email"
     */
    public function testPropertyEmail()
    {
    }

    /**
     * Test attribute "feature_i_ds"
     */
    public function testPropertyFeatureIDs()
    {
    }

    /**
     * Test attribute "issueremail"
     */
    public function testPropertyIssueremail()
    {
    }

    /**
     * Test attribute "signature"
     */
    public function testPropertySignature()
    {
    }

    /**
     * Test attribute "valid_from"
     */
    public function testPropertyValidFrom()
    {
    }

    /**
     * Test attribute "valid_until"
     */
    public function testPropertyValidUntil()
    {
    }
}
