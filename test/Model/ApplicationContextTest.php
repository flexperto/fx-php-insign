<?php
/**
 * ApplicationContextTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * inSign
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.21.2 build:1
 * Contact: insign-support@is2.de
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Insign;

/**
 * ApplicationContextTest Class Doc Comment
 *
 * @category    Class
 * @description ApplicationContext
 * @package     Insign
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ApplicationContextTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ApplicationContext"
     */
    public function testApplicationContext()
    {
    }

    /**
     * Test attribute "application_name"
     */
    public function testPropertyApplicationName()
    {
    }

    /**
     * Test attribute "autowire_capable_bean_factory"
     */
    public function testPropertyAutowireCapableBeanFactory()
    {
    }

    /**
     * Test attribute "bean_definition_count"
     */
    public function testPropertyBeanDefinitionCount()
    {
    }

    /**
     * Test attribute "bean_definition_names"
     */
    public function testPropertyBeanDefinitionNames()
    {
    }

    /**
     * Test attribute "class_loader"
     */
    public function testPropertyClassLoader()
    {
    }

    /**
     * Test attribute "display_name"
     */
    public function testPropertyDisplayName()
    {
    }

    /**
     * Test attribute "environment"
     */
    public function testPropertyEnvironment()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "parent"
     */
    public function testPropertyParent()
    {
    }

    /**
     * Test attribute "parent_bean_factory"
     */
    public function testPropertyParentBeanFactory()
    {
    }

    /**
     * Test attribute "startup_date"
     */
    public function testPropertyStartupDate()
    {
    }
}
