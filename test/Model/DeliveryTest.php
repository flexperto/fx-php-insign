<?php
/**
 * DeliveryTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * inSign
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.21.2 build:1
 * Contact: insign-support@is2.de
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Insign;

/**
 * DeliveryTest Class Doc Comment
 *
 * @category    Class
 * @description Delivery
 * @package     Insign
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class DeliveryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Delivery"
     */
    public function testDelivery()
    {
    }

    /**
     * Test attribute "abschliesen"
     */
    public function testPropertyAbschliesen()
    {
    }

    /**
     * Test attribute "docids"
     */
    public function testPropertyDocids()
    {
    }

    /**
     * Test attribute "email"
     */
    public function testPropertyEmail()
    {
    }

    /**
     * Test attribute "timestamp"
     */
    public function testPropertyTimestamp()
    {
    }

    /**
     * Test attribute "typen"
     */
    public function testPropertyTypen()
    {
    }

    /**
     * Test attribute "user_ip"
     */
    public function testPropertyUserIp()
    {
    }
}
