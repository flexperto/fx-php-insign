<?php
/**
 * SendDocumentsTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * inSign
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.21.2 build:1
 * Contact: insign-support@is2.de
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Insign;

/**
 * SendDocumentsTest Class Doc Comment
 *
 * @category    Class
 * @description SendDocuments
 * @package     Insign
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SendDocumentsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "SendDocuments"
     */
    public function testSendDocuments()
    {
    }

    /**
     * Test attribute "client_info"
     */
    public function testPropertyClientInfo()
    {
    }

    /**
     * Test attribute "customer_name"
     */
    public function testPropertyCustomerName()
    {
    }

    /**
     * Test attribute "datei"
     */
    public function testPropertyDatei()
    {
    }

    /**
     * Test attribute "extern_users"
     */
    public function testPropertyExternUsers()
    {
    }

    /**
     * Test attribute "mail_language"
     */
    public function testPropertyMailLanguage()
    {
    }

    /**
     * Test attribute "note"
     */
    public function testPropertyNote()
    {
    }

    /**
     * Test attribute "papier"
     */
    public function testPropertyPapier()
    {
    }

    /**
     * Test attribute "qes_mail_type"
     */
    public function testPropertyQesMailType()
    {
    }

    /**
     * Test attribute "qes_start_link"
     */
    public function testPropertyQesStartLink()
    {
    }

    /**
     * Test attribute "recipient"
     */
    public function testPropertyRecipient()
    {
    }

    /**
     * Test attribute "recipientbcc"
     */
    public function testPropertyRecipientbcc()
    {
    }

    /**
     * Test attribute "recipientcc"
     */
    public function testPropertyRecipientcc()
    {
    }

    /**
     * Test attribute "recipientsms"
     */
    public function testPropertyRecipientsms()
    {
    }

    /**
     * Test attribute "salutation"
     */
    public function testPropertySalutation()
    {
    }

    /**
     * Test attribute "send_fertig"
     */
    public function testPropertySendFertig()
    {
    }

    /**
     * Test attribute "send_mustberead"
     */
    public function testPropertySendMustberead()
    {
    }

    /**
     * Test attribute "sender"
     */
    public function testPropertySender()
    {
    }

    /**
     * Test attribute "smsonly"
     */
    public function testPropertySmsonly()
    {
    }

    /**
     * Test attribute "subject"
     */
    public function testPropertySubject()
    {
    }

    /**
     * Test attribute "vorgang_name"
     */
    public function testPropertyVorgangName()
    {
    }
}
