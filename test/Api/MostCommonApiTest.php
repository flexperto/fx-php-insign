<?php
/**
 * MostCommonApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * inSign
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.21.2 build:1
 * Contact: insign-support@is2.de
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Insign;

use \Insign\Configuration;
use \Insign\ApiException;
use \Insign\ObjectSerializer;

/**
 * MostCommonApiTest Class Doc Comment
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class MostCommonApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for configuredocumentsUsingPOST
     *
     * Creates a new session with the provided metadata for the documents.
     *
     */
    public function testConfiguredocumentsUsingPOST()
    {
    }

    /**
     * Test case for createVorgangsverwaltungSessionUsingGET
     *
     * Creates a new session for process management.
     *
     */
    public function testCreateVorgangsverwaltungSessionUsingGET()
    {
    }

    /**
     * Test case for createVorgangsverwaltungSessionUsingPOST
     *
     * Creates a new session for process management.
     *
     */
    public function testCreateVorgangsverwaltungSessionUsingPOST()
    {
    }

    /**
     * Test case for deleteSessionUsingDELETE
     *
     * Mark session as deleted.
     *
     */
    public function testDeleteSessionUsingDELETE()
    {
    }

    /**
     * Test case for deleteSessionUsingGET
     *
     * Mark session as deleted.
     *
     */
    public function testDeleteSessionUsingGET()
    {
    }

    /**
     * Test case for deleteSessionUsingPOST
     *
     * Mark session as deleted.
     *
     */
    public function testDeleteSessionUsingPOST()
    {
    }

    /**
     * Test case for deleteUserAndSessionsUsingDELETE
     *
     * Remove a user.
     *
     */
    public function testDeleteUserAndSessionsUsingDELETE()
    {
    }

    /**
     * Test case for deleteUserAndSessionsUsingGET
     *
     * Remove a user.
     *
     */
    public function testDeleteUserAndSessionsUsingGET()
    {
    }

    /**
     * Test case for deleteUserAndSessionsUsingPOST
     *
     * Remove a user.
     *
     */
    public function testDeleteUserAndSessionsUsingPOST()
    {
    }

    /**
     * Test case for deletedocumentUsingDELETE
     *
     * Remove a document from a session.
     *
     */
    public function testDeletedocumentUsingDELETE()
    {
    }

    /**
     * Test case for deletedocumentUsingGET
     *
     * Remove a document from a session.
     *
     */
    public function testDeletedocumentUsingGET()
    {
    }

    /**
     * Test case for deletedocumentUsingPOST
     *
     * Remove a document from a session.
     *
     */
    public function testDeletedocumentUsingPOST()
    {
    }

    /**
     * Test case for downloadDocumentsMultiUsingGET1
     *
     * Download several session documents as zips in zip file.
     *
     */
    public function testDownloadDocumentsMultiUsingGET1()
    {
    }

    /**
     * Test case for downloadDocumentsMultiUsingPOST1
     *
     * Download several session documents as zips in zip file.
     *
     */
    public function testDownloadDocumentsMultiUsingPOST1()
    {
    }

    /**
     * Test case for downloadDocumentsProtectedUsingGET
     *
     * Retrieve all sessions documents as single zip file.
     *
     */
    public function testDownloadDocumentsProtectedUsingGET()
    {
    }

    /**
     * Test case for downloadDocumentsProtectedUsingPOST
     *
     * Retrieve all sessions documents as single zip file.
     *
     */
    public function testDownloadDocumentsProtectedUsingPOST()
    {
    }

    /**
     * Test case for downloadDocumentsUsingGET2
     *
     * Download all sessions documents as single zip file.
     *
     */
    public function testDownloadDocumentsUsingGET2()
    {
    }

    /**
     * Test case for downloadDocumentsUsingPOST2
     *
     * Download all sessions documents as single zip file.
     *
     */
    public function testDownloadDocumentsUsingPOST2()
    {
    }

    /**
     * Test case for getBlobDataUsingGET
     *
     * Get session custom blob data.
     *
     */
    public function testGetBlobDataUsingGET()
    {
    }

    /**
     * Test case for getBlobDataUsingPOST
     *
     * Get session custom blob data.
     *
     */
    public function testGetBlobDataUsingPOST()
    {
    }

    /**
     * Test case for getCheckStatusUsingGET
     *
     * Get session status information.
     *
     */
    public function testGetCheckStatusUsingGET()
    {
    }

    /**
     * Test case for getCheckStatusUsingPOST
     *
     * Get session status information.
     *
     */
    public function testGetCheckStatusUsingPOST()
    {
    }

    /**
     * Test case for getDocumentUsingGET
     *
     * Download single document containing the current signatures and modifications.
     *
     */
    public function testGetDocumentUsingGET()
    {
    }

    /**
     * Test case for getDocumentUsingPOST
     *
     * Download single document containing the current signatures and modifications.
     *
     */
    public function testGetDocumentUsingPOST()
    {
    }

    /**
     * Test case for getStatusLoadableUsingGET
     *
     * Get session status information and load session into memory if not already loaded.
     *
     */
    public function testGetStatusLoadableUsingGET()
    {
    }

    /**
     * Test case for getStatusLoadableUsingPOST
     *
     * Get session status information and load session into memory if not already loaded.
     *
     */
    public function testGetStatusLoadableUsingPOST()
    {
    }

    /**
     * Test case for getStatusUsingGET1
     *
     * Get session status information.
     *
     */
    public function testGetStatusUsingGET1()
    {
    }

    /**
     * Test case for getStatusUsingPOST
     *
     * Get session status information.
     *
     */
    public function testGetStatusUsingPOST()
    {
    }

    /**
     * Test case for getTransaktionsnummerUsingGET
     *
     * Get session Transaction number (TAN).
     *
     */
    public function testGetTransaktionsnummerUsingGET()
    {
    }

    /**
     * Test case for getTransaktionsnummerUsingPOST
     *
     * Get session Transaction number (TAN).
     *
     */
    public function testGetTransaktionsnummerUsingPOST()
    {
    }

    /**
     * Test case for getVersionUsingGET
     *
     * Get inSign version.
     *
     */
    public function testGetVersionUsingGET()
    {
    }

    /**
     * Test case for getVersionUsingPOST
     *
     * Get inSign version.
     *
     */
    public function testGetVersionUsingPOST()
    {
    }

    /**
     * Test case for loadSessionUsingPOST
     *
     * Load session from database.
     *
     */
    public function testLoadSessionUsingPOST()
    {
    }

    /**
     * Test case for purgeSessionUsingDELETE
     *
     * Remove session by TAN oder sessionid. Removes session from memory..
     *
     */
    public function testPurgeSessionUsingDELETE()
    {
    }

    /**
     * Test case for purgeSessionUsingGET
     *
     * Remove session by TAN oder sessionid. Removes session from memory..
     *
     */
    public function testPurgeSessionUsingGET()
    {
    }

    /**
     * Test case for purgeSessionUsingPOST
     *
     * Remove session by TAN oder sessionid. Removes session from memory..
     *
     */
    public function testPurgeSessionUsingPOST()
    {
    }

    /**
     * Test case for recoverdeleteSessionsUsingDELETE
     *
     * Retrieve all sessions documents as single zip file.
     *
     */
    public function testRecoverdeleteSessionsUsingDELETE()
    {
    }

    /**
     * Test case for recoverdeleteSessionsUsingGET
     *
     * Retrieve all sessions documents as single zip file.
     *
     */
    public function testRecoverdeleteSessionsUsingGET()
    {
    }

    /**
     * Test case for recoverdeleteSessionsUsingPOST
     *
     * Retrieve all sessions documents as single zip file.
     *
     */
    public function testRecoverdeleteSessionsUsingPOST()
    {
    }

    /**
     * Test case for uploaddocumentstreammultipartUsingPOST
     *
     * Upload a document into a previously configured session.
     *
     */
    public function testUploaddocumentstreammultipartUsingPOST()
    {
    }

    /**
     * Test case for uploaddocumentstreammultipartieUsingPOST
     *
     * Upload a document into a previously configured session.
     *
     */
    public function testUploaddocumentstreammultipartieUsingPOST()
    {
    }
}
