<?php
/**
 * SessionStatusInformationApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * inSign
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.21.2 build:1
 * Contact: insign-support@is2.de
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Insign;

use \Insign\Configuration;
use \Insign\ApiException;
use \Insign\ObjectSerializer;

/**
 * SessionStatusInformationApiTest Class Doc Comment
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class SessionStatusInformationApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for getBestaetigungsdatumUsingGET
     *
     * Retrieve document receipt confirmation timestamp.
     *
     */
    public function testGetBestaetigungsdatumUsingGET()
    {
    }

    /**
     * Test case for getBestaetigungsdatumUsingPOST
     *
     * Retrieve document receipt confirmation timestamp.
     *
     */
    public function testGetBestaetigungsdatumUsingPOST()
    {
    }

    /**
     * Test case for getCheckStatusUsingGET
     *
     * Get session status information.
     *
     */
    public function testGetCheckStatusUsingGET()
    {
    }

    /**
     * Test case for getCheckStatusUsingPOST
     *
     * Get session status information.
     *
     */
    public function testGetCheckStatusUsingPOST()
    {
    }

    /**
     * Test case for getExternInfosUsingGET
     *
     * Additional status information about session that is in external state.
     *
     */
    public function testGetExternInfosUsingGET()
    {
    }

    /**
     * Test case for getExternInfosUsingPOST
     *
     * Additional status information about session that is in external state.
     *
     */
    public function testGetExternInfosUsingPOST()
    {
    }

    /**
     * Test case for getSessionsUsingGET2
     *
     * List of session.
     *
     */
    public function testGetSessionsUsingGET2()
    {
    }

    /**
     * Test case for getSessionsUsingGET3
     *
     * List of session that belong to a single user. You can also pass a user list separated by 'separator' for multiple users.
     *
     */
    public function testGetSessionsUsingGET3()
    {
    }

    /**
     * Test case for getSessionsUsingPOST1
     *
     * Retrieve information about a bunch of sessions.
     *
     */
    public function testGetSessionsUsingPOST1()
    {
    }

    /**
     * Test case for getSessionsUsingPOST2
     *
     * List of session.
     *
     */
    public function testGetSessionsUsingPOST2()
    {
    }

    /**
     * Test case for getSessionsUsingPOST3
     *
     * List of session that belong to a single user. You can also pass a user list separated by 'separator' for multiple users.
     *
     */
    public function testGetSessionsUsingPOST3()
    {
    }

    /**
     * Test case for getStatusLoadableUsingGET
     *
     * Get session status information and load session into memory if not already loaded.
     *
     */
    public function testGetStatusLoadableUsingGET()
    {
    }

    /**
     * Test case for getStatusLoadableUsingPOST
     *
     * Get session status information and load session into memory if not already loaded.
     *
     */
    public function testGetStatusLoadableUsingPOST()
    {
    }

    /**
     * Test case for getStatusUsingGET
     *
     * Retrieve status about external user.
     *
     */
    public function testGetStatusUsingGET()
    {
    }

    /**
     * Test case for getStatusUsingGET1
     *
     * Get session status information.
     *
     */
    public function testGetStatusUsingGET1()
    {
    }

    /**
     * Test case for getStatusUsingPOST
     *
     * Get session status information.
     *
     */
    public function testGetStatusUsingPOST()
    {
    }

    /**
     * Test case for getUserForSessionsUsingGET
     *
     * Get owner of a session.
     *
     */
    public function testGetUserForSessionsUsingGET()
    {
    }

    /**
     * Test case for getUserForSessionsUsingPOST
     *
     * Get owner of a session.
     *
     */
    public function testGetUserForSessionsUsingPOST()
    {
    }
}
