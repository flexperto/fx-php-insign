<?php
/**
 * AdministrativeApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * inSign
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.21.2 build:1
 * Contact: insign-support@is2.de
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Insign;

use \Insign\Configuration;
use \Insign\ApiException;
use \Insign\ObjectSerializer;

/**
 * AdministrativeApiTest Class Doc Comment
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class AdministrativeApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for getAuswertungJSONUsingGET
     *
     * Get the accounting data for one or all tenants.
     *
     */
    public function testGetAuswertungJSONUsingGET()
    {
    }

    /**
     * Test case for systemCheckAutoJSONUsingGET
     *
     * System health check.
     *
     */
    public function testSystemCheckAutoJSONUsingGET()
    {
    }

    /**
     * Test case for systemCheckAutoJSONUsingPOST
     *
     * System health check.
     *
     */
    public function testSystemCheckAutoJSONUsingPOST()
    {
    }
}
