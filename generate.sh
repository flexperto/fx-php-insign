#!/usr/bin/env bash
docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli generate \
    -i /local/spec.json \
    -l php \
    -o /local \
    -c /local/config.json

rm -r docs lib test
mv Insign/* ./
rm -r Insign