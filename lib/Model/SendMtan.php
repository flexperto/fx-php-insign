<?php
/**
 * SendMtan
 *
 * PHP version 5
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * inSign
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.21.2 build:1
 * Contact: insign-support@is2.de
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Insign\Model;

use \ArrayAccess;
use \Insign\ObjectSerializer;

/**
 * SendMtan Class Doc Comment
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class SendMtan implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'SendMtan';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'docid' => 'string',
        'recipientsms' => 'string',
        'signatureid' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'docid' => null,
        'recipientsms' => null,
        'signatureid' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'docid' => 'docid',
        'recipientsms' => 'recipientsms',
        'signatureid' => 'signatureid'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'docid' => 'setDocid',
        'recipientsms' => 'setRecipientsms',
        'signatureid' => 'setSignatureid'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'docid' => 'getDocid',
        'recipientsms' => 'getRecipientsms',
        'signatureid' => 'getSignatureid'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['docid'] = isset($data['docid']) ? $data['docid'] : null;
        $this->container['recipientsms'] = isset($data['recipientsms']) ? $data['recipientsms'] : null;
        $this->container['signatureid'] = isset($data['signatureid']) ? $data['signatureid'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['docid'] === null) {
            $invalidProperties[] = "'docid' can't be null";
        }
        if ($this->container['signatureid'] === null) {
            $invalidProperties[] = "'signatureid' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets docid
     *
     * @return string
     */
    public function getDocid()
    {
        return $this->container['docid'];
    }

    /**
     * Sets docid
     *
     * @param string $docid Document ID
     *
     * @return $this
     */
    public function setDocid($docid)
    {
        $this->container['docid'] = $docid;

        return $this;
    }

    /**
     * Gets recipientsms
     *
     * @return string
     */
    public function getRecipientsms()
    {
        return $this->container['recipientsms'];
    }

    /**
     * Sets recipientsms
     *
     * @param string $recipientsms SMS recipient phone number used for the MTAN
     *
     * @return $this
     */
    public function setRecipientsms($recipientsms)
    {
        $this->container['recipientsms'] = $recipientsms;

        return $this;
    }

    /**
     * Gets signatureid
     *
     * @return string
     */
    public function getSignatureid()
    {
        return $this->container['signatureid'];
    }

    /**
     * Sets signatureid
     *
     * @param string $signatureid Signature ID
     *
     * @return $this
     */
    public function setSignatureid($signatureid)
    {
        $this->container['signatureid'] = $signatureid;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


