<?php
/**
 * ApplicationContext
 *
 * PHP version 5
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * inSign
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.21.2 build:1
 * Contact: insign-support@is2.de
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Insign\Model;

use \ArrayAccess;
use \Insign\ObjectSerializer;

/**
 * ApplicationContext Class Doc Comment
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ApplicationContext implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ApplicationContext';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'application_name' => 'string',
        'autowire_capable_bean_factory' => '\Insign\Model\AutowireCapableBeanFactory',
        'bean_definition_count' => 'int',
        'bean_definition_names' => 'string[]',
        'class_loader' => '\Insign\Model\ClassLoader',
        'display_name' => 'string',
        'environment' => '\Insign\Model\Environment',
        'id' => 'string',
        'parent' => '\Insign\Model\ApplicationContext',
        'parent_bean_factory' => '\Insign\Model\BeanFactory',
        'startup_date' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'application_name' => null,
        'autowire_capable_bean_factory' => null,
        'bean_definition_count' => 'int32',
        'bean_definition_names' => null,
        'class_loader' => null,
        'display_name' => null,
        'environment' => null,
        'id' => null,
        'parent' => null,
        'parent_bean_factory' => null,
        'startup_date' => 'int64'
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'application_name' => 'applicationName',
        'autowire_capable_bean_factory' => 'autowireCapableBeanFactory',
        'bean_definition_count' => 'beanDefinitionCount',
        'bean_definition_names' => 'beanDefinitionNames',
        'class_loader' => 'classLoader',
        'display_name' => 'displayName',
        'environment' => 'environment',
        'id' => 'id',
        'parent' => 'parent',
        'parent_bean_factory' => 'parentBeanFactory',
        'startup_date' => 'startupDate'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'application_name' => 'setApplicationName',
        'autowire_capable_bean_factory' => 'setAutowireCapableBeanFactory',
        'bean_definition_count' => 'setBeanDefinitionCount',
        'bean_definition_names' => 'setBeanDefinitionNames',
        'class_loader' => 'setClassLoader',
        'display_name' => 'setDisplayName',
        'environment' => 'setEnvironment',
        'id' => 'setId',
        'parent' => 'setParent',
        'parent_bean_factory' => 'setParentBeanFactory',
        'startup_date' => 'setStartupDate'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'application_name' => 'getApplicationName',
        'autowire_capable_bean_factory' => 'getAutowireCapableBeanFactory',
        'bean_definition_count' => 'getBeanDefinitionCount',
        'bean_definition_names' => 'getBeanDefinitionNames',
        'class_loader' => 'getClassLoader',
        'display_name' => 'getDisplayName',
        'environment' => 'getEnvironment',
        'id' => 'getId',
        'parent' => 'getParent',
        'parent_bean_factory' => 'getParentBeanFactory',
        'startup_date' => 'getStartupDate'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['application_name'] = isset($data['application_name']) ? $data['application_name'] : null;
        $this->container['autowire_capable_bean_factory'] = isset($data['autowire_capable_bean_factory']) ? $data['autowire_capable_bean_factory'] : null;
        $this->container['bean_definition_count'] = isset($data['bean_definition_count']) ? $data['bean_definition_count'] : null;
        $this->container['bean_definition_names'] = isset($data['bean_definition_names']) ? $data['bean_definition_names'] : null;
        $this->container['class_loader'] = isset($data['class_loader']) ? $data['class_loader'] : null;
        $this->container['display_name'] = isset($data['display_name']) ? $data['display_name'] : null;
        $this->container['environment'] = isset($data['environment']) ? $data['environment'] : null;
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['parent'] = isset($data['parent']) ? $data['parent'] : null;
        $this->container['parent_bean_factory'] = isset($data['parent_bean_factory']) ? $data['parent_bean_factory'] : null;
        $this->container['startup_date'] = isset($data['startup_date']) ? $data['startup_date'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets application_name
     *
     * @return string
     */
    public function getApplicationName()
    {
        return $this->container['application_name'];
    }

    /**
     * Sets application_name
     *
     * @param string $application_name application_name
     *
     * @return $this
     */
    public function setApplicationName($application_name)
    {
        $this->container['application_name'] = $application_name;

        return $this;
    }

    /**
     * Gets autowire_capable_bean_factory
     *
     * @return \Insign\Model\AutowireCapableBeanFactory
     */
    public function getAutowireCapableBeanFactory()
    {
        return $this->container['autowire_capable_bean_factory'];
    }

    /**
     * Sets autowire_capable_bean_factory
     *
     * @param \Insign\Model\AutowireCapableBeanFactory $autowire_capable_bean_factory autowire_capable_bean_factory
     *
     * @return $this
     */
    public function setAutowireCapableBeanFactory($autowire_capable_bean_factory)
    {
        $this->container['autowire_capable_bean_factory'] = $autowire_capable_bean_factory;

        return $this;
    }

    /**
     * Gets bean_definition_count
     *
     * @return int
     */
    public function getBeanDefinitionCount()
    {
        return $this->container['bean_definition_count'];
    }

    /**
     * Sets bean_definition_count
     *
     * @param int $bean_definition_count bean_definition_count
     *
     * @return $this
     */
    public function setBeanDefinitionCount($bean_definition_count)
    {
        $this->container['bean_definition_count'] = $bean_definition_count;

        return $this;
    }

    /**
     * Gets bean_definition_names
     *
     * @return string[]
     */
    public function getBeanDefinitionNames()
    {
        return $this->container['bean_definition_names'];
    }

    /**
     * Sets bean_definition_names
     *
     * @param string[] $bean_definition_names bean_definition_names
     *
     * @return $this
     */
    public function setBeanDefinitionNames($bean_definition_names)
    {
        $this->container['bean_definition_names'] = $bean_definition_names;

        return $this;
    }

    /**
     * Gets class_loader
     *
     * @return \Insign\Model\ClassLoader
     */
    public function getClassLoader()
    {
        return $this->container['class_loader'];
    }

    /**
     * Sets class_loader
     *
     * @param \Insign\Model\ClassLoader $class_loader class_loader
     *
     * @return $this
     */
    public function setClassLoader($class_loader)
    {
        $this->container['class_loader'] = $class_loader;

        return $this;
    }

    /**
     * Gets display_name
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->container['display_name'];
    }

    /**
     * Sets display_name
     *
     * @param string $display_name display_name
     *
     * @return $this
     */
    public function setDisplayName($display_name)
    {
        $this->container['display_name'] = $display_name;

        return $this;
    }

    /**
     * Gets environment
     *
     * @return \Insign\Model\Environment
     */
    public function getEnvironment()
    {
        return $this->container['environment'];
    }

    /**
     * Sets environment
     *
     * @param \Insign\Model\Environment $environment environment
     *
     * @return $this
     */
    public function setEnvironment($environment)
    {
        $this->container['environment'] = $environment;

        return $this;
    }

    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param string $id id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets parent
     *
     * @return \Insign\Model\ApplicationContext
     */
    public function getParent()
    {
        return $this->container['parent'];
    }

    /**
     * Sets parent
     *
     * @param \Insign\Model\ApplicationContext $parent parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->container['parent'] = $parent;

        return $this;
    }

    /**
     * Gets parent_bean_factory
     *
     * @return \Insign\Model\BeanFactory
     */
    public function getParentBeanFactory()
    {
        return $this->container['parent_bean_factory'];
    }

    /**
     * Sets parent_bean_factory
     *
     * @param \Insign\Model\BeanFactory $parent_bean_factory parent_bean_factory
     *
     * @return $this
     */
    public function setParentBeanFactory($parent_bean_factory)
    {
        $this->container['parent_bean_factory'] = $parent_bean_factory;

        return $this;
    }

    /**
     * Gets startup_date
     *
     * @return int
     */
    public function getStartupDate()
    {
        return $this->container['startup_date'];
    }

    /**
     * Sets startup_date
     *
     * @param int $startup_date startup_date
     *
     * @return $this
     */
    public function setStartupDate($startup_date)
    {
        $this->container['startup_date'] = $startup_date;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


