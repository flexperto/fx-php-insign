<?php
/**
 * ConfigureSignature
 *
 * PHP version 5
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * inSign
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.21.2 build:1
 * Contact: insign-support@is2.de
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Insign\Model;

use \ArrayAccess;
use \Insign\ObjectSerializer;

/**
 * ConfigureSignature Class Doc Comment
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ConfigureSignature implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ConfigureSignature';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'anrede' => 'string',
        'displayname' => 'string',
        'gebdat' => '\DateTime',
        'id' => 'string',
        'nachname' => 'string',
        'ort' => 'string',
        'posindex' => 'int',
        'position' => '\Insign\Model\PosistionDefintionInsideADocument',
        'required' => 'bool',
        'role' => 'string',
        'textsearch' => 'string',
        'vorname' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'anrede' => null,
        'displayname' => null,
        'gebdat' => 'date-time',
        'id' => null,
        'nachname' => null,
        'ort' => null,
        'posindex' => 'int32',
        'position' => null,
        'required' => null,
        'role' => null,
        'textsearch' => null,
        'vorname' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'anrede' => 'anrede',
        'displayname' => 'displayname',
        'gebdat' => 'gebdat',
        'id' => 'id',
        'nachname' => 'nachname',
        'ort' => 'ort',
        'posindex' => 'posindex',
        'position' => 'position',
        'required' => 'required',
        'role' => 'role',
        'textsearch' => 'textsearch',
        'vorname' => 'vorname'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'anrede' => 'setAnrede',
        'displayname' => 'setDisplayname',
        'gebdat' => 'setGebdat',
        'id' => 'setId',
        'nachname' => 'setNachname',
        'ort' => 'setOrt',
        'posindex' => 'setPosindex',
        'position' => 'setPosition',
        'required' => 'setRequired',
        'role' => 'setRole',
        'textsearch' => 'setTextsearch',
        'vorname' => 'setVorname'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'anrede' => 'getAnrede',
        'displayname' => 'getDisplayname',
        'gebdat' => 'getGebdat',
        'id' => 'getId',
        'nachname' => 'getNachname',
        'ort' => 'getOrt',
        'posindex' => 'getPosindex',
        'position' => 'getPosition',
        'required' => 'getRequired',
        'role' => 'getRole',
        'textsearch' => 'getTextsearch',
        'vorname' => 'getVorname'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['anrede'] = isset($data['anrede']) ? $data['anrede'] : null;
        $this->container['displayname'] = isset($data['displayname']) ? $data['displayname'] : null;
        $this->container['gebdat'] = isset($data['gebdat']) ? $data['gebdat'] : null;
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['nachname'] = isset($data['nachname']) ? $data['nachname'] : null;
        $this->container['ort'] = isset($data['ort']) ? $data['ort'] : null;
        $this->container['posindex'] = isset($data['posindex']) ? $data['posindex'] : null;
        $this->container['position'] = isset($data['position']) ? $data['position'] : null;
        $this->container['required'] = isset($data['required']) ? $data['required'] : null;
        $this->container['role'] = isset($data['role']) ? $data['role'] : null;
        $this->container['textsearch'] = isset($data['textsearch']) ? $data['textsearch'] : null;
        $this->container['vorname'] = isset($data['vorname']) ? $data['vorname'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['id'] === null) {
            $invalidProperties[] = "'id' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets anrede
     *
     * @return string
     */
    public function getAnrede()
    {
        return $this->container['anrede'];
    }

    /**
     * Sets anrede
     *
     * @param string $anrede Form of address for this signature field
     *
     * @return $this
     */
    public function setAnrede($anrede)
    {
        $this->container['anrede'] = $anrede;

        return $this;
    }

    /**
     * Gets displayname
     *
     * @return string
     */
    public function getDisplayname()
    {
        return $this->container['displayname'];
    }

    /**
     * Sets displayname
     *
     * @param string $displayname Functional role for the signature (Who is supposed to sign here). The text is displayed on the signature pad and in acrobat reader. If empty the technical role will be used here.
     *
     * @return $this
     */
    public function setDisplayname($displayname)
    {
        $this->container['displayname'] = $displayname;

        return $this;
    }

    /**
     * Gets gebdat
     *
     * @return \DateTime
     */
    public function getGebdat()
    {
        return $this->container['gebdat'];
    }

    /**
     * Sets gebdat
     *
     * @param \DateTime $gebdat Date of birth for this signature field
     *
     * @return $this
     */
    public function setGebdat($gebdat)
    {
        $this->container['gebdat'] = $gebdat;

        return $this;
    }

    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param string $id Signatur ID, must be distinct per document
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets nachname
     *
     * @return string
     */
    public function getNachname()
    {
        return $this->container['nachname'];
    }

    /**
     * Sets nachname
     *
     * @param string $nachname Last name for this signature field
     *
     * @return $this
     */
    public function setNachname($nachname)
    {
        $this->container['nachname'] = $nachname;

        return $this;
    }

    /**
     * Gets ort
     *
     * @return string
     */
    public function getOrt()
    {
        return $this->container['ort'];
    }

    /**
     * Sets ort
     *
     * @param string $ort Place for this signature field
     *
     * @return $this
     */
    public function setOrt($ort)
    {
        $this->container['ort'] = $ort;

        return $this;
    }

    /**
     * Gets posindex
     *
     * @return int
     */
    public function getPosindex()
    {
        return $this->container['posindex'];
    }

    /**
     * Sets posindex
     *
     * @param int $posindex Order of the signatures in the PDF and for the client(UI). Spaces or duplicates aren't a problem. The position (page,y,x) is used as a secondary criteria for sorting
     *
     * @return $this
     */
    public function setPosindex($posindex)
    {
        $this->container['posindex'] = $posindex;

        return $this;
    }

    /**
     * Gets position
     *
     * @return \Insign\Model\PosistionDefintionInsideADocument
     */
    public function getPosition()
    {
        return $this->container['position'];
    }

    /**
     * Sets position
     *
     * @param \Insign\Model\PosistionDefintionInsideADocument $position Position of the signature in the PDF by reference or coordinates
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->container['position'] = $position;

        return $this;
    }

    /**
     * Gets required
     *
     * @return bool
     */
    public function getRequired()
    {
        return $this->container['required'];
    }

    /**
     * Sets required
     *
     * @param bool $required Is the signature required?
     *
     * @return $this
     */
    public function setRequired($required)
    {
        $this->container['required'] = $required;

        return $this;
    }

    /**
     * Gets role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->container['role'];
    }

    /**
     * Sets role
     *
     * @param string $role Technical role for the signature (Who is supposed to sign here)
     *
     * @return $this
     */
    public function setRole($role)
    {
        $this->container['role'] = $role;

        return $this;
    }

    /**
     * Gets textsearch
     *
     * @return string
     */
    public function getTextsearch()
    {
        return $this->container['textsearch'];
    }

    /**
     * Sets textsearch
     *
     * @param string $textsearch Scan the documents content for this text and re-calculate the positions and number of fields according to to matches. The meaning of coordinates given in 'position' will change in this case. These x0 and y0 are now meant to be offsets to the match-positions lower left corner. Use trailing and/or leading spaces to avoid 'N2' to be found in 'N25'. The value must be a valid Java regular expression. See more Info regarding this feature at https://confluence.is2.de/display/IKB/Documentation+for+external+requests
     *
     * @return $this
     */
    public function setTextsearch($textsearch)
    {
        $this->container['textsearch'] = $textsearch;

        return $this;
    }

    /**
     * Gets vorname
     *
     * @return string
     */
    public function getVorname()
    {
        return $this->container['vorname'];
    }

    /**
     * Sets vorname
     *
     * @param string $vorname First name for this signature field
     *
     * @return $this
     */
    public function setVorname($vorname)
    {
        $this->container['vorname'] = $vorname;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


