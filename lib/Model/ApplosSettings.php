<?php
/**
 * ApplosSettings
 *
 * PHP version 5
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * inSign
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.21.2 build:1
 * Contact: insign-support@is2.de
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Insign\Model;

use \ArrayAccess;
use \Insign\ObjectSerializer;

/**
 * ApplosSettings Class Doc Comment
 *
 * @category Class
 * @package  Insign
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ApplosSettings implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ApplosSettings';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'error' => 'int',
        'message' => 'string',
        'messages' => 'map[string,string]',
        'need_to_acceept_to_sign_text' => 'string',
        'need_to_acceept_to_sign_text_active' => 'bool',
        'notification' => '\Insign\Model\ClientNotification',
        'real_name_input_skippable' => 'bool',
        'sessionid' => 'string',
        'trace' => 'string',
        'transaktionsnummer' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'error' => 'int32',
        'message' => null,
        'messages' => null,
        'need_to_acceept_to_sign_text' => null,
        'need_to_acceept_to_sign_text_active' => null,
        'notification' => null,
        'real_name_input_skippable' => null,
        'sessionid' => null,
        'trace' => null,
        'transaktionsnummer' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'error' => 'error',
        'message' => 'message',
        'messages' => 'messages',
        'need_to_acceept_to_sign_text' => 'needToAcceeptToSignText',
        'need_to_acceept_to_sign_text_active' => 'needToAcceeptToSignTextActive',
        'notification' => 'notification',
        'real_name_input_skippable' => 'realNameInputSkippable',
        'sessionid' => 'sessionid',
        'trace' => 'trace',
        'transaktionsnummer' => 'transaktionsnummer'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'error' => 'setError',
        'message' => 'setMessage',
        'messages' => 'setMessages',
        'need_to_acceept_to_sign_text' => 'setNeedToAcceeptToSignText',
        'need_to_acceept_to_sign_text_active' => 'setNeedToAcceeptToSignTextActive',
        'notification' => 'setNotification',
        'real_name_input_skippable' => 'setRealNameInputSkippable',
        'sessionid' => 'setSessionid',
        'trace' => 'setTrace',
        'transaktionsnummer' => 'setTransaktionsnummer'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'error' => 'getError',
        'message' => 'getMessage',
        'messages' => 'getMessages',
        'need_to_acceept_to_sign_text' => 'getNeedToAcceeptToSignText',
        'need_to_acceept_to_sign_text_active' => 'getNeedToAcceeptToSignTextActive',
        'notification' => 'getNotification',
        'real_name_input_skippable' => 'getRealNameInputSkippable',
        'sessionid' => 'getSessionid',
        'trace' => 'getTrace',
        'transaktionsnummer' => 'getTransaktionsnummer'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['error'] = isset($data['error']) ? $data['error'] : null;
        $this->container['message'] = isset($data['message']) ? $data['message'] : null;
        $this->container['messages'] = isset($data['messages']) ? $data['messages'] : null;
        $this->container['need_to_acceept_to_sign_text'] = isset($data['need_to_acceept_to_sign_text']) ? $data['need_to_acceept_to_sign_text'] : null;
        $this->container['need_to_acceept_to_sign_text_active'] = isset($data['need_to_acceept_to_sign_text_active']) ? $data['need_to_acceept_to_sign_text_active'] : null;
        $this->container['notification'] = isset($data['notification']) ? $data['notification'] : null;
        $this->container['real_name_input_skippable'] = isset($data['real_name_input_skippable']) ? $data['real_name_input_skippable'] : null;
        $this->container['sessionid'] = isset($data['sessionid']) ? $data['sessionid'] : null;
        $this->container['trace'] = isset($data['trace']) ? $data['trace'] : null;
        $this->container['transaktionsnummer'] = isset($data['transaktionsnummer']) ? $data['transaktionsnummer'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['error'] === null) {
            $invalidProperties[] = "'error' can't be null";
        }
        if ($this->container['sessionid'] === null) {
            $invalidProperties[] = "'sessionid' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets error
     *
     * @return int
     */
    public function getError()
    {
        return $this->container['error'];
    }

    /**
     * Sets error
     *
     * @param int $error Error code 0=OK
     *
     * @return $this
     */
    public function setError($error)
    {
        $this->container['error'] = $error;

        return $this;
    }

    /**
     * Gets message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->container['message'];
    }

    /**
     * Sets message
     *
     * @param string $message Error message if error!=0
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->container['message'] = $message;

        return $this;
    }

    /**
     * Gets messages
     *
     * @return map[string,string]
     */
    public function getMessages()
    {
        return $this->container['messages'];
    }

    /**
     * Sets messages
     *
     * @param map[string,string] $messages Error message if error!=0
     *
     * @return $this
     */
    public function setMessages($messages)
    {
        $this->container['messages'] = $messages;

        return $this;
    }

    /**
     * Gets need_to_acceept_to_sign_text
     *
     * @return string
     */
    public function getNeedToAcceeptToSignText()
    {
        return $this->container['need_to_acceept_to_sign_text'];
    }

    /**
     * Sets need_to_acceept_to_sign_text
     *
     * @param string $need_to_acceept_to_sign_text need_to_acceept_to_sign_text
     *
     * @return $this
     */
    public function setNeedToAcceeptToSignText($need_to_acceept_to_sign_text)
    {
        $this->container['need_to_acceept_to_sign_text'] = $need_to_acceept_to_sign_text;

        return $this;
    }

    /**
     * Gets need_to_acceept_to_sign_text_active
     *
     * @return bool
     */
    public function getNeedToAcceeptToSignTextActive()
    {
        return $this->container['need_to_acceept_to_sign_text_active'];
    }

    /**
     * Sets need_to_acceept_to_sign_text_active
     *
     * @param bool $need_to_acceept_to_sign_text_active need_to_acceept_to_sign_text_active
     *
     * @return $this
     */
    public function setNeedToAcceeptToSignTextActive($need_to_acceept_to_sign_text_active)
    {
        $this->container['need_to_acceept_to_sign_text_active'] = $need_to_acceept_to_sign_text_active;

        return $this;
    }

    /**
     * Gets notification
     *
     * @return \Insign\Model\ClientNotification
     */
    public function getNotification()
    {
        return $this->container['notification'];
    }

    /**
     * Sets notification
     *
     * @param \Insign\Model\ClientNotification $notification ID of the message that is responsible for the call
     *
     * @return $this
     */
    public function setNotification($notification)
    {
        $this->container['notification'] = $notification;

        return $this;
    }

    /**
     * Gets real_name_input_skippable
     *
     * @return bool
     */
    public function getRealNameInputSkippable()
    {
        return $this->container['real_name_input_skippable'];
    }

    /**
     * Sets real_name_input_skippable
     *
     * @param bool $real_name_input_skippable real_name_input_skippable
     *
     * @return $this
     */
    public function setRealNameInputSkippable($real_name_input_skippable)
    {
        $this->container['real_name_input_skippable'] = $real_name_input_skippable;

        return $this;
    }

    /**
     * Gets sessionid
     *
     * @return string
     */
    public function getSessionid()
    {
        return $this->container['sessionid'];
    }

    /**
     * Sets sessionid
     *
     * @param string $sessionid Session ID
     *
     * @return $this
     */
    public function setSessionid($sessionid)
    {
        $this->container['sessionid'] = $sessionid;

        return $this;
    }

    /**
     * Gets trace
     *
     * @return string
     */
    public function getTrace()
    {
        return $this->container['trace'];
    }

    /**
     * Sets trace
     *
     * @param string $trace Stack trace if exceptions occurred
     *
     * @return $this
     */
    public function setTrace($trace)
    {
        $this->container['trace'] = $trace;

        return $this;
    }

    /**
     * Gets transaktionsnummer
     *
     * @return string
     */
    public function getTransaktionsnummer()
    {
        return $this->container['transaktionsnummer'];
    }

    /**
     * Sets transaktionsnummer
     *
     * @param string $transaktionsnummer Transaction number
     *
     * @return $this
     */
    public function setTransaktionsnummer($transaktionsnummer)
    {
        $this->container['transaktionsnummer'] = $transaktionsnummer;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


